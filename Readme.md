# Core
## Install
To install TkachInc\Core - file composer.json your project must be entered in the unit require:

```
"tkachinc/core": "~3.2"
```

After that, if you have already installed composer execute:

```sh
$ php composer.phar install tkachinc/core
```

If not, then you must install the composer:

```sh
curl -sS https://getcomposer.org/installer | php
```

Create file compose.json

```json
{
    "require": {
        "tkachinc/core": "~3.2"
    }
}
```

And execute command:

```sh
$ php composer.phar install tkachinc/core
```
