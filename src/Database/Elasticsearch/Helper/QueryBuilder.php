<?php
namespace TkachInc\Core\Database\Elasticsearch\Helper;

/**
 * @author maxim
 */
class QueryBuilder
{
	protected $body = [];

	public function search($searchString, Array $fields = [])
	{
		$str = $searchString;
		$str = trim(str_replace(['"', "'", "-", "(", ")", "+", "\\", "|", "^"], ' ', $str));

		$this->body['query']['filtered']['query']['multi_match']['fields'] = $fields;
		$this->body['query']['filtered']['query']['multi_match']['query'] = $str;
		$this->body['query']['filtered']['query']['multi_match']['fuzziness'] = "AUTO";
		$this->body['query']['filtered']['query']['multi_match']['minimum_should_match'] = "70%";
	}

	public function checkBool($name, $bool)
	{
		$this->body['query']['filtered']['filter']['term'][$name] = $bool;
	}

	public function range($field, $query)
	{
		$this->body['query']['bool']['should'][] = ['range' => [$field => $query]];
	}

	public function term($field, $query)
	{
		$this->body['query']['bool']['should'][] = ['term' => [$field => $query]];
	}

	public function get()
	{
		return $this->body;
	}
}