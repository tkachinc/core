<?php
namespace TkachInc\Core\Database\Elasticsearch;

use Elasticsearch\Client;
use TkachInc\Core\Config\Config;
use TkachInc\Core\Database\Connections\ElasticsearchDatabase;
use TkachInc\Engine\Services\Helpers\ArrayHelper;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class ObjectModel
{
	protected static $_indexElastic = null;
	protected static $_typeElastic = null;
	protected static $_hasPrefix = false;
	protected static $_hasPostfix = false;

	protected static $_settings = [];
	protected static $_mapping = [];
	protected static $_fields = [];
	protected static $_templateName = null;
	protected $_id;

	protected static $_cache = [];
	protected static $_useCache = true;

	/**
	 * @param       $command
	 * @param array ...$params
	 * @return mixed
	 * @throws \Exception
	 */
	private static function command($command, ...$params)
	{
		$client = static::getElasticsearch();
		if (method_exists($client, $command)) {
			return $client->{$command}(...$params);
		} else {
			throw new \Exception('Not found method');
		}
	}

	/**
	 * @param       $command
	 * @param array ...$params
	 * @return mixed
	 * @throws \Exception
	 */
	private static function indices($command, ...$params)
	{
		$client = static::getElasticsearch()->indices();
		if (method_exists($client, $command)) {
			return $client->{$command}(...$params);
		} else {
			throw new \Exception('Not found method');
		}
	}

	/**
	 * @return null
	 */
	public static function getIndex()
	{
		if (static::$_hasPrefix === true) {
			$prefix = Config::getInstance()->get(['prefix'], '');

			return $prefix . static::$_indexElastic;
		}
		if (static::$_hasPostfix === true) {
			$postfix = Config::getInstance()->get(['postfix'], '');

			return static::$_indexElastic . $postfix;
		}

		return static::$_indexElastic;
	}

	/**
	 * @return null
	 */
	public static function getType()
	{
		return static::$_typeElastic;
	}


	/**
	 * @return bool
	 */
	public static function hasPrefix() {
		return static::$_hasPrefix;
	}

	/**
	 * @return bool
	 */
	public static function hasPostfix() {
		return static::$_hasPostfix;
	}

	/**
	 * @return string
	 */
	public static function getTemplateName()
	{
		return static::$_templateName;
	}

	/**
	 * @return array
	 */
	public static function getSettings()
	{
		return static::$_settings;
	}

	/**
	 * @return array
	 */
	public static function getMapping()
	{
		return static::$_mapping;
	}

	/**
	 * @param $useCache
	 */
	public function setUseCache($useCache)
	{
		static::$_useCache = (bool)$useCache;
	}

	/**
	 * @param $id
	 */
	public function __construct($id)
	{
		$this->_id = $id;
		if (static::$_useCache && isset(static::$_cache[$id])) {
			foreach (static::$_cache[$id] as $key => $value) {
				$this->{$key} = $value;
			}
		} else {
			$result = static::command(
				'get',
				[
					'id'    => $this->_id,
					'index' => static::getIndex(),
					'type'  => static::getType(),
				]
			);

			if (isset($result['_source'])) {
				$fields = static::getFields();
				$fields = ArrayHelper::filterOnlyRequiredParams($result['_source'], $fields);
				if (empty($fields['id'])) {
					$fields['id'] = $this->_id;
				}

				foreach ($fields as $key => $value) {
					$this->{$key} = $value;
				}
			}

			if (static::$_useCache) {
				static::$_cache[$id] = $this->getFieldsArray();
			}
		}
	}

	/**
	 * @return array
	 */
	public static function getFields()
	{
		return static::$_fields;
	}

	/**
	 * Излекает поля объекта в массив, пригодный для сохранения
	 *
	 * @return array Массив полей объекта
	 */
	public function getFieldsArray()
	{
		$result = array_intersect_key(
			(array)$this,
			array_flip(array_keys(static::getFields()))
		);

		return $result;
	}

	/**
	 * @return bool
	 */
	public function delete()
	{
		if (static::isExistObject($this->_id)) {
			return static::command(
				'delete',
				[
					'id'    => $this->_id,
					'index' => static::getIndex(),
					'type'  => static::getType(),
				]

			);
		}

		return false;
	}

	/**
	 * @param $
	 * @return bool
	 */
	public function update()
	{
		return static::command(
			'update',
			[
				'id'    => $this->_id,
				'index' => static::getIndex(),
				'type'  => static::getType(),
				'body'  => ['doc' => $this->getFieldsArray()],
			]

		);
	}

	/**
	 * @return bool
	 */
	public function create()
	{
		return static::command(
			'create',
			[
				'id'    => $this->_id,
				'index' => static::getIndex(),
				'type'  => static::getType(),
				'body'  => $this->getFieldsArray(),
			]

		);
	}

	/**
	 * @return bool
	 */
	public function save()
	{
		if (static::isExistObject($this->_id)) {
			return $this->update();
		} else {
			return $this->create();
		}
	}

	/**
	 * @return \Elasticsearch\Client
	 */
	public static function getElasticsearch()
	{
		return ElasticsearchDatabase::getInstance();
	}

	/**
	 * @param array $body
	 * @return int
	 */
	public static function count($body = [])
	{
		$params = [
			'index' => static::getIndex(),
			'type'  => static::getType(),
		];
		if (!empty($body)) {
			$params['body'] = $body;
		}
		$count = 0;
		$result = static::command('count', $params);
		if (isset($result['count'])) {
			$count = $result['count'];
		}

		return $count;
	}

	/**
	 * @param string|array $body Query DSL
	 * @param string|array $q Query in the Lucene query string syntax
	 * @param int $from
	 * @param int $perPage
	 * @example
	 *                           $user->search(['query' => ['query_string' => ['query' => 'gollariel', 'fields' => [
	 *                           "email", "*_name" ]]]]);
	 * @param bool $onlyHits
	 * @param bool $onlySource
	 * @return array
	 */
	public static function search($body = [], $q = '', $from = 0, $perPage = 100, $onlyHits = true, $onlySource = true)
	{
		$params = [
			'index' => static::getIndex(),
			'type'  => static::getType(),
			'size'  => $perPage,
			'from'  => $from,
		];

		if (!empty($q)) {
			$params['q'] = $q;
		}
		if (!empty($body)) {
			$params['body'] = $body;
		}

		$result = static::command('search', $params);
		if ($onlyHits) {
			if (isset($result['hits']['hits'])) {
				if ($onlySource) {
					$data = [];
					foreach ($result['hits']['hits'] as $value) {
						if (isset($value['_source'])) {
							$data[] = $value['_source'];
						}
					}

					return $data;
				} else {
					return $result['hits']['hits'];
				}
			}
		} else {
			return $result;
		}

		return [];
	}

	/**
	 * @param array $id
	 * @param bool $onlySource
	 * @return array
	 */
	public static function get($id, $onlySource = true)
	{
		$result = static::command('get', ['id' => $id, 'type' => static::getType(), 'index' => static::getIndex()]);
		if ($onlySource) {
			if (isset($result['_source'])) {
				return $result['_source'];
			}
		}

		return $result;
	}

	/**
	 * Заполняет поля объекта из массива
	 *
	 * @param array $fields - массив, из которого происходит заполнение
	 */
	public function setFieldsArray($fields)
	{
		foreach (static::getFields() as $field => $value) {
			if (isset($fields[$field])) {
				$this->{$field} = $fields[$field];
			}
		}
	}

	/**
	 * @return mixed
	 */
	public static function createIndex()
	{
		$query = ['index' => static::getIndex(), 'body' => []];

		$template = static::getTemplateName();
		if ($template !== null) {
			$query['body']['template'] = $template;
		}

		$settings = static::getSettings();
		$mappings = static::getMapping();
		if (!empty($settings)) {
			$query['body']['settings'] = $settings;
		}
		if (!empty($mappings)) {
			$query['body']['mappings'][static::getType()] = $mappings;
		}

		return static::indices(
			'create',
			$query
		);
	}

	/**
	 * @return mixed
	 */
	public static function putMapping()
	{
		$query = [
			'index' => static::getIndex(),
			'type'  => static::getType(),
			'body'  => [static::getType() => static::getMapping()],
		];

		return static::indices(
			'putMapping',
			$query
		);
	}

	/**
	 * @return mixed
	 */
	public static function deleteMapping()
	{
		$query = ['index' => static::getIndex(), 'type' => static::getType()];

		return static::indices(
			'deleteMapping',
			$query
		);
	}

	/**
	 * @return mixed
	 */
	public static function getMappingInDB()
	{
		$query = ['index' => static::getIndex(), 'type' => static::getType()];

		return static::indices(
			'getMapping',
			$query
		);
	}

	/**
	 * @return mixed
	 */
	public static function putSettings()
	{
		$query = ['index' => static::getIndex(), 'body' => static::getSettings()];

		return static::indices(
			'putSettings',
			$query
		);
	}

	/**
	 * @return mixed
	 */
	public static function getSettingsInDB()
	{
		$query = ['index' => static::getIndex()];

		return static::indices(
			'getSettings',
			$query
		);
	}

	/**
	 * @return mixed
	 */
	public static function deleteIndex()
	{
		return static::indices(
			'delete',
			[
				'index' => static::getIndex(),
			]
		);
	}

	/**
	 * @param $id
	 * @return array
	 */
	public static function isExistObject($id)
	{
		return static::command(
			'exists',
			[
				'id'    => $id,
				'index' => static::getIndex(),
				'type'  => static::getType(),
			]
		);
	}

	/**
	 * @param $id
	 * @param $data
	 * @return bool
	 */
	public static function saveData($id, $data)
	{
		$exist = static::isExistObject($id);
		if ($exist) {
			return static::updateData($id, $data);
		} else {
			return static::createData($id, $data);
		}
	}

	/**
	 * @param       $id
	 * @param array $data
	 * @return bool
	 */
	public static function updateData($id, Array $data)
	{
		return static::command(
			'update',
			[
				'id'    => $id,
				'index' => static::getIndex(),
				'type'  => static::getType(),
				'body'  => ['doc' => $data],
			]
		);
	}

	/**
	 * @param       $id
	 * @param array $data
	 * @return bool
	 */
	public static function createData($id, Array $data)
	{
		return static::command(
			'create',
			[
				'id'    => $id,
				'index' => static::getIndex(),
				'type'  => static::getType(),
				'body'  => $data,
			]
		);
	}

	/**
	 * @param $id
	 * @return bool
	 */
	public static function deleteData($id)
	{
		if (static::isExistObject($id)) {
			return static::command(
				'delete',
				[
					'id'    => $id,
					'index' => static::getIndex(),
					'type'  => static::getType(),
				]
			);
		}

		return false;
	}
}