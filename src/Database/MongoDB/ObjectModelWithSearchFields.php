<?php
namespace TkachInc\Core\Database\MongoDB;

/**
 * @author maxim
 */
abstract class ObjectModelWithSearchFields extends ObjectModel
{
	/** @var array Поля для полнотекстового поиска */
	protected static $_fieldsSearch = [];

	/**
	 * Излекает поля объекта в массив, пригодный для сохранения в Elasticsearch
	 *
	 * @return array Массив полей объекта
	 */
	public function getFieldsArrayBySearch()
	{
		$result = array_intersect_key(
			(array)$this,
			static::getFieldsSearch()
		);

		return $result;
	}

	/**
	 * @param       $funcName
	 * @param array $data
	 *
	 * @return array
	 */
	protected static function mergeField($funcName, $data)
	{
		$current = get_called_class();
		$parent = get_parent_class($current);
		if ($parent && !empty($parent) && method_exists($parent, $funcName)) {
			return array_merge($parent::$funcName(), $data);
		} else {
			return $data;
		}
	}

	/**
	 * Возвращает значения по умолчанию полей
	 *
	 * @return array значения по умолчанию полей
	 */
	public static function getFieldsSearch()
	{
		$current = get_called_class();
		if (array_search($current . 'fieldsSearch', static::$_fieldMerged) === false) {
			static::$_fieldsSearch = static::mergeField('getFieldsSearch', static::$_fieldsSearch);
			static::$_fieldMerged[] = $current . 'fieldsSearch';
		}

		return static::$_fieldsSearch;
	}
}