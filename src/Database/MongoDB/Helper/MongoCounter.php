<?php
namespace TkachInc\Core\Database\MongoDB\Helper;
use TkachInc\Core\Database\MongoDB\Helper\Model\CounterModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class MongoCounter
{
	/**
	 * @param string $name
	 * @return bool|int
	 */
	public static function inc($name)
	{
		$result = CounterModel::findAndModify(
			['_id' => $name],
			['$inc' => ['counter' => 1]],
			['counter' => true],
			['new' => true, 'upsert' => true]
		);
		if (isset($result['counter'])) {
			return $result['counter'];
		} else {
			return false;
		}
	}

	/**
	 * @param $name
	 * @param $value
	 * @return bool|int
	 */
	public static function set($name, $value)
	{
		$result = CounterModel::findAndModify(
			['_id' => $name],
			['$set' => ['counter' => (int)$value]],
			['counter' => true],
			['new' => true, 'upsert' => true]
		);
		if (isset($result['counter'])) {
			return $result['counter'];
		} else {
			return false;
		}
	}

	/**
	 * @param $name
	 * @return bool
	 */
	public static function get($name)
	{
		$result = CounterModel::findOne(['_id' => $name]);
		if (isset($result['counter'])) {
			return $result['counter'];
		} else {
			return false;
		}
	}
}