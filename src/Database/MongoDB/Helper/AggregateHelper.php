<?php
namespace TkachInc\Core\Database\MongoDB\Helper;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * Class AggregateHelper
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AggregateHelper
{
	protected $pipeline = [];

	/**
	 * @param array $block
	 * @return $this
	 */
	public function addCustomBlock(Array $block)
	{
		$this->pipeline[] = $block;

		return $this;
	}

	/**
	 * @param array $data
	 * @return $this
	 */
	public function addMatch(Array $data)
	{
		$this->pipeline[] = ['$match' => $data];

		return $this;
	}

	public function addFullTextMatch(Array $data)
	{
		$this->pipeline[] = ['$match' => ['$text' => ['$search' => $data]]];

		return $this;
	}

	public function addSortByTextScore(Array $data)
	{
		$this->pipeline[] = ['$sort' => ['score' => ['$meta' => $data]]];

		return $this;
	}


	/**
	 * @param array $data
	 * @return $this
	 */
	public function addGroup(Array $data)
	{
		$this->pipeline[] = ['$group' => $data];

		return $this;
	}

	/**
	 * @param array $data
	 * @return $this
	 */
	public function addProject(Array $data)
	{
		$this->pipeline[] = ['$project' => $data];

		return $this;
	}

	/**
	 * @param $data
	 * @return $this
	 */
	public function addUnwind($data)
	{
		$this->pipeline[] = ['$unwind' => $data];

		return $this;
	}

	/**
	 * @param array $data
	 * @return $this
	 */
	public function addLookup(Array $data)
	{
		$this->pipeline[] = ['$lookup' => $data];

		return $this;
	}

	/**
	 * @param $fromCollectionName
	 * @param $localField
	 * @param $foreignField
	 * @param $as
	 * @return $this
	 */
	public function addRelationModel($fromCollectionName, $localField, $foreignField, $as)
	{
		$this->addLookup(
			[
				'from'         => $fromCollectionName,
				'localField'   => $localField,
				'foreignField' => $foreignField,
				'as'           => $as,
			]
		);

		return $this;
	}

	/**
	 * @param $collection
	 * @return $this
	 */
	public function addOut($collection)
	{
		$this->pipeline[] = ['$out' => $collection];

		return $this;
	}

	/**
	 * @param array $data
	 * @return $this
	 */
	public function addSort(Array $data)
	{
		$this->pipeline[] = ['$sort' => $data];

		return $this;
	}

	/**
	 * @return array
	 */
	public function getQuery()
	{
		return $this->pipeline;
	}

	/**
	 * @param          $collectionName
	 * @param \Closure $callable
	 * @param string $name
	 * @return \Generator
	 */
	public function generate($collectionName, \Closure $callable, $name = '')
	{
		$cursor = ObjectModel::getMongoDatabase($name)->selectCollection($collectionName)->aggregate(
			$this->getQuery(),
			['maxTimeMS' => 300000]
		);
		foreach ($cursor as $item) {
			yield $callable($item);
		}
	}
}