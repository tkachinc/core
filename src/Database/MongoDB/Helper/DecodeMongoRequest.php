<?php
namespace TkachInc\Core\Database\MongoDB\Helper;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class DecodeMongoRequest
{
	protected $opers = [
		'$eq'     => 1,
		'$lt'     => 1,
		'$gt'     => 1,
		'$lte'    => 1,
		'$gte'    => 1,
		'$ne'     => 1,
		'$in'     => 5,
		'$nin'    => 5,
		'$type'   => 4,
		'$exists' => 3,

		'$or'  => 2,
		'$and' => 2,
		'$not' => 2,
		'$nor' => 2,
	];

	protected $validate = [];

	/**
	 * @param array $validate
	 */
	public function setValidate(Array $validate)
	{
		$this->validate = $validate;
	}

	/**
	 * @return array|mixed|string
	 */
	public function jsonDecodeFilter($filter)
	{
		if (!empty($filter)) {
			$filter = json_decode($filter, true);
			if (json_last_error() !== JSON_ERROR_NONE) {
				$filter = [];
			}
		} else {
			$filter = [];
		}

		return $filter;
	}

	/**
	 * @return bool|mixed|string
	 */
	public function getSortMongo($sort)
	{
		if (!empty($sort)) {
			$sort = json_decode($sort, true);
			if (json_last_error() !== JSON_ERROR_NONE) {
				$sort = true;
			}

			if (is_array($sort)) {
				foreach ($sort as $field => &$value) {
					$value = (int)$value;
				}
			}
		} else {
			$sort = true;
		}

		return $sort;
	}

	/**
	 * @param array $data
	 */
	public function recursiveDecode(Array &$data, $field = null)
	{
		foreach ($data as $key => &$value) {
			if (isset($this->validate[$key])) {
				if (!is_array($value) && !is_object($value)) {
					$value = ObjectModel::decode($value, $this->validate[$key]);
				} else {
					$this->recursiveDecode($value, $key);
				}
			} elseif (isset($this->opers[$key])) {
				if ($this->opers[$key] === 2) {
					foreach ($value as &$val) {
						$this->recursiveDecode($val, $field);
					}
				} elseif ($this->opers[$key] === 3) {
					$value = ObjectModel::decode($value, ObjectModel::TYPE_BOOL);
				} elseif ($this->opers[$key] === 4) {
					$value = ObjectModel::decode($value, ObjectModel::TYPE_UNSIGNED_INT);
				} elseif ($this->opers[$key] === 5) {
					if (!is_array($value)) {
						$value = ObjectModel::decode($value, ObjectModel::TYPE_JSON);
					}
				} else {
					if ($field !== null && isset($this->validate[$field])) {
						$value = ObjectModel::decode($value, $this->validate[$field]);
					}
				}
			}
		}
	}
}