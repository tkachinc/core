<?php
namespace TkachInc\Core\Database\MongoDB\Helper\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class CounterModel extends ObjectModel
{
	protected static $_writeConcernMode = 1;
	//protected static $readPreference = ObjectModel::RP_SECONDARY_PREFERRED;
	protected static $_collection = 'counters';
	protected static $_updateMethod = self::UPDATE_METHOD_SET;
	protected static $_pk = '_id';
	protected static $_sort = ['_id' => 1];
	protected static $_indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true,
		],
	];

	protected static $_fieldsDefault = [
		'_id'     => '',
		'counter' => 0,
	];

	protected static $_fieldsValidate = [
		'_id'     => self::TYPE_STRING,
		'counter' => self::TYPE_UNSIGNED_INT,
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;
}