<?php
namespace TkachInc\Core\Database\MongoDB\Helper;

/**
 * @author Kostya Choporov, Maxim Tkach
 */
class MongoConditionChecker
{
	protected $dbobject;
	protected static $operation = [
		'$or'  => 1,
		'$and' => 2,
		'_or'  => 1,
		'_and' => 2,
	];

	protected $flag = false;

	/**
	 * @param array $conditions
	 * @param array|object $dbobject
	 * @return bool
	 */
	public static function evaluate($conditions, $dbobject)
	{
		$cond = new static;
		$cond->dbobject = (array)$dbobject;

		return (bool)$cond->checkConditions($conditions);
	}

	/**
	 * @param mixed $conditions - массив или простое значение
	 * @return bool
	 */
	protected function checkConditions($conditions)
	{
		foreach ($conditions as $field => $condition) {
			$this->flag = $this->checkOperation($condition, $this->dbobject[$field], $field);
			if (!isset($condition[0]) && $this->flag === false) {
				return $this->flag;
			}
		}

		return $this->flag;
	}

	/**
	 * @param $condition
	 * @param $dbobject
	 * @param $field
	 * @return bool
	 */
	protected function checkOperation($condition, $dbobject, $field)
	{
		$flag = true;
		$callbacks = static::returnCallbacks();
		if (is_array($condition) && !isset($condition[0])) {
			foreach ($condition as $op => $value) {
				// операция $lt, $gt, $in и т.п.
				if (isset($callbacks[$op])) {
					$result = false;
					if (isset($dbobject)) {
						$result = $callbacks[$op]([$dbobject, $value]);
					}
					if (!$result) {
						$flag = false;
					}
				} elseif (isset($dbobject[$op])) { // Сравниваем массив
					$flag = $this->checkOperation($value, $dbobject[$op], $op);
				}
			}
		} elseif (is_array($condition) && isset($condition[0])) { // $and, $or
			if (isset(static::$operation[$field])) {
				foreach ($condition as $item) {
					foreach ($item as $field_c => $condition) {
						$flag = $this->checkOperation($condition, $dbobject[$field_c], $field_c);
						if ($flag === true && static::$operation[$field] === 1) {
							return $flag;
						} elseif ($flag === false && static::$operation[$field] === 2) {
							return $flag;
						}
					}
				}
			}
		} else {
			// обычное значение
			if ($dbobject != $condition) {
				$flag = false;
			}
		}

		return $flag;
	}

	/**
	 * @return array
	 */
	protected static function returnCallbacks()
	{
		return [
			'$lt'    => function ($arg) {
				return ($arg[0] < $arg[1]);
			},
			'$gt'    => function ($arg) {
				return ($arg[0] > $arg[1]);
			},
			'$lte'   => function ($arg) {
				return ($arg[0] <= $arg[1]);
			},
			'$gte'   => function ($arg) {
				return ($arg[0] >= $arg[1]);
			},
			'$eq'    => function ($arg) {
				return ($arg[0] === $arg[1]);
			},
			'$ne'    => function ($arg) {
				return ($arg[0] !== $arg[1]);
			},
			'$in'    => function ($arg) {
				return (bool)in_array($arg[0], $arg[1]);
			},
			'$nin'   => function ($arg) {
				return (!in_array($arg[0], $arg[1])) ? true : false;
			},
			'$regex' => function ($arg) {
				if (preg_match($arg[0], $arg[1], $matches)) {
					return true;
				} else {
					return false;
				}
			},
			'_lt'    => function ($arg) {
				return ($arg[0] < $arg[1]);
			},
			'_gt'    => function ($arg) {
				return ($arg[0] > $arg[1]);
			},
			'_lte'   => function ($arg) {
				return ($arg[0] <= $arg[1]);
			},
			'_gte'   => function ($arg) {
				return ($arg[0] >= $arg[1]);
			},
			'_eq'    => function ($arg) {
				return ($arg[0] === $arg[1]);
			},
			'_ne'    => function ($arg) {
				return ($arg[0] !== $arg[1]);
			},
			'_in'    => function ($arg) {
				return (bool)in_array($arg[0], $arg[1]);
			},
			'_nin'   => function ($arg) {
				return (!in_array($arg[0], $arg[1])) ? true : false;
			},
			'_regex' => function ($arg) {
				if (preg_match($arg[0], $arg[1], $matches)) {
					return true;
				} else {
					return false;
				}
			},
		];
	}
}
