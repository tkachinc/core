<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 2/22/17
 * Time: 13:07
 */

namespace TkachInc\Core\Database\MongoDB\Helper\GeospatialQueryOperators;


class GeospatialQueryOperators
{
	/**
	 * @var array
	 */
	protected $queries = [];

	/**
	 * @param GeospatialQueryOperator $operator
	 */
	public function add(GeospatialQueryOperator $operator)
	{
		$this->queries[] = $operator->getQuery();
	}

	/**
	 * This is array
	 * key - field
	 * value - query
	 * @return array
	 */
	public function getAll()
	{
		return $this->queries;
	}
}