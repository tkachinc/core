<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 2/22/17
 * Time: 13:14
 */

namespace TkachInc\Core\Database\MongoDB\Helper\GeospatialQueryOperators;


interface GeospatialQueryOperator
{
	public function getValue();
	public function getQuery();
	public function getField();
}