<?php
namespace TkachInc\Core\Database\MongoDB\Helper\GeospatialQueryOperators;

/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 2/22/17
 * Time: 13:07
 */
class GeoWithin implements GeospatialQueryOperator
{
	protected $field;

	protected $value;
	protected $type;
	protected $crs;

	public function __construct($field, array $coordinates, $type = 'Polygon', $crs = [])
	{
		$this->field = $field;
		$this->value = $coordinates;
		$this->type = $type;
		$this->crs = $crs;
	}

	public function getField()
	{
		return $this->field;
	}

	public function getValue()
	{
		return $this->value;
	}

	public function getQuery()
	{
		$query = [$this->field => ['$geoWithin' => ['$geometry' => ['type' => $this->type, 'coordinates' => $this->value]]]];
		if(!empty($this->crs)) {
			$query[$this->field]['$geoWithin']['$geometry']['crs'] = $this->crs;
		}
		return $query;
	}
}