<?php
namespace TkachInc\Core\Database\MongoDB;

use DateTime;
use MongoDB\Client;
use MongoDB\Collection;
use MongoDB\Database;
use MongoDB\Driver\Cursor;
use MongoDB\Driver\ReadPreference;
use MongoDB\Driver\WriteConcern;
use MongoDB\Driver\ReadConcern;
use TkachInc\Core\Config\Config;
use TkachInc\Core\Database\DBException;
use TkachInc\Core\Database\MongoDB\Helper\GeospatialQueryOperators\GeospatialQueryOperators;
use TkachInc\Core\Database\ThreadKeepStaticArray;
use TkachInc\Engine\Services\Helpers\ArrayHelper;
use TkachInc\Core\Database\Connections\MongoDBDatabase;
use TkachInc\Core\Log\FastLog;

/**
 * Class OM
 *
 * @package TkachInc\Core\Database
 */
abstract class ObjectModel
{
	const RP_PRIMARY = 1;
	const RP_PRIMARY_PREFERRED = 5;
	const RP_SECONDARY = 2;
	const RP_SECONDARY_PREFERRED = 6;
	const RP_NEAREST = 10;

	const UPDATE_METHOD_FULL = 'full';
	const UPDATE_METHOD_INC = 'inc';
	const UPDATE_METHOD_SET = 'set';
	const UPDATE_METHOD_CACHE_ONLY = 'cacheOnly';

	const TYPE_INT = 'isInt';
	const TYPE_UNSIGNED_INT = 'isUnsignedInt';
	const TYPE_FLOAT = 'isFloat';
	const TYPE_DECIMAL = 'isDecimal';
	const TYPE_UNSIGNED_FLOAT = 'isUnsignedFloat';
	const TYPE_BOOL = 'isBool';
	const TYPE_TIMESTAMP = 'isTimeStamp';
	const TYPE_STRING = 'isString';
	const TYPE_TEXT = 'isText';
	const TYPE_HTML = 'isHTML';
	const TYPE_MARKDOWN = 'isMarkDown';
	const TYPE_JSON = 'isJSON';
	const TYPE_MONGO_ID = 'isMongoId';
	const TYPE_MONGO_DATE = 'isMongoDate';
	const TYPE_MONGO_TIMESTAMP = 'isMongoTimestamp';
	const TYPE_UID = 'isUID';

	const WRITE_CONCERN_MAJORITY = 'majority';

	/** @var string имя первичного ключа */
	protected static $_pk = 'id';

	/** @var array Параметры сортировки для Mongo */
	/*  Например array('id'=>1) */
	protected static $_sort = null;

	/** @var array Параметры сортировки для выбора максимального id в автодобавлении */
	protected static $_rsort = null;

	/** @var string Имя коллекции Mongo */
	protected static $_collection = null;

	protected static $_hasPrefix = false;

	protected static $_hasPostfix = false;

	/** @var array Все поля со значениями по умолчанию */
	protected static $_fieldsDefault = [];

	/** @var array Хинты всех полей */
	protected static $_fieldsHint = [];

	/** @var array Скрытые от клиента поля */
	protected static $_fieldsPrivate = [];

	/** @var array Правила валидации полей */
	protected static $_fieldsValidate = [];

	/** @var array замерженные поля */
	protected static $_fieldMerged = [];

	/** @var array Статичестий кеш */
	protected static $_cache = [];

	/**
	 * @link https://docs.mongodb.org/manual/reference/write-concern/#wc-wtimeout
	 * This option specifies a time limit, in milliseconds, for the write concern.
	 * wtimeout is only applicable for w values greater than 1.
	 * wtimeout causes write operations to return with an error after the specified
	 * limit, even if the required write concern will eventually succeed. When these
	 * write operations return, MongoDB does not undo successful data modifications
	 * performed before the write concern exceeded the wtimeout time limit.
	 * If you do not specify the wtimeout option and the level of write concern is
	 * unachievable, the write operation will block indefinitely. Specifying a wtimeout
	 * value of 0 is equivalent to a write concern without the wtimeout option.
	 */
	protected static $_wtimeout = 0;

	/**
	 * @var bool
	 * @link https://docs.mongodb.org/manual/reference/write-concern/
	 * Requests acknowledgement that the mongod instances, as specified in the w: <value>, have written to the
	 * on-disk journal. j: true does not by itself guarantee that the write will not be rolled back due to replica
	 * set primary failover.
	 * Changed in version 3.2: With j: true, MongoDB returns only after the requested number of members,
	 * including the primary, have written to the journal. Previously j: true write concern in a replica set only
	 * requires the primary to write to the journal, regardless of the w: <value> write concern.
	 * For replica sets using protocolVersion: 1, w: "majority" implies j: true, if journaling is enabled.
	 * Journaling is enabled by default.
	 */
	protected static $_journal = false;

	/**
	 * @var int|string|array|null Безопасная ли запись, обычно 1 (запись с подтверждением)
	 * @link http://www.php.net/manual/ru/mongo.writeconcerns.php
	 * w=0    Unacknowledged    A write will not be followed up with a GLE call, and therefore not checked ("fire and
	 * forget") w=1    Acknowledged    The write will be acknowledged by the server (the primary on replica set
	 * configuration) w=N    Replica Set Acknowledged    The write will be acknowledged by the primary server, and
	 * replicated to N-1 secondaries. w=majority    Majority Acknowledged    The write will be acknowledged by the
	 * majority of the replica set (including the primary). This is a special reserved string. w=<tag set>    Replica
	 * Set Tag Set Acknowledged    The write will be acknowledged by members of the entire tag set PECL mongo >=0.9.0
	 */
	protected static $_writeConcernMode = null;

	/**
	 * @var int|string|array|null
	 * For the WiredTiger storage engine, the readConcern option allows clients to choose a level of isolation for
	 * their reads. You can specify a read concern of "majority" to read data that has been written to a majority of
	 * replica set members and thus cannot be rolled back. With the MMAPv1 storage engine, you can only specify a
	 * readConcern option of "local".TIP The serverStatus command returns the storageEngine.supportsCommittedReads
	 * field which indicates whether the storage engine supports "majority" read concern. readConcern requires MongoDB
	 * drivers updated for 3.2.
	 */
	protected static $_readConcern = null;

	/** @var string|null Read preference для данной модели */
	protected static $_readPreference = null;

	/** @var array[] Список tag set для таргетирования определенных серверов из реплики */
	protected static $_tagSet = [];

	/** @var array Индексы */
	protected static $_indexes = null;

	/** @var string Метод обновления записи - full, inc, set, cacheOnly */
	protected static $_updateMethod = self::UPDATE_METHOD_SET;

	/** @var bool Флаг, определяющий, используется ли кеш даной моделью */
	protected static $_isCacheOn = true;

	/** @var int Ограничение размера статического кеша */
	protected static $_cacheSizeLimit = 16;

	/**
	 * Является ли коллекция ограниченой по размеру
	 *
	 * @link http://docs.mongodb.org/manual/core/capped-collections/
	 * @var bool
	 */
	protected static $_capped = false;

	/**
	 * Размер ограниченой коллекции в байтах
	 *
	 * @link http://docs.mongodb.org/manual/core/capped-collections/
	 * @var int
	 */
	protected static $_cappedSize = 0;

	/**
	 * Размер ограниченой коллекции в количестве записей. Не может существовать без $cappedSize
	 *
	 * @link http://docs.mongodb.org/manual/core/capped-collections/
	 * @var int
	 */
	protected static $_cappedMax = 0;

	/**
	 * @var array
	 */
	protected static $callbacks = [];

	protected $_before = [];

	protected $_isLoadedObject = false;

	/**
	 * @param          $name
	 * @param \Closure $listener
	 */
	public static function listenMagicConfig($name, \Closure $listener)
	{
		static::$callbacks[static::getCollectionName()][$name] = $listener;
	}

	/**
	 * @param       $name
	 * @param mixed $argument
	 */
	public static function callMagicConfig($name, &$argument = [])
	{
		if (isset(static::$callbacks[static::getCollectionName()][$name])) {
			call_user_func_array(static::$callbacks[static::getCollectionName()][$name], [&$argument]);
		}
	}

	/**
	 * Возвращает read preference для установки на курсоре
	 *
	 * @return string
	 */
	public static function getReadPreference()
	{
		return static::$_readPreference !== null ? static::$_readPreference : ObjectModel::RP_SECONDARY_PREFERRED;
	}

	/**
	 * Возвращает tag set для данной модели
	 *
	 * @return array[]
	 */
	public static function getTagSet()
	{
		return static::$_tagSet;
	}

	/**
	 * @return int
	 */
	public static function getWTimeout()
	{
		return static::$_wtimeout;
	}

	/**
	 * @return bool
	 */
	public static function getJournal()
	{
		return static::$_journal;
	}

	/**
	 * Возвращает write concern для установки на курсоре
	 *
	 * @return int
	 */
	public static function getWriteConcern()
	{
		return static::$_writeConcernMode !== null ? static::$_writeConcernMode : 0;
	}

	/**
	 * @return string
	 */
	public static function getReadConcern()
	{
		return static::$_readConcern !== null ? static::$_readConcern : ReadConcern::LOCAL;
	}

	/**
	 * Устанавливает read preference либо slave okay на курсоре
	 *
	 * @param $options
	 */
	public static function configureCursorReadPreference(&$options)
	{
		static::callMagicConfig('configureCursorReadPreference', $options);
		if (!isset($options['readPreference'])) {
			$options['readPreference'] = new ReadPreference(
				static::getReadPreference(), static::getTagSet()
			);
		}
		if (!isset($options['readConcern'])) {
			$options['readConcern'] = new ReadConcern(static::getReadConcern());
		}
	}

	/**
	 * Выбираем как будем записывать, в записимости от версии
	 *
	 * @param $options
	 */
	public static function configureWriteConcernMode(&$options)
	{
		static::callMagicConfig('configureWriteConcernMode', $options);
		if (!isset($options['writeConcern'])) {
			$options['writeConcern'] = new WriteConcern(
				static::getWriteConcern(), static::getWTimeout(), static::getJournal()
			);
		}
	}

	/**
	 * Создание объекта
	 *
	 * @param mixed $id Для загрузки существующего объекта (optional)
	 */
	public function __construct($id = null)
	{
		$fieldsDefault = static::getFieldsDefault();
		$fieldsValidate = static::getFieldsValidate();

		/* Загрузка объекта из базы данных, если указан id */
		$dbobject = [];
		if (isset($id)) {
			if (!is_array($id)) {
				/*
				 * Поиск объекта по id
				 */
				$id = static::decode($id, $fieldsValidate[static::$_pk]);
				if ($this->isCached($id)) {
					$dbobject = $this->getFromCache($id);
					$this->_isLoadedObject = true;
				} else {
					$options = ['limit' => 1];
					static::configureCursorReadPreference($options);
					/** @var Collection $coll */
					$coll = static::getMongoCollection();
					$res = $coll->find([static::$_pk => $id], $options);
					/**
					 * @var \MongoDB\Model\BSONDocument $dbobject
					 */
					foreach ($res as $dbobject) {
						$this->cacheRecord($id, $dbobject);
						$this->_isLoadedObject = true;
						break;
					}
				}

				if (!$this->isLoadedObject()) {
					$dbobject[static::$_pk] = $id;
				}
			} else {
				/*
				 * Поиск объекта по массиву параметров
				 */
				$options = ['limit' => 1];
				static::configureCursorReadPreference($options);
				/** @var Collection $coll */
				$coll = static::getMongoCollection();
				$res = $coll->find($id, $options);
				foreach ($res as $dbobject) {
					$id = $dbobject[static::$_pk];
					$this->cacheRecord($id, $dbobject);
					$this->_isLoadedObject = true;
					break;
				}

				if (!$this->isLoadedObject()) {
					foreach ($id as $field => $value) {
						$dbobject[$field] = $value;
					}
				}
			}
		}

		foreach ($fieldsDefault as $field => $value) {
			if ($fieldsValidate[$field] == self::TYPE_MONGO_DATE && !isset($dbobject[$field])) {
				$value = static::decode($value, self::TYPE_MONGO_DATE);
			}
			if ($fieldsValidate[$field] == self::TYPE_MONGO_TIMESTAMP && !isset($dbobject[$field])) {
				$value = static::decode($value, self::TYPE_MONGO_TIMESTAMP);
			}
			$this->{$field} = isset($dbobject[$field]) ? $dbobject[$field] : $value;
		}

		$this->_before = $this->getFieldsArray();
	}

	/**
	 * Проверяет, закеширована ли запись с данными ID
	 *
	 * @param mixed $id
	 *
	 * @return bool
	 */
	protected function isCached($id)
	{
		return isset(static::$_cache[static::getCollectionName()][(string)$id]);
	}

	/**
	 * Кеширует запись с заданными ID
	 *
	 * @param mixed $id
	 * @param array $dbObject
	 */
	protected function cacheRecord($id, array $dbObject)
	{
		if (!static::$_isCacheOn) {
			return;
		}

		if (!isset(static::$_cache[static::getCollectionName()])) {
			static::resetCache();
		}

		if (count(static::$_cache[static::getCollectionName()]) >= static::$_cacheSizeLimit) {
			array_shift(static::$_cache[static::getCollectionName()]);
		}
		static::$_cache[static::getCollectionName()][(string)$id] = $dbObject;
	}

	/**
	 * Возвращает запись из кеша
	 *
	 * @param mixed $id
	 *
	 * @return array NULL, если запись не закеширована
	 */
	protected function getFromCache($id)
	{
		return isset(static::$_cache[static::getCollectionName()][(string)$id]) ? static::$_cache[static::getCollectionName()][(string)$id] : null;
	}

	/**
	 * Устраняет запись из кеша
	 *
	 * @param mixed $id
	 */
	protected function uncacheRecord($id)
	{
		if (isset(static::$_cache[static::getCollectionName()][(string)$id])) {
			unset(static::$_cache[static::getCollectionName()][(string)$id]);
		}
	}

	/**
	 * Устраняет все записи из кеша
	 */
	protected static function resetCache()
	{
		static::$_cache[static::getCollectionName()] = [];
	}

	/**
	 * Включает кеширование для данной модели
	 */
	public static function enableCache()
	{
		static::$_isCacheOn = true;
	}

	/**
	 * Отключает кеширование для данной модели
	 */
	public static function disableCache()
	{
		static::$_isCacheOn = false;
	}

	/**
	 * @param $oldPk
	 * @param $newPk
	 *
	 * @return bool Returns true if successful and return false if not found else return exception
	 * @throws DBException
	 */
	public static function renamePk($oldPk, $newPk)
	{
		/** @var Collection $coll */
		$coll = static::getMongoCollection();
		static::configureCursorReadPreference($options);
		$cursor = $coll->find([static::$_pk => $oldPk], $options);
		$success = false;
		foreach ($cursor as $data) {
			$oldPk = $data[static::$_pk];
			$data[static::$_pk] = $newPk;

			$options = ['upsert' => true];
			//static::configureWriteConcernMode($options);
			$options['writeConcern'] = new WriteConcern(1);
			$res = $coll->updateOne([static::$_pk => $newPk], ['$set' => $data], $options);
			if ($res->isAcknowledged() ||
				$res->getMatchedCount() > 0 ||
				$res->getUpsertedCount() > 0 ||
				$res->getModifiedCount() > 0 ||
				$res === true
			) {
				$res = $coll->deleteOne([static::$_pk => $oldPk], $options);
				if ($res->isAcknowledged() || $res->getDeletedCount() > 0 || $res === true) {
					$success = true;
				} else {
					throw new DBException('Don`t remove old field: ' . json_encode($res));
				}
			} else {
				throw new DBException('Don`t save new field: ' . json_encode($res));
			}
		}

		return $success;
	}

	/**
	 * @param $documents
	 * @param $options
	 *
	 * @return mixed
	 */
	public static function insertMany($documents, array $options = [])
	{
		static::configureWriteConcernMode($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();
		$result = $coll->insertMany($documents, $options);

		return $result;
	}

	/**
	 * @param       $data
	 * @param array $options
	 */
	public static function insertOne($data, array $options = [])
	{
		static::insert($data, $options);
	}

	/**
	 * @param $data
	 * @param $options
	 *
	 * @return array|bool
	 */
	public static function insert($data, array $options = [])
	{
		static::configureWriteConcernMode($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();
		$result = $coll->insertOne($data, $options);

		return $result;
	}

	/**
	 * @param       $params
	 * @param       $data
	 * @param array $options
	 *
	 * @return bool
	 */
	public static function updateMany($params, $data, array $options = [])
	{
		return static::update($params, $data, $options);
	}

	/**
	 * @param       $params
	 * @param       $data
	 * @param array $options
	 *
	 * @return \MongoDB\UpdateResult
	 */
	public static function updateOne($params, $data, array $options = [])
	{
		static::configureWriteConcernMode($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();
		$result = $coll->updateOne($params, $data, $options);

		return $result;
	}

	/**
	 * @param       $params
	 * @param       $data
	 * @param array $options
	 *
	 * @return bool
	 */
	public static function update($params, $data, array $options = [])
	{
		static::configureWriteConcernMode($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();
		$result = $coll->updateMany($params, $data, $options);

		return $result;
	}

	/**
	 * Сохранение текущего объекта в БД (добавление или обновление)
	 *
	 * @param string|null $updateMethod
	 *
	 * @return boolean Результат сохранения
	 */
	public function save($updateMethod = null)
	{
		if ($updateMethod !== null) {
			$oldUpdateMethod = static::setUpdateMethod($updateMethod);
		}
		$result = (empty($this->{static::$_pk})) ? $this->addCurrent() : $this->updateCurrent();
		if ($updateMethod !== null) {
			static::setUpdateMethod($oldUpdateMethod);
		}

		return $result;
	}

	/**
	 * Добавление текущего объекта в БД
	 *
	 * @param bool $autoid
	 *
	 * @return boolean Результат добавления
	 */
	public function addCurrent($autoid = true)
	{
		$fieldsValidate = static::getFieldsValidate();
		//~ echo "Adding to db\n";
		if ($autoid &&
			($fieldsValidate[static::$_pk] == self::TYPE_INT ||
				$fieldsValidate[static::$_pk] == self::TYPE_UNSIGNED_INT)
		) {
			$maxid = 0;
			$options = ['limit' => 1, 'sort' => static::getRevSortOrder()];
			/** @var Collection $coll */
			static::configureCursorReadPreference($options);
			$coll = static::getMongoCollection();
			$res = $coll->find([static::$_pk => ['$gt' => 0]], $options);
			foreach ($res as $row) {
				$maxid = $row[static::$_pk];
				//echo $maxid;
				break;
			}
			$maxid++;
			//echo $maxid;
			$this->{static::$_pk} = $maxid;
		}
		if ($autoid && $fieldsValidate[static::$_pk] == self::TYPE_MONGO_ID) {
			$this->{static::$_pk} = static::getMongoId();
		}
		$dbobject = $this->getFieldsArray();
		$options = [];
		static::configureWriteConcernMode($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();
		//var_dump($dbobject);ob_flush();
		$result = $coll->insertOne($dbobject, $options);
		$this->_before = $this->getFieldsArray();
		$this->_isLoadedObject = true;

		return $result;
	}

	/**
	 * Обновление текущего объекта в БД
	 *
	 * @return boolean Результат обновления
	 */
	public function updateCurrent()
	{
		$dbobject = $this->getFieldsArray();

		$method = static::$_updateMethod;
		//$method = 'inc';
		//$method = 'set';
		//$method = 'full';

		$options = ['upsert' => true];
		static::configureWriteConcernMode($options);

		$update = [];
		$replace = [];

		$fieldsValidate = static::getFieldsValidate();
		if ($this->isLoadedObject()) {
			switch ($method) {
				case self::UPDATE_METHOD_INC:
					$diff = ArrayHelper::changed($this->_before, $dbobject);
					$incDiff = ArrayHelper::incDiff($this->_before, $dbobject);
					unset($diff['_id']);

					if (!empty($diff)) {
						$set = [];
						$inc = [];
						foreach ($diff as $k => $v) {
							if (isset($incDiff[$k]) && $fieldsValidate[$k] != self::TYPE_TIMESTAMP) {
								$inc[$k] = $incDiff[$k];
							} else {
								$set[$k] = $dbobject[$k];
							}
						}
						if (!empty($set) && !empty($inc)) {
							$update = ['$set' => $set, '$inc' => $inc];
						} elseif (!empty($set)) {
							$update = ['$set' => $set];
						} elseif (!empty($inc)) {
							$update = ['$inc' => $inc];
						}
						//echo (string) $this->{static::$_pk};
						//print_r($update);
					}
					break;
				case self::UPDATE_METHOD_SET:
					$diff = ArrayHelper::changed($this->_before, $dbobject);
					unset($diff['_id']);
					if (!empty($diff)) {
						$update = ['$set' => $diff];
					}
					//print_r($update);
					break;
				case self::UPDATE_METHOD_CACHE_ONLY:
					$update = [];
					break;
				case self::UPDATE_METHOD_FULL:
				default:
					$replace = $dbobject;
					break;
			}
		} else {
			$update = ['$set' => $dbobject];
		}

		if (!empty($update)) {
			//var_dump($update);ob_flush();
			/** @var Collection $coll */
			$coll = static::getMongoCollection();
			$result = $coll->updateOne(
				[static::$_pk => $this->{static::$_pk}],
				$update,
				$options
			);
		} elseif (!empty($replace)) {
			/** @var Collection $coll */
			$coll = static::getMongoCollection();
			$result = $coll->replaceOne(
				[static::$_pk => $this->{static::$_pk}],
				$replace,
				$options
			);
		} else {
			$result = true;
		}

		$this->cacheRecord($this->{static::$_pk}, $dbobject);
		$this->_before = $this->getFieldsArray();
		$this->_isLoadedObject = true;

		return $result;
	}

	/**
	 * Удаление текущего объекта из БД
	 *
	 * @param       $params
	 * @param array $options
	 *
	 * @return bool Результат удаления
	 */
	public static function deleteOne($params, array $options = [])
	{
		static::configureWriteConcernMode($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();

		return $coll->deleteOne($params, $options);
	}

	/**
	 * Удаление текущего объекта из БД
	 *
	 * @param       $params
	 * @param array $options
	 *
	 * @return bool Результат удаления
	 */
	public static function deleteMany($params, array $options = [])
	{
		return static::deleteAllAdvanced($params, $options);
	}

	/**
	 * @param       $params
	 * @param array $options
	 *
	 * @return mixed
	 */
	public static function remove($params, array $options = [])
	{
		return static::remove($params, $options);
	}

	/**
	 * Удаление текущего объекта из БД
	 *
	 * @param array $options
	 *
	 * @return bool Результат удаления
	 */
	public function delete(array $options = [])
	{
		$this->uncacheRecord($this->{static::$_pk});
		static::configureWriteConcernMode($options);

		/** @var Collection $coll */
		$coll = static::getMongoCollection();
		$this->_before = [];
		$this->_isLoadedObject = false;

		return $coll->deleteOne([static::$_pk => $this->{static::$_pk}], $options);
	}

	/**
	 * Удаление всех объектов из БД
	 *
	 * @return boolean Результат удаления
	 */
	public static function deleteAll()
	{
		static::resetCache();
		if (static::$_capped) {
			/** @var Database $db */
			$db = static::getMongoDatabase();
			$db->command(
				[
					'emptycapped' => static::getCollectionName(),
				]
			);
		} else {
			$options = [];
			static::configureWriteConcernMode($options);
			/** @var Collection $coll */
			$coll = static::getMongoCollection();
			$coll->deleteMany([], $options);
		}
	}

	/**
	 * Удаление объектов удовлетворяющих условия
	 *
	 * @param       $params - условия
	 * @param array $options
	 *
	 * @return bool Результат удаления
	 */
	public static function deleteAllAdvanced($params, array $options = [])
	{
		static::resetCache();
		static::configureWriteConcernMode($options);

		/** @var Collection $coll */
		$coll = static::getMongoCollection();

		return $coll->deleteMany($params, $options);
	}

	/**
	 * Излекает поля объекта в массив, пригодный для сохранения
	 *
	 * @return array Массив полей объекта
	 */
	public function getFieldsArray()
	{
		$result = array_intersect_key(
			(array)$this,
			array_flip(array_keys(static::getFieldsDefault()))
		);

		return $result;
	}

	/**
	 * Заполняет поля объекта из массива
	 *
	 * @param array $fields - массив, из которого происходит заполнение
	 */
	public function setFieldsArray($fields)
	{
		foreach (static::getFieldsDefault() as $field => $value) {
			if (isset($fields[$field])) {
				$this->{$field} = $fields[$field];
			}
		}
	}

	/**
	 * Излекает поля объекта в массив, пригодный для отправки клиенту
	 *
	 * @return array Массив полей объекта
	 */
	public function getFieldsSanitized()
	{
		$fields = array_diff(
			array_keys(static::getFieldsDefault()),
			array_keys(static::getFieldsPrivate())
		);
		$result = array_intersect_key((array)$this, array_flip($fields));

		return $result;
	}

	/**
	 * Возвращает количество объектов в БД
	 *
	 * @param array $params
	 * @param array $options
	 *
	 * @return int значение
	 */
	public static function count($params = [], array $options = [])
	{
		static::configureCursorReadPreference($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();
		if (!empty($params)) {
			return $coll->count($params, $options);
		} else {
			return $coll->count([], $options);
		}
	}

	/**
	 * @param int   $page
	 * @param int   $per_page
	 * @param array $params
	 * @param bool  $sort
	 * @param bool  $onlycursor
	 * @param array $options
	 *
	 * @return array
	 */
	public static function getList($page = 0,
	                               $per_page = 10,
	                               $params = [],
	                               $sort = false,
	                               $onlycursor = false,
	                               array $options = [])
	{
		return static::getAllAdvanced(
			$params,
			[],
			false,
			$sort,
			$page,
			$per_page,
			$onlycursor,
			$options
		);
	}

	/**
	 * @param array $params
	 * @param bool  $sort
	 * @param bool  $onlycursor
	 * @param array $options
	 *
	 * @return array
	 */
	public static function getAll($params = [], $sort = false, $onlycursor = false, array $options = [])
	{
		return static::getAllAdvanced(
			$params,
			[],
			false,
			$sort,
			false,
			false,
			$onlycursor,
			$options
		);
	}

	/**
	 * @param array $params
	 *
	 * @return \Generator
	 */
	public static function getNewObjects($params = [], $options = [])
	{
		/** Поиск объекта по массиву параметров */
		static::configureCursorReadPreference($options);
		$res = static::getMongoCollection()->find($params, $options);
		foreach ($res as $dbobject) {
			$id = $dbobject[static::$_pk];
			if (!isset(static::$_cache[static::getCollectionName()])) {
				static::resetCache();
			}
			if (count(static::$_cache[static::getCollectionName()]) >= static::$_cacheSizeLimit) {
				array_shift(static::$_cache[static::getCollectionName()]);
			}
			static::$_cache[static::getCollectionName()][(string)$id] = $dbobject;
			yield $id => new static($id);
			if (!static::$_isCacheOn) {
				unset(static::$_cache[static::getCollectionName()][(string)$id]);
			}
		}
	}

	/**
	 * @param array $dbobject
	 *
	 * @return $this
	 */
	public static function getNewObject(Array $dbobject = [])
	{
		$id = $dbobject[static::$_pk];
		if (!isset(static::$_cache[static::getCollectionName()])) {
			static::resetCache();
		}
		if (count(static::$_cache[static::getCollectionName()]) >= static::$_cacheSizeLimit) {
			array_shift(static::$_cache[static::getCollectionName()]);
		}
		static::$_cache[static::getCollectionName()][(string)$id] = $dbobject;
		$object = new static($id);
		if (!static::$_isCacheOn) {
			unset(static::$_cache[static::getCollectionName()][(string)$id]);
		}

		return $object;
	}

	/**
	 * Делает выборку объектов
	 *
	 * @param array      $params     массив условий выборки
	 * @param array      $fields
	 * @param bool       $sanitize   подчищать ли вывод для отдачи клиенту
	 * @param bool|array $sort       true/false - использовать ли параметры сортировки по умолчанию, либо массив
	 *                               параметров сортировки
	 * @param bool|int   $page       если не false, то определяет объекты с какой страницы возвращать, начиная с 0
	 * @param bool|int   $per_page   если не false, то определяет по сколько возвращать объектов на страницу
	 * @param bool       $onlycursor вернуть не объекты, а MongoCursor
	 * @param array      $options
	 *
	 * @return array значения по умолчанию полей
	 */
	public static function getAllAdvanced($params,
	                                      $fields = [],
	                                      $sanitize = false,
	                                      $sort = false,
	                                      $page = false,
	                                      $per_page = false,
	                                      $onlycursor = false,
	                                      array $options = [])
	{
		static::configureCursorReadPreference($options);

		/** @var Collection $coll */
		$coll = static::getMongoCollection();

		if ($sort === true) {
			$options['sort'] = static::getSortOrder();
		} elseif (is_array($sort)) {
			$options['sort'] = $sort;
		}
		if ($per_page !== false && $page !== false) {
			$options['limit'] = $per_page;
			$options['skip'] = $page * $per_page;
		}
		if (!empty($fields)) {
			$options['projection'] = $fields;
		}

		$res = $coll->find($params, $options);
		if ($onlycursor) {
			return $res;
		}

		$objects = [];
		$fieldsDefault = static::getFieldsDefault();
		$fieldsPrivate = static::getFieldsPrivate();
		$fieldsValidate = static::getFieldsValidate();

		foreach ($res as $dbobject) {
			$object = [];
			foreach ($fieldsDefault as $field => $value) {
				if ((!$sanitize || !isset($fieldsPrivate[$field])) && (empty($fields) || isset($fields[$field]))) {
					if ($sanitize) {
						$rule = $fieldsValidate[$field];
						$object[$field] = ObjectModel::encode($value, $rule);
					}

					$object[$field] = isset($dbobject[$field]) ? $dbobject[$field] : $value;
				}
			}
			//$object['_id'] = $dbobject['_id'];
			$objects[] = $object;
			//var_dump($object);
			//ob_flush();
		}
		//var_dump($objects);
		//ob_flush();

		//print_r($res->info());
		return $objects;
	}

	/**
	 * @param array $pipeline
	 * @param array $options
	 *
	 * @return mixed
	 */
	public static function aggregate(array $pipeline, array $options = [])
	{
		$options['useCursor'] = true;
		static::configureCursorReadPreference($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();

		return $coll->aggregate($pipeline, $options);
	}

	/**
	 * @param array $pipeline
	 * @param array $options
	 *
	 * @return mixed
	 */
	public static function aggregateWithoutCursor(array $pipeline, array $options = [])
	{
		$options['useCursor'] = false;
		static::configureCursorReadPreference($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();

		return $coll->aggregate($pipeline, $options);
	}

	/**
	 * Querys this collection, returning a MongoCursor for the result set
	 *
	 * @see MongoCollection::find
	 *
	 * @param array $query
	 * @param array $fields
	 * @param array $options
	 *
	 * @return Cursor
	 */
	public static function find(array $query = [], array $fields = [], array $options = [])
	{
		static::configureCursorReadPreference($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();
		if (!empty($fields)) {
			$options['projection'] = $fields;
		}

		return $coll->find($query, $options);
	}

	/**
	 * Querys this collection, returning a single element
	 *
	 * @see MongoCollection::findOne
	 *
	 * @param array $query
	 * @param array $options
	 *
	 * @return array|null if it cannot reach the database.
	 */
	public static function findOne(array $query = [], array $options = [])
	{
		static::configureCursorReadPreference($options);
		/** @var Collection $coll */
		$coll = static::getMongoCollection();

		return $coll->findOne($query, $options);
	}

	/**
	 * Update a document and return it
	 *
	 * @param array $query
	 * @param array $update
	 * @param array $fields
	 * @param array $options
	 *
	 * @return mixed
	 */
	public static function findAndModify(array $query = [], array $update = [], array $fields = [], array $options = [])
	{
		static::configureCursorReadPreference($options);
		static::configureWriteConcernMode($options);
		/** @var Collection $collection */
		$coll = static::getMongoCollection();
		if (!empty($fields)) {
			$options['projection'] = $fields;
		}

		return (array)$coll->findOneAndUpdate($query, $update, $options);
	}

	/**
	 * @param array $query
	 * @param array $update
	 * @param array $fields
	 * @param array $options
	 *
	 * @return mixed
	 */
	public static function findOneAndUpdate(array $query = [],
	                                        array $update = [],
	                                        array $fields = [],
	                                        array $options = [])
	{
		return static::findAndModify($query, $update, $fields, $options);
	}

	/**
	 * @param array $query
	 * @param array $replace
	 * @param array $fields
	 * @param array $options
	 *
	 * @return array
	 */
	public static function findOneAndReplace(array $query = [],
	                                         array $replace = [],
	                                         array $fields = [],
	                                         array $options = [])
	{
		static::configureCursorReadPreference($options);
		static::configureWriteConcernMode($options);
		/** @var Collection $collection */
		$coll = static::getMongoCollection();
		if (!empty($fields)) {
			$options['projection'] = $fields;
		}

		return (array)$coll->findOneAndReplace($query, $replace, $options);
	}

	/**
	 * @param array $query
	 * @param array $fields
	 * @param array $options
	 *
	 * @return array
	 */
	public static function findOneAndDelete(array $query = [], array $fields = [], array $options = [])
	{
		static::configureCursorReadPreference($options);
		static::configureWriteConcernMode($options);
		/** @var Collection $collection */
		$coll = static::getMongoCollection();
		if (!empty($fields)) {
			$options['projection'] = $fields;
		}

		return (array)$coll->findOneAndDelete($query, $options);
	}

	/**
	 * Возвращает значения по умолчанию полей
	 *
	 * @return array значения по умолчанию полей
	 */
	public static function getFieldsDefault()
	{
		$current = get_called_class();
		if (array_search($current . 'fieldsDefault', static::$_fieldMerged) === false) {
			static::$_fieldsDefault = static::mergeField('getFieldsDefault', static::$_fieldsDefault);
			static::$_fieldMerged[] = $current . 'fieldsDefault';
		}

		return static::$_fieldsDefault;
	}

	/**
	 * Возвращает правила валидации полей
	 *
	 * @return array правила валидации полей
	 */
	public static function getFieldsValidate()
	{
		$current = get_called_class();
		if (array_search($current . 'fieldsValidate', static::$_fieldMerged) === false) {
			static::$_fieldsValidate = static::mergeField('getFieldsValidate', static::$_fieldsValidate);
			static::$_fieldMerged[] = $current . 'fieldsValidate';
		}

		return static::$_fieldsValidate;
	}

	/**
	 * Возвращает правила валидации полей
	 *
	 * @return array правила валидации полей
	 */
	public static function getFieldsPrivate()
	{
		$current = get_called_class();
		if (array_search($current . 'fieldsPrivate', static::$_fieldMerged) === false) {
			static::$_fieldsPrivate = static::mergeField('getFieldsPrivate', static::$_fieldsPrivate);
			static::$_fieldMerged[] = $current . 'fieldsPrivate';
		}

		return static::$_fieldsPrivate;
	}

	/**
	 * Возвращает хинты полей
	 *
	 * @return array хинты полей
	 */
	public static function getFieldsHint()
	{
		$current = get_called_class();
		if (array_search($current . 'fieldsHint', static::$_fieldMerged) === false) {
			static::$_fieldsHint = static::mergeField('getFieldsHint', static::$_fieldsHint);
			static::$_fieldMerged[] = $current . 'fieldsHint';
		}

		return static::$_fieldsHint;
	}

	/**
	 * Получаем поля, которые были замережены уже
	 *
	 * @return array
	 */
	public static function getMergedField()
	{
		return static::$_fieldMerged;
	}

	/**
	 * Рекурсивно мержит свойство с типом Array во всей иерархии наследования
	 *
	 * @param       $funcName - название метода
	 * @param array $data     - данные свойства
	 *
	 * @return array
	 */
	protected static function mergeField($funcName, $data)
	{
		$current = get_called_class();
		$parent = get_parent_class($current);
		if ($parent && !empty($parent)) {
			return array_merge($parent::$funcName(), $data);
		} else {
			return $data;
		}
	}

	/**
	 * Возвращает имя ключегово поля объета
	 *
	 * @return string имя ключевого поля
	 */
	public static function getPK()
	{
		return static::$_pk;
	}

	/**
	 * Возвращает параметры сортировки коллекции для монго
	 *
	 * @return array параметры сортировки коллекции
	 */
	public static function getSortOrder()
	{
		if (static::$_sort != null) {
			return static::$_sort;
		} else {
			return [static::$_pk => 1];
		}
	}

	/**
	 * Возвращает параметры обратной сортировки коллекции для монго
	 *
	 * @return array параметры обратной сортировки коллекции
	 */
	public static function getRevSortOrder()
	{
		if (static::$_rsort != null) {
			return static::$_rsort;
		} else {
			return [static::$_pk => -1];
		}
	}

	/**
	 * Возвращает mongo-коллекцию связанную с объектом
	 *
	 * @param array $options
	 *
	 * @return Collection mongo-коллекция
	 */
	public static function getMongoCollection(Array $options = [])
	{
		static::configureCursorReadPreference($options);
		static::configureWriteConcernMode($options);
		/** @var Database $db */
		$db = static::getMongoDatabase();

		return $db->selectCollection(static::getCollectionName(), $options);
	}

	/**
	 * Очищает статический кеш
	 */
	public static function clearStaticCache()
	{
		static::$_cache = [];
	}

	/**
	 * Устанавливает параметр updateMethod
	 *
	 * @param string $value
	 *
	 * @return string Метод обновления, установленный ранее
	 */
	public static function setUpdateMethod($value)
	{
		$oldVal = static::$_updateMethod;
		static::$_updateMethod = $value;

		return $oldVal;
	}

	/**
	 * Получает параметр updateMethod
	 */
	public static function getUpdateMethod()
	{
		static::callMagicConfig('getUpdateMethod', static::$_updateMethod);

		return static::$_updateMethod;
	}

	/**
	 * Проверка на capped коллекцию
	 */
	public static function checkCappedCollection()
	{
		if (static::$_capped && (static::$_cappedSize > 0 || static::$_cappedMax > 0)) {
			$db = static::getMongoDatabase();
			$validate = $db->command(
				[
					'validate' => static::getCollectionName(),
					'full'     => false,
				]
			)->toArray()[0];

			if (static::$_cappedSize > 0 && static::$_cappedMax > 0 && $validate['ok'] == 0) { // Если коллекции нет, создаем
				$db->command(
					[
						'create' => static::getCollectionName(),
						'capped' => true,
						'size'   => static::$_cappedSize,
						'max'    => static::$_cappedMax,
					]
				);
			} elseif (static::$_cappedSize > 0 && static::$_cappedMax == 0 && $validate['ok'] == 0) { // Если коллекции нет, создаем
				$db->command(
					['create' => static::getCollectionName(), 'capped' => true, 'size' => static::$_cappedSize]
				);
			} elseif (static::$_cappedSize > 0 &&
				static::$_cappedMax == 0 &&
				(!isset($validate['capped']) || $validate['capped'] === false)
			) { // Если коллекция существует, и не задана как capped, и нужно утсановить ограничение только по размеру - конвертируем
				$db->command(['size' => static::$_cappedSize, 'convertToCapped' => static::getCollectionName()]);
			}
		}
	}

	public static function ensureIndexes()
	{
		static::createIndex();
	}

	public static function getPrepareIndexes()
	{
		$indexes = [];
		if (is_array(static::$_indexes) && !empty(static::$_indexes)) {
			foreach (static::$_indexes as $index) {
				if (isset($index['keys'])) {
					$index['key'] = $index['keys'];
					unset($index['keys']);
				}
				if (isset($index['key']['_id'])) {
					unset($index['key']['_id']);
				}

				if (isset($index['options'])) {
					$index += $index['options'];
					unset($index['options']);
				}

				$index['background'] = true;

				if (isset($index['key']) && !empty($index['key'])) {
					$indexes[] = $index;
				}
			}
		}

		return $indexes;
	}

	/**
	 * Проверяет и устанавливает индексы для коллекции
	 */
	public static function createIndex()
	{
		/** @var Collection $coll */
		$coll = static::getMongoCollection();

		$indexes = static::getPrepareIndexes();

		if (!empty($indexes)) {
			$coll->createIndexes($indexes);
		}
	}

	/**
	 * Проверяет и устанавливает индексы для коллекции
	 */
	public static function checkIndexes()
	{
		$indexes = static::$_indexes??[];
		$existing = static::getExistingIndexes();
		$ok = true;
		foreach ($indexes as &$index) {
			$index['exists'] = false;
			foreach ($existing as $e_index) {
				if ($e_index['key'] === $index['keys']) {
					$index['exists'] = true;
					break;
				}
			}
			if (!$index['exists']) {
				$ok = false;
			}
		}

		return ['ok' => $ok, 'indexes' => $indexes];
	}

	/**
	 * Получает индексы для коллекции
	 */
	public static function getExistingIndexes()
	{
		/** @var Collection $coll */
		$coll = static::getMongoCollection();

		return $coll->listIndexes();
	}

	/**
	 * Получает индексы для коллекции
	 */
	public static function getIndexes()
	{
		return static::$_indexes;
	}

	/**
	 * Удаляет индекс из коллекции
	 *
	 * @param $index_name
	 *
	 * @return bool
	 */
	public static function deleteIndex($index_name)
	{
		$res = false;
		if (!empty($index_name) && $index_name != '_id_') {
			/** @var Collection $coll */
			$coll = static::getMongoCollection();
			$coll->dropIndex($index_name);
		}

		return !empty($res);
	}

	/**
	 *
	 * @return array|object
	 */
	public static function drop()
	{
		static::resetCache();

		/** @var Collection $coll */
		$coll = static::getMongoCollection();

		return $coll->drop();
	}

	/**
	 * Возвращает конфиг базы
	 *
	 * @return array
	 */
	public static function getMongoConfig()
	{
		return MongoDBDatabase::get()->getConfig();
	}

	/**
	 * Возвращает экземляр объекта MongoDB
	 *
	 * @param string $name
	 *
	 * @return Database
	 */
	public static function getMongoDatabase($name = '')
	{
		return MongoDBDatabase::getInstance($name);
	}

	/**
	 * Возвращает экземляр объекта Mongo
	 *
	 * @param string $name
	 *
	 * @return Client
	 */
	public static function getMongo($name = '')
	{
		return MongoDBDatabase::getInstanceDriver($name);
	}

	/**
	 * Возвращает название коллекции
	 *
	 * @return string
	 */
	public static function getCollectionName()
	{
		if (static::$_hasPrefix === true) {
			$prefix = Config::getInstance()->get(['prefix'], '');

			return $prefix . static::$_collection;
		}
		if (static::$_hasPostfix === true) {
			$postfix = Config::getInstance()->get(['postfix'], '');

			return static::$_collection . $postfix;
		}

		return static::$_collection;
	}

	/**
	 * @return bool
	 */
	public static function hasPrefix()
	{
		return static::$_hasPrefix;
	}

	/**
	 * @return bool
	 */
	public static function hasPostfix()
	{
		return static::$_hasPostfix;
	}

	/**
	 * Возвращает уникальные значения ключа
	 *
	 * @param string $key   ключ
	 * @param array  $query необязательные параметры ограничения выборки
	 *
	 * @param array  $options
	 *
	 * @return array
	 */
	public static function distinct($key, $query = [], $options = [])
	{
		$collection = static::getMongoCollection();
		static::configureCursorReadPreference($options);
		$res = $collection->distinct($key, $query, $options);

		return $res;
	}

	public function addIndex($index, $options)
	{
		$collection = static::getMongoCollection();
		$collection->createIndex($index, $options);
	}

	/**
	 * @param array $options
	 * @param array ...$fields
	 */
	public static function createFullTextIndex($options = [], ...$fields)
	{
		$collection = static::getMongoCollection();

		$index = [];
		foreach ($fields as $field) {
			$index[$field] = 'text';
		}

		if (!empty($index)) {
			$collection->createIndex($index, $options);
		}
	}

	public static function createGeoIndex($field, $sphere = true, $options = [])
	{
		$collection = static::getMongoCollection();

		if ($sphere) {
			$index[$field] = '2dsphere';
		} else {
			$index[$field] = '2d';
		}

		if (!empty($index)) {
			$collection->createIndex($index, $options);
		}
	}

	/**
	 *
	 * @param       $text
	 * @param null  $scoreMeta
	 * @param null  $language
	 * @param null  $caseSensitive
	 * @param null  $diacriticSensitive
	 * @param array $params
	 * @param array $fields
	 * @param bool  $sanitize
	 * @param bool  $sort
	 * @param bool  $page
	 * @param bool  $per_page
	 * @param bool  $onlycursor
	 * @param array $options
	 *
	 * @param bool  $sortByTextScoreMeta
	 *
	 * @return array
	 * @internal param null $score
	 */
	public static function search($text,
	                              $scoreMeta = null,
	                              $language = null,
	                              $caseSensitive = null,
	                              $diacriticSensitive = null,
	                              $params = [],
	                              $fields = [],
	                              $sanitize = false,
	                              $sort = false,
	                              $page = false,
	                              $per_page = false,
	                              $onlycursor = false,
	                              array $options = [],
	                              $sortByTextScoreMeta = false)
	{
		$params['$text']['$search'] = $text;
		if ($language) {
			$params['$text']['$language'] = $language;
		}

		if ($caseSensitive) {
			$params['$text']['$caseSensitive'] = $caseSensitive;
		}
		if ($diacriticSensitive) {
			$params['$text']['$diacriticSensitive'] = $diacriticSensitive;
		}

		if ($sortByTextScoreMeta) {
			$options['score']['$meta'] = 'textScore';
			$sort['$meta'] = 'textScore';
		} elseif ($scoreMeta) {
			$options['score']['$meta'] = $scoreMeta;
		}

		return static::getAllAdvanced($params, $fields, $sanitize, $sort, $page, $per_page, $onlycursor, $options);
	}

	public static function getGeoSearch(GeospatialQueryOperators $query,
	                                    $params = [],
	                                    $fields = [],
	                                    $sanitize = false,
	                                    $sort = false,
	                                    $page = false,
	                                    $per_page = false,
	                                    $onlycursor = false,
	                                    array $options = [])
	{
		$queries = $query->getAll();
		foreach ($queries as $geoQueries) {
			foreach ($geoQueries as $field => $query) {
				if (isset($params[$field])) {
					$params[$field] += $query;
				} else {
					$params[$field] = $query;
				}
			}
		}

		return static::getAllAdvanced($params, $fields, $sanitize, $sort, $page, $per_page, $onlycursor, $options);
	}

	/**
	 * Проверка загружены ли данные с БД
	 *
	 * @return boolean
	 */
	public function isLoadedObject()
	{
		return $this->_isLoadedObject;
	}

	/**
	 * Декодировать данные в нужный тип
	 *
	 * @param string $val - данные для кодирования
	 * @param        $rule
	 *
	 * @return mixed $rule тип данных
	 */
	public static function decode($val, $rule)
	{
		switch ($rule) {
			case ObjectModel::TYPE_UNSIGNED_INT:
			case ObjectModel::TYPE_INT:
				$val = (int)$val;
				break;
			case ObjectModel::TYPE_UNSIGNED_FLOAT:
			case ObjectModel::TYPE_FLOAT:
				$val = (float)$val;
				break;
			case ObjectModel::TYPE_DECIMAL:
				if (class_exists('\MongoDB\BSON\Decimal128')) {
					$val = new \MongoDB\BSON\Decimal128((string)$val);
				} else {
					$val = (float)$val;
				}
				break;
			case ObjectModel::TYPE_BOOL:
				if ($val === 'true') {
					$val = true;
				}
				if ($val === 'false') {
					$val = false;
				}
				$val = (bool)$val;
				break;
			case ObjectModel::TYPE_TIMESTAMP:
				if ($val == '0') {
					$val = 0;
				} else {
					$date = new DateTime($val);
					$val = $date->getTimestamp();
				}
				break;
			case ObjectModel::TYPE_JSON:
				if (is_string($val)) {
					$val = json_decode($val, true);
				} else {
					$val = (array)$val;
				}
				break;
			case ObjectModel::TYPE_MONGO_ID:
				$val = static::getMongoId($val);
				break;
			case ObjectModel::TYPE_MONGO_DATE:
				if ($val == '0') {
					$timestamp = 0;
				} else {
					$timestamp = (new DateTime($val))->getTimestamp();
				}
				$val = static::getMongoDate($timestamp);
				break;
			case ObjectModel::TYPE_MONGO_TIMESTAMP:
				$val = static::getMongoTimestamp($val);
				break;
			case ObjectModel::TYPE_TEXT:
			case ObjectModel::TYPE_HTML:
			case ObjectModel::TYPE_MARKDOWN:
			case ObjectModel::TYPE_STRING:
			case ObjectModel::TYPE_UID:
				$val = (string)$val;
				break;
		}

		return $val;
	}

	public static function encode($val, $rule)
	{
		switch ($rule) {
			case ObjectModel::TYPE_UNSIGNED_INT:
			case ObjectModel::TYPE_INT:
				$val = (int)$val;
				break;
			case ObjectModel::TYPE_UNSIGNED_FLOAT:
			case ObjectModel::TYPE_FLOAT:
				$val = (float)$val;
				break;
			case ObjectModel::TYPE_DECIMAL:
				$val = (string)$val;
				break;
			case ObjectModel::TYPE_BOOL:
				if ($val) {
					$val = 'true';
				} else {
					$val = 'false';
				}
				break;
			case ObjectModel::TYPE_TIMESTAMP:
				$date = new DateTime();
				$date->setTimestamp($val);
				$val = $date->format('Y-m-d H:i:s');
				break;
			case ObjectModel::TYPE_JSON:
				if (is_object($val)) {
					$val = (array)$val;
				}
				$val = json_encode($val, JSON_UNESCAPED_UNICODE);
				break;
			case ObjectModel::TYPE_MONGO_ID:
				$val = (string)$val;
				break;
			case ObjectModel::TYPE_MONGO_DATE:
				$date = static::getMongoDate($val);
				/** @var \DateTime $dateTime */
				$dateTime = $date->toDateTime()->setTimezone(
					new \DateTimeZone(date_default_timezone_get())
				); // Иначе время будет возвращено в UTC
				$val = $dateTime->format('Y-m-d H:i:s');
				break;
			case ObjectModel::TYPE_MONGO_TIMESTAMP:
				$val = (string)static::getMongoTimestamp($val);
				break;
			case ObjectModel::TYPE_TEXT:
			case ObjectModel::TYPE_HTML:
			case ObjectModel::TYPE_MARKDOWN:
			case ObjectModel::TYPE_STRING:
			case ObjectModel::TYPE_UID:
				if (is_array($val) || is_object($val)) {
					$val = json_encode($val);
					FastLog::add('ERROR', ['message' => 'Error array to string! Value: ' . $val . ' Collection:' . static::getCollectionName()]);
				}
				$val = (string)$val;
				break;
		}

		return $val;
	}

	/**
	 * @param null|int|DateTime|\\MongoDB\BSON\UTCDateTime $val
	 * @param null $msec milisecond
	 *
	 * @return \MongoDB\BSON\UTCDateTime|null
	 * @throws DBException
	 */
	public static function getMongoDate($val = null, $msec = null)
	{
		if ($val instanceof \MongoDB\BSON\UTCDateTime) {
			$date = $val;
		} elseif ($val instanceof DateTime) {
			$date = new \MongoDB\BSON\UTCDateTime($val->getTimestamp() * 1000);
		} elseif ($msec !== null) {
			$date = new \MongoDB\BSON\UTCDateTime($msec);
		} elseif (is_int($val)) {
			$date = new \MongoDB\BSON\UTCDateTime($val * 1000);
		} else {
			$date = new \MongoDB\BSON\UTCDateTime(time() * 1000);
		}

		return $date;
	}

	/**
	 * @param null $val
	 * @param null $msec
	 *
	 * @return \MongoDB\BSON\Timestamp|null
	 */
	public static function getMongoTimestamp($val = null, $msec = null)
	{
		if ($val instanceof \MongoDB\BSON\Timestamp) {
			$date = $val;
		} elseif ($val instanceof DateTime) {
			$date = new \MongoDB\BSON\Timestamp(1, $val->getTimestamp() * 1000);
		} elseif ($msec !== null) {
			$date = new \MongoDB\BSON\Timestamp(1, $msec / 1000);
		} elseif (is_int($val)) {
			$date = new \MongoDB\BSON\Timestamp(1, $val);
		} elseif (is_string($val)) {
			$date = substr($val, 1, -1);
			$tmp = explode(':', $date);
			$date = new \MongoDB\BSON\Timestamp((int)$tmp[0]??0, (int)$tmp[1]??0);
		} else {
			$date = new \MongoDB\BSON\Timestamp(1, time());
		}

		return $date;
	}

	/**
	 * @param null $val
	 *
	 * @return \MongoDB\BSON\ObjectID|null
	 * @throws DBException
	 */
	public static function getMongoId($val = null)
	{
		try {
			if (!($val instanceof \MongoDB\BSON\ObjectID)) {
				$val = new \MongoDB\BSON\ObjectID($val);
			}
		} catch (\Exception $e) {
			$val = new \MongoDB\BSON\ObjectID();
		}

		return $val;
	}

	/**
	 * @param ThreadKeepStaticArray $keep
	 */
	public static function thread(ThreadKeepStaticArray $keep)
	{
		if (!isset(static::$_fieldsDefault)) {
			static::$_fieldsDefault = $keep->get(static::class, 'getFieldsDefault');
		}
		if (!isset(static::$_fieldsValidate)) {
			static::$_fieldsValidate = $keep->get(static::class, 'getFieldsValidate');
		}
		if (!isset(static::$_fieldsPrivate)) {
			static::$_fieldsPrivate = $keep->get(static::class, 'getFieldsPrivate');
		}
		if (!isset(static::$_tagSet)) {
			static::$_tagSet = $keep->get(static::class, 'getTagSet');
		}
		if (!isset(static::$_sort)) {
			static::$_sort = $keep->get(static::class, 'getSortOrder');
		}
		if (!isset(static::$_rsort)) {
			static::$_rsort = $keep->get(static::class, 'getRevSortOrder');
		}
		if (!isset(static::$_fieldsHint)) {
			static::$_fieldsHint = $keep->get(static::class, 'getFieldsHint');
		}
	}

	public function encodeField($field)
	{
		$validate = static::getFieldsValidate();
		$rule = $validate[$field];

		return static::encode($this->{$field}, $rule);
	}

	public function decodeField($field)
	{
		$validate = static::getFieldsValidate();
		$rule = $validate[$field];

		return static::decode($this->{$field}, $rule);
	}

	public static function makeDBRef($collection, $_id, $db)
	{
		return ['$ref' => $collection, '$id' => $_id, '$db' => $db];
	}
}