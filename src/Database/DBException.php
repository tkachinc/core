<?php
namespace TkachInc\Core\Database;

/**
 * Class DBException
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class DBException extends \Exception
{

}