<?php
namespace TkachInc\Core\Database\Redis\Helper;
use TkachInc\Core\Database\Connections\RedisDatabase;

/**
 * @package TkachInc\Core\Rating
 */
class RedisRating
{
	const SORT_ASC = 1;
	const SORT_DESC = -1;

	protected $redis;

	/**
	 *
	 */
	public function __construct()
	{
		$this->redis = RedisDatabase::getInstance();
	}

	/**
	 * @param      $netId
	 * @param      $type
	 * @param      $typeProduct
	 * @param null $date
	 * @return string
	 */
	public static function getKeyName($netId, $type, $typeProduct, $date = null)
	{
		$key = $netId . '_rating_' . $type . '_' . $typeProduct;
		switch ($type) {
			case 'daily':
				if ($date === null) {
					$date = date('w');
				}
				$key .= '_' . $date;
				break;
			case 'weekly':
				if ($date === null) {
					$date = floor(date("j") / 7);
				}
				$key .= '_' . $date;
				break;
		}

		return $key;
	}

	/**
	 * @param      $ratingKey
	 * @param      $start
	 * @param      $end
	 * @param int $place
	 * @param int $sort
	 * @return array
	 */
	public function getRating($ratingKey, $start, $end, $place = 1, $sort = self::SORT_DESC)
	{
		$range = [];
		if ($sort === self::SORT_ASC) {
			$result = $this->redis->zRange($ratingKey, $start, $end, true);
		} else {
			$result = $this->redis->zRevRange($ratingKey, $start, $end, true);
		}

		if ($result) {
			foreach ($result as $userId => $score) {
				$range[] = ['place' => $place, 'userId' => $userId, 'score' => $score];
				$place++;
			}
		}

		return $range;
	}

	/**
	 * @param     $ratingKey
	 * @param     $start
	 * @param     $end
	 * @param     $place
	 * @param int $sort
	 * @return array
	 */
	public function getBottomRating($ratingKey, $start, $end, $place, $sort = self::SORT_DESC)
	{
		$range = [];
		if ($sort === self::SORT_ASC) {
			$result = $this->redis->zRevRange($ratingKey, $start, $end, true);
		} else {
			$result = $this->redis->zRange($ratingKey, $start, $end, true);
		}

		if ($result) {
			foreach ($result as $userId => $score) {
				$range[] = ['place' => $place, 'userId' => $userId, 'score' => $score];
				$place--;
			}
		}
		$range = array_reverse($range);

		return $range;
	}

	/**
	 * @param      $ratingKey
	 * @param      $userId
	 * @param      $count
	 * @param      $after
	 * @param      $before
	 * @param bool $showTopIfNotMember
	 * @return array
	 * @throws \Exception
	 */
	public function getTotalRating($ratingKey, $userId, $count, $after, $before, $showTopIfNotMember = false)
	{
		$bottom = $after + $before;
		if ($count < $bottom) {
			throw new \Exception('Error count');
		}

		// Определяем рейтинг пользователя.
		$userRank = $this->getUserRank($ratingKey, $userId);
		$userPlace = $userRank + 1;

		$rating = [];
		$countUsers = $this->getUserCount($ratingKey);
		if ($userRank !== false) {
			// Если пользователь не входит в том N выбираем две части, топ и вокруг пользователя
			if ($userRank > $count) {
				$top = $count - $bottom - 1;
				$topRating = $this->getRating($ratingKey, 0, $top);

				//var_dump($userPlace);
				//var_dump($countUsers);

				if ($userPlace + $before > $countUsers && $userPlace < $countUsers) {
					$diff = $countUsers - $userPlace;
					if ($diff > 0 && $diff <= $before) {
						$after = $after + $diff;
						$before = $before - $diff;
					} else {
						$after = $after + $before;
						$before = 0;
					}
				} elseif ($userPlace >= $countUsers) {
					$after = $after + $before;
					$before = 0;
				}
				//var_dump($after);
				//var_dump($before);

				$bottomRating = $this->getRating(
					$ratingKey,
					$userRank - $after,
					$userRank + $before,
					$userPlace - $after
				);
				$rating[] = $topRating;
				$rating[] = $bottomRating;
			} else {
				$rating[] = $this->getRating($ratingKey, 0, $count);
			}
		} else {
			if ($showTopIfNotMember) {
				$rating[] = $this->getRating($ratingKey, 0, $count);
			} else {
				$top = $count - $bottom - 1;
				$topRating = $this->getRating($ratingKey, 0, $top);
				$bottomRating = $this->getBottomRating($ratingKey, 0, $bottom - 1, $countUsers, true);
				$last = end($bottomRating);
				$bottomRating[] = ['place' => $last['place'] + 1, 'userId' => $userId, 'score' => 0];

				$rating[] = $topRating;
				$rating[] = $bottomRating;
			}
		}

		return $rating;
	}

	/**
	 * @param $ratingKey
	 * @return int
	 */
	public function getUserCount($ratingKey)
	{
		return $this->redis->zCard($ratingKey);
	}

	/**
	 * @param $ratingKey
	 * @param $userId
	 * @return mixed
	 */
	public function getScore($ratingKey, $userId)
	{
		return $this->redis->zScore($ratingKey, $userId);
	}

	/**
	 * @param     $ratingKey
	 * @param     $userId
	 * @param int $sort
	 * @return int
	 */
	public function getUserRank($ratingKey, $userId, $sort = self::SORT_DESC)
	{
		if ($sort == self::SORT_ASC) {
			return $this->redis->zRank($ratingKey, $userId);
		} else {
			return $this->redis->zRevRank($ratingKey, $userId);
		}
	}
}