<?php
namespace TkachInc\Core\Database\Redis;

use TkachInc\Core\Config\Config;
use TkachInc\Core\Database\Connections\RedisDatabase;
use TkachInc\Engine\Services\Helpers\ArrayHelper;
use TkachInc\Core\Database\ThreadKeepStaticArray;
use TkachInc\Core\Log\FastLog;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class ObjectModel
{
	const TYPE_INT = 'isInt';
	const TYPE_UNSIGNED_INT = 'isUnsignedInt';
	const TYPE_FLOAT = 'isFloat';
	const TYPE_UNSIGNED_FLOAT = 'isUnsignedFloat';
	const TYPE_BOOL = 'isBool';
	const TYPE_TIMESTAMP = 'isTimeStamp';
	const TYPE_STRING = 'isString';
	const TYPE_JSON = 'isJSON';

	protected static $_keyName = null;

	protected static $_separator = ':';

	protected static $_cache = [];

	protected $_pk;

	protected static $_useCache = false;

	protected static $_cacheSizeLimit = 16;

	protected static $_transaction = false;

	protected $_alreadyTransaction = false;

	protected static $_fieldsDefault = [];

	protected static $_fieldsPrivate = [];

	protected static $_fieldsValidate = [];

	protected static $_hasPrefix = false;

	protected static $_hasPostfix = false;

	protected static $_checkFalseNotFound = true;

	protected $_before = [];

	/**
	 * @var string
	 */
	public $_id;

	/**
	 * @param            $id
	 * @param bool|true  $useCache
	 * @param bool|false $transaction
	 */
	public function __construct($id, $useCache = null, $transaction = null)
	{
		$this->_id = $id;
		$this->_pk = static::makePk($id);
		$pk = $this->_pk;
		if (!isset($useCache)) {
			$useCache = static::$_useCache;
		} else {
			static::$_useCache = $useCache;
		}
		if (!isset($transaction)) {
			$transaction = static::$_transaction;
		} else {
			static::$_transaction = $transaction;
		}

		if ($useCache && isset(static::$_cache[$pk])) {
			foreach (static::$_cache[$pk] as $key => $value) {
				$this->{$key} = $value;
			}
		} else {
			$fields = (array)static::getRedis()->hGetAll($this->_pk);
			$fields = ArrayHelper::filterOnlyRequiredParams(
				$fields,
				static::getFieldsDefault(),
				function ($item, $requiredValue, $value) {
					$result = static::giveValidType($item, $value);
					if ($result === null) {
						return $requiredValue;
					}

					return $result;
				}
			);
			foreach ($fields as $key => $value) {
				$this->{$key} = $value;
			}

			if ($useCache) {
				if (count(static::$_cache) >= static::$_cacheSizeLimit) {
					array_shift(static::$_cache);
				}
				static::$_cache[$pk] = $this->getFieldsArray();
			}
		}

		if ($transaction) {
			$this->multi();
		}

		$this->_before = $this->getFieldsArray();
	}

	/**
	 * @param $field
	 * @param $value
	 *
	 * @return string
	 * @throws \Exception
	 */
	protected static function saveValidType($field, $value)
	{
		if (isset(static::$_fieldsValidate[$field])) {
			return static::encode($value, static::$_fieldsValidate[$field]);
		} else {
			throw new \Exception('Not found validate in save: ' . $field);
		}
	}

	/**
	 * @param $field
	 * @param $value
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	protected static function giveValidType($field, $value)
	{
		if (isset(static::$_fieldsValidate[$field])) {
			return static::decode($value, static::$_fieldsValidate[$field]);
		} else {
			throw new \Exception('Not found validate in give: ' . $field);
		}
	}

	/**
	 * @param $transaction
	 */
	public function setTransaction($transaction)
	{
		static::$_transaction = (bool)$transaction;
	}

	/**
	 * @param $useCache
	 */
	public function setUseCache($useCache)
	{
		static::$_useCache = (bool)$useCache;
	}

	/**
	 * @param $expireSeconds
	 *
	 * @return bool
	 */
	public function expire($expireSeconds)
	{
		return static::getRedis()->expire($this->_pk, $expireSeconds);
	}

	public function delete()
	{
		static::remove($this->_pk);
	}

	public function multi()
	{
		static::startTransaction();
	}

	public function exec()
	{
		static::endTransaction();
		$this->_alreadyTransaction = true;
	}

	/**
	 * @param $id
	 *
	 * @return string
	 */
	public static function makePk($id)
	{
		$name = '';
		if (!empty($id)) {
			$name .= static::$_separator . $id;
		}

		if (static::$_hasPrefix === true) {
			$prefix = Config::getInstance()->get(['prefix'], '');
			$name = $prefix . static::$_keyName . $name;

			return $name;
		}
		if (static::$_hasPostfix === true) {
			$postfix = Config::getInstance()->get(['postfix'], '');
			$name = static::$_keyName . $postfix . $name;

			return $name;
		}
		$name = static::$_keyName . $name;

		return $name;
	}

	/**
	 * @param $pk
	 *
	 * @return mixed
	 */
	public static function getIdOfThePk($pk)
	{
		$keys = explode(static::$_separator, $pk);

		return end($keys);
	}

	/**
	 * @return \Redis
	 */
	public static function getRedis()
	{
		return RedisDatabase::getInstance();
	}

	public static function unsetRedis()
	{
		RedisDatabase::unsetInstance();
	}

	public static function startTransaction()
	{
		static::getRedis()->multi();
	}

	/**
	 * @param $id
	 *
	 * @return int
	 */
	public static function remove($id)
	{
		return static::getRedis()->del(static::makePk($id));
	}

	/**
	 * @param $key
	 *
	 * @return int
	 */
	public static function del($key)
	{
		return static::getRedis()->del($key);
	}

	public static function close()
	{
		static::getRedis()->close();
	}


	/**
	 * @return bool
	 */
	public static function hasPrefix() {
		return static::$_hasPrefix;
	}

	/**
	 * @return bool
	 */
	public static function hasPostfix() {
		return static::$_hasPostfix;
	}
	
	/**
	 * @param      $method
	 * @param      $id
	 * @param null $value
	 *
	 * @return array|mixed
	 */
	public static function command($method, $id, $value = null)
	{
		$id = static::makePk($id);
		if (method_exists(static::getRedis(), $method)) {
			$arguments = [$id];
			if ($value !== null) {
				$arguments[] = $value;
			}

			return call_user_func_array([static::getRedis(), $method], $arguments);
		}

		return [];
	}

	public static function endTransaction()
	{
		static::getRedis()->exec();
	}

	public function __destruct()
	{
		if (static::$_transaction && $this->_alreadyTransaction === false) {
			$this->exec();
		}
	}

	/**
	 * @param $key
	 */
	public static function clearCache($key)
	{
		unset(static::$_cache[$key]);
	}

	/**
	 * @param null|string $pattern
	 *
	 * @return array
	 */
	public static function getKeys($pattern = '*')
	{
		return static::getRedis()->keys($pattern);
	}

	/**
	 * @return string
	 */
	public static function getKeyName()
	{
		return static::$_keyName;
	}

	/**
	 * @return string
	 */
	public static function getSeparator()
	{
		return static::$_separator;
	}

	/**
	 * @param $id
	 *
	 * @return int
	 */
	public static function type($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->type($key);
	}

	/**
	 * @param $srcKey
	 * @param $dstKey
	 *
	 * @return bool
	 */
	public static function rename($srcKey, $dstKey)
	{
		$srcKey = static::makePk($srcKey);
		$dstKey = static::makePk($dstKey);

		return static::getRedis()->rename($srcKey, $dstKey);
	}

	/**
	 * @param $id
	 *
	 * @return bool
	 */
	public static function exists($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->exists($key);
	}

	/**
	 * @return array
	 */
	public static function getFieldsDefault()
	{
		return static::$_fieldsDefault;
	}

	/**
	 * @param $id
	 *
	 * @return array
	 * @return array
	 */
	public static function getAllAdvanced($id)
	{
		return static::getRedis()->hMGet(static::makePk($id), array_keys(static::getFieldsDefault()));
	}

	/**
	 * Излекает поля объекта в массив, пригодный для сохранения
	 *
	 * @return array Массив полей объекта
	 */
	public function getFieldsArray()
	{
		$result = array_intersect_key(
			(array)$this,
			array_flip(array_keys(static::getFieldsDefault()))
		);

		return $result;
	}

	/**
	 * Излекает поля объекта в массив, пригодный для отправки клиенту
	 *
	 * @return array Массив полей объекта
	 */
	public function getFieldsSanitized()
	{
		$fields = array_diff(
			array_keys(static::getFieldsDefault()),
			array_keys(static::getFieldsPrivate())
		);
		$result = array_intersect_key((array)$this, array_flip($fields));

		return $result;
	}

	/**
	 * @return array
	 */
	public static function getFieldsPrivate()
	{
		return static::$_fieldsPrivate;
	}

	/**
	 * @return array
	 */
	public static function getFieldsValidate()
	{
		return static::$_fieldsValidate;
	}

	/**
	 * Сохранить
	 */
	public function save()
	{
		$pk = $this->_pk;
		$dbobject = ArrayHelper::changed($this->_before, $this->getFieldsArray());
		foreach ($dbobject as $field => &$item) {
			$item = static::saveValidType($field, $item);
		}

		$this->_before = $this->getFieldsArray();
		//var_dump($dbobject);ob_flush();
		if (!empty($dbobject)) {
			return static::getRedis()->hMset($pk, $dbobject);
		}

		return false;
	}

	public function clearStaticCache()
	{
		static::clearCache($this->_pk);
	}

	/**
	 * @param $id
	 * @param $hashKey
	 * @param $value
	 *
	 * @return int
	 */
	public static function hSet($id, $hashKey, $value)
	{
		$key = static::makePk($id);
		try {
			$value = static::saveValidType($hashKey, $value);
		} catch (\Exception $e) {

		}

		return static::getRedis()->hSet($key, $hashKey, $value);
	}

	/**
	 * @param $id
	 * @param $hashKey
	 *
	 * @return string
	 */
	public static function hGet($id, $hashKey)
	{
		$key = static::makePk($id);
		$value = static::getRedis()->hGet($key, $hashKey);
		try {
			$value = static::giveValidType($hashKey, $value);
			if ($value === null) {
				$value = static::$_fieldsDefault[$hashKey]??null;
			}
		} catch (\Exception $e) {

		}

		return $value;
	}

	/**
	 * @param $id
	 *
	 * @return int
	 */
	public static function hLen($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->hLen($key);
	}

	/**
	 * @param $id
	 * @param $hashKey
	 *
	 * @return int
	 */
	public static function hDel($id, $hashKey)
	{
		$key = static::makePk($id);

		return static::getRedis()->hDel($key, $hashKey);
	}

	/**
	 * @param $id
	 * @param $hashKey
	 *
	 * @return bool
	 */
	public static function hExists($id, $hashKey)
	{
		$key = static::makePk($id);

		return static::getRedis()->hExists($key, $hashKey);
	}

	/**
	 * @param $id
	 *
	 * @return array
	 */
	public static function hKeys($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->hKeys($key);
	}

	/**
	 * @param $id
	 *
	 * @return array
	 */
	public static function hVals($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->hVals($key);
	}

	/**
	 * @param $id
	 *
	 * @return array
	 */
	public static function hGetAll($id)
	{
		$key = static::makePk($id);
		$fields = (array)static::getRedis()->hGetAll($key);
		$fields = ArrayHelper::filterOnlyRequiredParams(
			$fields,
			static::getFieldsDefault(),
			function ($item, $requiredValue, $value) {
				$result = static::giveValidType($item, $value);
				if ($result === null) {
					return $requiredValue;
				}

				return $result;
			}
		);

		return $fields;
	}

	/**
	 * @param $id
	 * @param $hashKeys
	 *
	 * @return bool
	 */
	public static function hMset($id, Array $hashKeys)
	{
		$key = static::makePk($id);
		foreach ($hashKeys as $field => &$item) {
			$item = static::saveValidType($field, $item);
		}

		return static::getRedis()->hMset($key, $hashKeys);
	}

	/**
	 * @param $id
	 * @param $hashKeys
	 *
	 * @return array
	 */
	public static function hMGet($id, Array $hashKeys)
	{
		$key = static::makePk($id);
		$fields = (array)static::getRedis()->hMGet($key, $hashKeys);
		$fields = ArrayHelper::filterOnlyRequiredParams(
			$fields,
			static::getFieldsDefault(),
			function ($item, $requiredValue, $value) {
				$result = static::giveValidType($item, $value);
				if ($result === null) {
					return $requiredValue;
				}

				return $result;
			}
		);

		return $fields;
	}

	/**
	 * @param $field
	 * @param $increment
	 */
	public function incBy($field, $increment)
	{
		static::getRedis()->hIncrBy($this->_pk, $field, $increment);
	}

	/**
	 * @param $id
	 * @param $hashKey
	 * @param $increment
	 *
	 * @return mixed
	 */
	public static function hIncrByType($id, $hashKey, $increment)
	{
		$key = static::makePk($id);
		if (is_float($increment)) {
			return (float)static::hIncrByFloat($key, $hashKey, $increment);
		} else {
			return (int)static::hIncrBy($key, $hashKey, $increment);
		}
	}

	/**
	 * @param $id
	 * @param $hashKey
	 * @param $increment
	 *
	 * @return mixed
	 */
	public static function hIncrBy($id, $hashKey, $increment)
	{
		$key = static::makePk($id);

		return static::getRedis()->hIncrBy($key, $hashKey, $increment);
	}

	/**
	 * @param $id
	 * @param $hashKey
	 * @param $increment
	 *
	 * @return mixed
	 */
	public static function hIncrByFloat($id, $hashKey, $increment)
	{
		$key = static::makePk($id);

		return static::getRedis()->hIncrByFloat($key, $hashKey, $increment);
	}

	/**
	 * Декодировать данные в нужный тип
	 *
	 * @param string $val - данные для кодирования
	 * @param        $rule
	 *
	 * @return mixed $rule тип данных
	 */
	public static function decode($val, $rule)
	{
		if (static::$_checkFalseNotFound && $val === false && $rule !== ObjectModel::TYPE_BOOL) {
			return null;
		}

		switch ($rule) {
			case ObjectModel::TYPE_UNSIGNED_INT:
			case ObjectModel::TYPE_INT:
				$val = (int)$val;
				break;
			case ObjectModel::TYPE_UNSIGNED_FLOAT:
			case ObjectModel::TYPE_FLOAT:
				$val = (float)$val;
				break;
			case ObjectModel::TYPE_BOOL:
				if ($val === 'true') {
					$val = true;
				}
				if ($val === 'false') {
					$val = false;
				}
				$val = (bool)$val;
				break;
			case ObjectModel::TYPE_TIMESTAMP:
				if ($val == '0') {
					$val = 0;
				} else {
					$date = new \DateTime($val);
					$val = $date->getTimestamp();
				}
				break;
			case ObjectModel::TYPE_JSON:
				if (is_string($val)) {
					$val = json_decode($val, true);
				} else {
					$val = (array)$val;
				}
				break;
			case ObjectModel::TYPE_STRING:
				$val = (string)$val;
				break;
		}

		return $val;
	}

	/**
	 * @param $val
	 * @param $rule
	 *
	 * @return float|array|string
	 */
	public static function encode($val, $rule)
	{
		switch ($rule) {
			case ObjectModel::TYPE_UNSIGNED_INT:
			case ObjectModel::TYPE_INT:
				$val = (int)$val;
				break;
			case ObjectModel::TYPE_UNSIGNED_FLOAT:
			case ObjectModel::TYPE_FLOAT:
				$val = (float)$val;
				break;
			case ObjectModel::TYPE_BOOL:
				if ($val) {
					$val = 'true';
				} else {
					$val = 'false';
				}
				break;
			case ObjectModel::TYPE_TIMESTAMP:
				$date = new \DateTime();
				$date->setTimestamp($val);
				$val = $date->format('Y-m-d H:i:s');
				break;
			case ObjectModel::TYPE_JSON:
				if (is_object($val)) {
					$val = (array)$val;
				}
				$val = json_encode($val, JSON_UNESCAPED_UNICODE);
				break;
			case ObjectModel::TYPE_STRING:
				if (is_array($val) || is_object($val)) {
					$val = json_encode($val);
					FastLog::add('ERROR', ['message' => 'Error array to string! Value: ' . $val . ' Collection:' . static::getKeyName()]);
				}
				$val = (string)$val;
				break;
		}

		return $val;
	}

	/**
	 * @param string $pattern
	 * @param int    $count
	 *
	 * @return \Generator
	 */
	public function scan($pattern = '*', $count = 200)
	{
		$itr = null;
		static::getRedis()->setOption(\Redis::OPT_SCAN, \Redis::SCAN_RETRY);
		while ($arrKeys = static::getRedis()->scan($itr, static::makePk($pattern), $count)) {
			foreach ($arrKeys as $strKey) {
				yield $strKey;
			}
		}
	}

	/**
	 * @param ThreadKeepStaticArray $keep
	 */
	public static function thread(ThreadKeepStaticArray $keep)
	{
		if (!isset(static::$_fieldsDefault)) {
			static::$_fieldsDefault = $keep->get(static::class, 'getFieldsDefault');
		}
		if (!isset(static::$_fieldsValidate)) {
			static::$_fieldsValidate = $keep->get(static::class, 'getFieldsValidate');
		}
		if (!isset(static::$_fieldsPrivate)) {
			static::$_fieldsPrivate = $keep->get(static::class, 'getFieldsPrivate');
		}
	}
} 