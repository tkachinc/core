<?php
namespace TkachInc\Core\Database\Connections;

use MongoDB\Client;
use MongoDB\Database;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class MongoDBDatabase extends BaseDriver
{
	/**
	 * @var \MongoDB\Driver\Manager
	 */
	protected $connect;
	/**
	 * @var Database
	 */
	protected $db;

	/**
	 * @return bool
	 */
	public function ping()
	{
		//echo PHP_EOL.'-ping-'.PHP_EOL;
		if (isset($this->db)) {
			$cursor = $this->db->command(['ping' => 1]);
			$result = current($cursor->toArray());
			if (isset($result->ok) && $result->ok == 1) {
				//echo PHP_EOL.'-ok-'.PHP_EOL;
				return true;
			}
		}

		//echo PHP_EOL.'-non-'.PHP_EOL;
		return false;
	}

	public function connect()
	{
		//echo PHP_EOL.'-connect-'.PHP_EOL;
		$config = $this->getConfig();
		if (!isset($config['host']) || !isset($config['db'])) {
			throw new ConnectionException('Not found host or db');
		}

		$host = $config['host'];
		$uriOptions = isset($config['uriOptions']) ? $config['uriOptions'] : ['connectTimeoutMS' => 500];
		$driverOptions = isset($config['driverOptions']) ? $config['driverOptions'] : [];
		$db = $config['db'];

		if (!isset($driverOptions['typeMap'])) {
			$driverOptions['typeMap'] = [
				'array'    => 'Array',
				'document' => 'Array',
				'root'     => 'Array',
			];
		}
		$this->connect = new Client($host, $uriOptions, $driverOptions);

		$options = isset($config['dbOptions'][$db]) ? $config['dbOptions'][$db] : [];
		$this->db = $this->connect->selectDatabase($db, $options);
	}

	/**
	 * @return null
	 */
	public function getConnect()
	{
		return isset($this->connect) ? $this->connect : null;
	}

	/**
	 * @return null
	 */
	public function getDB()
	{
		return isset($this->db) ? $this->db : null;
	}

	/**
	 * @param string $name
	 * @return string
	 */
	public static function getName($name = '')
	{
		return 'mongodb' . $name;
	}
}