<?php
namespace TkachInc\Core\Database\Connections;

use TkachInc\Core\Config\Config;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class BaseDriver
{
	/**
	 * @var BaseDriver
	 */
	protected static $instance = [];
	protected $name;

	protected $nextPing = 0;

	/**
	 * @return int
	 */
	protected function getTimeoutPing()
	{
		return 5;
	}

	/**
	 * @return int
	 */
	protected function getNextPing()
	{
		return $this->nextPing;
	}

	/**
	 * @param int $val
	 */
	protected function setNextPing($val)
	{
		$this->nextPing = $val;
	}

	/**
	 *
	 */
	protected function resetNextPing()
	{
		$this->setNextPing(time() + $this->getTimeoutPing());
	}

	/**
	 * @return bool
	 */
	protected function isNeedPing()
	{
		return ($this->getNextPing() < time());
	}

	/**
	 * @return bool
	 */
	public function pingCheck()
	{
		if ($this->isNeedPing()) {
			$this->resetNextPing();

			return $this->ping();
		}

		return true;
	}

	/**
	 * @param string $name
	 */
	public function __construct($name = '')
	{
		$this->name = $name;
		$this->resetNextPing();
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public static function getInstance($name = '')
	{
		$name = static::getName($name);
		if (!isset(static::$instance[$name]) || !static::$instance[$name]->pingCheck()) {
			static::$instance[$name] = new static($name);
			static::$instance[$name]->connect();
		}

		return static::$instance[$name]->getDB();
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public static function getInstanceDriver($name = '')
	{
		$name = static::getName($name);
		if (!isset(static::$instance[$name]) || !static::$instance[$name]->pingCheck()) {
			static::$instance[$name] = new static($name);
			static::$instance[$name]->connect();
		}

		return static::$instance[$name]->getConnect();
	}

	/**
	 * @param string $name
	 */
	public static function unsetInstance($name = '')
	{
		$name = static::getName($name);
		if (isset(static::$instance[$name])) {
			unset(static::$instance[$name]);
		}
	}

	/**
	 * @param string $name
	 * @return BaseDriver
	 */
	public static function get($name = '')
	{
		$name = static::getName($name);
		if (!isset(static::$instance[$name])) {
			static::getInstance($name);
		}

		return static::$instance[$name];
	}

	/**
	 * @return mixed
	 */
	public function getConfig()
	{
		$projectId = Config::getInstance()->get(['projectId'], 'main');

		return (array)Config::getInstance($projectId)->get([$this->name], []);
	}

	abstract public function ping();

	abstract public function connect();

	abstract public function getConnect();

	abstract public function getDB();

	/**
	 * @param string $name
	 * @return string
	 */
	public static function getName($name = '')
	{
		return 'base' . $name;
	}
}