<?php
namespace TkachInc\Core\Database\Connections;

use Redis;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class RedisDatabase extends BaseDriver
{
	/**
	 * @var Redis
	 */
	protected $connect;

	/**
	 * @return bool
	 */
	public function ping()
	{
		if (isset($this->connect)) {
			return ($this->connect->ping() === '+PONG');
		}

		return false;
	}

	public function connect()
	{
		$config = static::getConfig();
		if (!isset($config['host']) || !isset($config['port'])) {
			throw new ConnectionException('Not found host or port');
		}

		$this->connect = new Redis();
		$port = $config['port'];
		if (isset($port)) {
			$this->connect->connect($config['host'], $port);
		} else {
			$this->connect->connect($config['host']);
		}
		if (isset($config['pass']) && !empty($config['pass'])) {
			$this->connect->auth($config['pass']);
		}
		$this->connect->select($config['db']);
	}

	/**
	 * @return null
	 */
	public function getConnect()
	{
		return isset($this->connect) ? $this->connect : null;
	}

	/**
	 * @return null
	 */
	public function getDB()
	{
		return isset($this->connect) ? $this->connect : null;
	}

	/**
	 * @param string $name
	 * @return string
	 */
	public static function getName($name = '')
	{
		return 'redis' . $name;
	}
}