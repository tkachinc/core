<?php
namespace TkachInc\Core\Database\Connections;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class MysqliDatabase extends BaseDriver
{
	protected $connect;

	/**
	 * @return bool
	 */
	public function ping()
	{
		if (isset($this->connect)) {
			return mysqli_ping($this->connect);
		}

		return false;
	}

	public function connect()
	{
		$config = $this->getConfig();

		if (!isset($config['host']) || !isset($config['user']) || !isset($config['password']) || !isset($config['db'])) {
			throw new ConnectionException('Not found host or user or password or db');
		}

		$this->connect = mysqli_connect(
			$config['host'],
			$config['user'],
			$config['password'],
			$config['db']
		);

		if (mysqli_connect_errno()) {
			throw new \Exception(
				"MySQLi connect error: (" . mysqli_connect_errno() . ") " . mysqli_connect_error()
			);
		}
	}

	/**
	 * @return null
	 */
	public function getConnect()
	{
		return isset($this->connect) ? $this->connect : null;
	}

	/**
	 * @return null
	 */
	public function getDB()
	{
		return isset($this->connect) ? $this->connect : null;
	}

	/**
	 * @param string $name
	 * @return string
	 */
	public static function getName($name = '')
	{
		return 'mysql' . $name;
	}
}