<?php
namespace TkachInc\Core\Database\Connections;

use Elasticsearch\Client;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ElasticsearchDatabase extends BaseDriver
{
	protected $connect;

	/**
	 * @return bool
	 */
	public function ping()
	{
		return true;
	}

	public function connect()
	{
		$config = $this->getConfig();
		$this->connect = \Elasticsearch\ClientBuilder::create()->fromConfig($config);
	}

	/**
	 * @return null
	 */
	public function getConnect()
	{
		return isset($this->connect) ? $this->connect : null;
	}

	/**
	 * @return null
	 */
	public function getDB()
	{
		return isset($this->connect) ? $this->connect : null;
	}

	/**
	 * @param string $name
	 * @return string
	 */
	public static function getName($name = '')
	{
		return 'elasticsearch' . $name;
	}
}