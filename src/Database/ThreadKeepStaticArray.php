<?php
namespace TkachInc\Core\Database;

/**
 * @author maxim
 */
class ThreadKeepStaticArray
{
	/**
	 * @var array
	 */
	protected $storage = [

	];

	/**
	 * @param $class
	 * @param $key
	 * @param $value
	 */
	public function set(string $class, string $key, Array $value)
	{
		$this->storage[$class][$key] = (array)$value;
	}

	/**
	 * @param string $class
	 * @param string $key
	 * @return array
	 */
	public function get(string $class, string $key): array
	{
		if (isset($this->storage[$class]) && isset($this->storage[$class][$key])) {
			return (array)$this->storage[$class][$key];
		}

		return [];
	}
}