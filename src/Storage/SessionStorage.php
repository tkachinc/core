<?php
namespace TkachInc\Core\Storage;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SessionStorage
{
	const TYPE_BASE = 'base';

	/**
	 *
	 */
	public function __construct()
	{
		static::init();
	}

	public static function init()
	{
		if (!isset($_SESSION)) {
			session_start();
		}
	}

	/**
	 * @param        $name
	 * @param        $value
	 * @param string $type
	 */
	public static function set($name, $value, $type = self::TYPE_BASE)
	{
		$_SESSION[$type][$name] = $value;
	}

	/**
	 * @param        $name
	 * @param string $type
	 * @param null $default
	 * @return null
	 */
	public static function get($name, $type = self::TYPE_BASE, $default = null)
	{
		return isset($_SESSION[$type][$name]) ? $_SESSION[$type][$name] : $default;
	}

	/**
	 * @param        $name
	 * @param string $type
	 * @return bool
	 */
	public static function issetSession($name, $type = self::TYPE_BASE)
	{
		return isset($_SESSION[$type][$name]);
	}

	/**
	 * @param        $name
	 * @param string $type
	 */
	public static function unsetSession($name, $type = self::TYPE_BASE)
	{
		if (isset($_SESSION[$type][$name])) {
			unset($_SESSION[$type][$name]);
		}
	}

	/**
	 * @param string $type
	 */
	public static function destroySession($type = self::TYPE_BASE)
	{
		if (isset($_SESSION[$type])) {
			unset($_SESSION[$type]);
		}
	}
} 