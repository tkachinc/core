<?php
namespace TkachInc\Core\Storage;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class TempFileCache
{
	protected static $instance;

	protected $temp;

	protected $sendFunction;

	protected $getFunction;

	/**
	 * TempFileCache constructor.
	 *
	 * @param bool $useJson
	 */
	protected function __construct($useJson)
	{
		$this->temp = tmpfile();

		if ($useJson) {
			$this->sendFunction = function (Array $data) {
				fputs($this->temp, json_encode($data, JSON_UNESCAPED_UNICODE) . PHP_EOL);
			};
			$this->getFunction = function () {
				while (($data = fgets($this->temp)) !== false) {
					yield $data;
				}
			};
		} else {
			$this->sendFunction = function (Array $data) {
				fputcsv($this->temp, $data);
			};
			$this->getFunction = function () {
				while (($data = fgetcsv($this->temp, 0)) !== false) {
					yield $data;
				}
			};
		}
	}

	/**
	 * @param bool $useJson
	 *
	 * @return TempFileCache
	 */
	public static function getInstance($useJson = true)
	{
		if (!isset(static::$instance)) {
			static::$instance = new static($useJson);
		}

		return static::$instance;
	}

	/**
	 * @param array $data
	 *
	 * @return $this
	 */
	public function send(Array $data)
	{
		fseek($this->temp, 0, SEEK_END);
		$sendFunction = $this->sendFunction;
		$sendFunction($data);

		return $this;
	}

	/**
	 * @return \Generator
	 */
	public function get()
	{
		fseek($this->temp, 0);
		$getFunction = $this->getFunction;

		return $getFunction();
	}

	public function __destruct()
	{
		fclose($this->temp);
	}

	/**
	 * @param $data
	 *
	 * @return $this
	 */
	public function write($data)
	{
		fseek($this->temp, 0, SEEK_END);
		fwrite($this->temp, $data);

		return $this;
	}

	/**
	 * @return int
	 */
	public function read()
	{
		fseek($this->temp, 0);

		return stream_get_contents($this->temp);
	}
}