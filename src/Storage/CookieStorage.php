<?php
namespace TkachInc\Core\Storage;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class CookieStorage
{
	/**
	 * @param        $name
	 * @param        $value
	 * @param int $expire
	 * @param string $patch
	 */
	public static function set($name, $value, $expire = 0, $patch = '/', $domain = null, $secure = null, $httponly = null)
	{
		setcookie($name, $value, $expire, $patch, $domain, $secure, $httponly);
		$_COOKIE[$name] = $value;
	}

	/**
	 * @param        $name
	 * @param null $default
	 * @return null
	 */
	public static function get($name, $default = null)
	{
		return isset($_COOKIE[$name]) ? $_COOKIE[$name] : $default;
	}

	/**
	 * @param        $name
	 * @return bool
	 */
	public static function issetCookie($name)
	{
		return isset($_COOKIE[$name]);
	}

	/**
	 * @param        $name
	 */
	public static function unsetCookie($name)
	{
		if (isset($_COOKIE[$name])) {
			setcookie($name, null, -1, '/');
			unset($_COOKIE[$name]);
		}
	}
} 