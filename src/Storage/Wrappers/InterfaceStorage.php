<?php

namespace TkachInc\Core\Storage\Wrappers;

/**
 * @author maxim
 */
interface InterfaceStorage
{
	public function __construct(Array $config = []);

	public function set($key, $value);

	public function get($key, $default);

	public function isset($key);

	public function remove($key);
}