<?php
namespace TkachInc\Core\Storage\Wrappers;

use TkachInc\Core\Storage\CookieStorage;

/**
 * @author maxim
 */
class Cookie implements InterfaceStorage
{
	protected $expire, $patch, $domain, $secure, $httponly;

	public function __construct(Array $config = [])
	{
		$this->expire = $config['expire']??0;
		$this->patch = $config['patch']??'/';
		$this->domain = $config['domain']??null;
		$this->secure = $config['secure']??null;
		$this->httponly = $config['httponly']??null;
	}

	public function set($key, $value)
	{
		CookieStorage::set($key, $value, $this->expire, $this->patch, $this->domain, $this->secure, $this->httponly);
	}

	public function get($key, $default)
	{
		return CookieStorage::get($key, $default);
	}

	public function isset($key)
	{
		return CookieStorage::issetCookie($key);
	}

	public function remove($key)
	{
		CookieStorage::unsetCookie($key);
	}
}