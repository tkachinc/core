<?php
namespace TkachInc\Core\Storage\Wrappers;

use TkachInc\Core\Storage\SessionStorage;

/**
 * @author maxim
 */
class Session implements InterfaceStorage
{
	protected $type;

	public function __construct(Array $config = [])
	{
		$this->type = $config['type']??SessionStorage::TYPE_BASE;
	}

	public function set($key, $value)
	{
		SessionStorage::set($key, $value, $this->type);
	}

	public function get($key, $default)
	{
		return SessionStorage::get($key, $default, $this->type);
	}

	public function isset($key)
	{
		return SessionStorage::issetSession($key, $this->type);
	}

	public function remove($key)
	{
		SessionStorage::unsetSession($key, $this->type);
	}
}