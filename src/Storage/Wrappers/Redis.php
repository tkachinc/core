<?php
namespace TkachInc\Core\Storage\Wrappers;

use TkachInc\Core\Database\Connection\RedisDatabase;
use TkachInc\Core\Storage\SessionStorage;
use TkachInc\Engine\Services\Helpers\Generator\HashGenerator;

/**
 * @author maxim
 */
class Redis
{
	protected $id;

	public function __construct(Array $config = [])
	{
		$id = SessionStorage::get('id', SessionStorage::TYPE_BASE, HashGenerator::getStringUniqueId());
		$this->id = $id;
		SessionStorage::set('id', $this->id);
	}

	public function set($key, $value)
	{
		/** @var \Redis $redis */
		$redis = RedisDatabase::getInstance();
		$redis->hSet('session:' . $this->id, $key, $value);
	}

	public function get($key, $default)
	{
		/** @var \Redis $redis */
		$redis = RedisDatabase::getInstance();
		$result = $redis->hGet('session:' . $this->id, $key);
		if (!$result) {
			$result = $default;
		}

		return $result;
	}

	public function isset($key)
	{
		/** @var \Redis $redis */
		$redis = RedisDatabase::getInstance();
		$result = $redis->hExists('session:' . $this->id, $key);

		return $result;
	}

	public function remove($key)
	{
		/** @var \Redis $redis */
		$redis = RedisDatabase::getInstance();
		$redis->hDel('session:' . $this->id, $key);
	}
}