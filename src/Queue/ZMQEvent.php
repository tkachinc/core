<?php
namespace TkachInc\Core\Queue;

use TkachInc\Core\Log\FastLog;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ZMQEvent
{
	/**
	 * @param        $name
	 * @param        $payload
	 * @param string $host
	 */
	public static function run($name, $payload, $host = 'tcp://127.0.0.1:5555')
	{
		new static($name, $payload, $host);
	}

	protected $name;
	protected $payload;
	protected $host;

	/**
	 * @param        $name
	 * @param        $payload
	 * @param string $host
	 */
	public function __construct($name, $payload, $host = 'tcp://127.0.0.1:5555')
	{
		$this->name = $name;
		$this->payload = $payload;
		$this->host = $host;

		$this->send();
	}

	public function send()
	{
		if (class_exists('ZMQContext')) {
			$entryData = ['name' => $this->name, 'payload' => $this->payload];
			$context = new \ZMQContext();
			$socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my pusher');
			$socket->connect($this->host);
			$socket->send(json_encode($entryData));
		} else {
			FastLog::add('error', ['message' => 'Please install ZMQ and reload PHP'], false, true);
		}
	}
}