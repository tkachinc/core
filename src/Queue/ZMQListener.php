<?php
namespace TkachInc\Core\Queue;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ZMQListener
{
	public function get($key, Callable $function, $delay = 1, $daemon = false, $host = 'tcp://127.0.0.1:5555')
	{
		$context = new \ZMQContext();
		$subscriber = new \ZMQSocket($context, \ZMQ::SOCKET_SUB);
		$subscriber->connect($host);
		$subscriber->setSockOpt(\ZMQ::SOCKOPT_SUBSCRIBE, $key);

		while (true) {
			//  Read envelope with address
			$data = $subscriber->recv();
			$more = $subscriber->getSockOpt(\ZMQ::SOCKOPT_RCVMORE);
			if ($more) {
				//  Read message contents
				$data = $subscriber->recv();
				call_user_func_array($function, [$data]);
			} elseif (!$daemon) {
				break;
			}
			sleep($delay);
		}
	}
}