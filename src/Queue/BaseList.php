<?php
namespace TkachInc\Core\Queue;
use TkachInc\Core\Database\Connections\RedisDatabase;

/**
 * @author maxim
 */
abstract class BaseList
{
	protected $methodPush, $methodPop;

	public function __construct($leftPush = true, $leftPop = false)
	{
		if ($leftPush) {
			$this->methodPush = 'lPush';
		} else {
			$this->methodPush = 'rPush';
		}

		if ($leftPop) {
			$this->methodPop = 'lPop';
		} else {
			$this->methodPop = 'rPop';
		}
	}

	/**
	 * @return mixed Return redis key name
	 */
	abstract public function getTableName();

	/**
	 * @return \Generator
	 */
	public function get()
	{
		$table = $this->getTableName();
		$count = $this->getLen();
		/** @var \Redis $redis */
		$redis = RedisDatabase::getInstance();
		while ($count > 0) {
			if (!$redis->ping()) {
				$redis = RedisDatabase::getInstance();
			}
			$method = $this->methodPop;
			yield $redis->{$method}($table);
			$count = $this->getLen();
		}
	}

	/**
	 * @return int return elements in list
	 */
	public function getLen()
	{
		$table = $this->getTableName();
		/** @var \Redis $redis */
		$redis = RedisDatabase::getInstance();

		return (int)$redis->lLen($table);
	}

	/**
	 * @param $value
	 * @return bool
	 */
	public function add($value)
	{
		$table = $this->getTableName();
		/** @var \Redis $redis */
		$redis = RedisDatabase::getInstance();
		$method = $this->methodPush;

		return $redis->{$method}($table, $value);
	}

	public function clear()
	{
		$table = $this->getTableName();
		/** @var \Redis $redis */
		$redis = RedisDatabase::getInstance();

		return $redis->del($table);
	}
}