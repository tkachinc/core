<?php
namespace TkachInc\Core\Queue;
use TkachInc\Core\Database\Connections\RedisDatabase;

/**
 * @author maxim
 */
class RedisEvent
{
	/**
	 * @param        $name
	 * @param        $payload
	 */
	public static function run($name, $payload)
	{
		new static($name, $payload);
	}

	protected $name;
	protected $payload;

	/**
	 * @param        $name
	 * @param        $payload
	 */
	public function __construct($name, $payload)
	{
		$this->name = $name;
		$this->payload = $payload;
		$this->send();
	}

	public function send()
	{
		/** @var \Redis $redis */
		$redis = RedisDatabase::getInstance();
		$redis->rPush($this->name, json_encode($this->payload));
	}
}