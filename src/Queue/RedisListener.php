<?php
namespace TkachInc\Core\Queue;
use TkachInc\Core\Database\Connections\RedisDatabase;

/**
 * @author maxim
 */
class RedisListener
{
	public function get($key, Callable $function, $delay = 1, $daemon = false)
	{
		/** @var \Redis $redis */
		$redis = RedisDatabase::getInstance();
		$count = $redis->lLen($key);
		while (true) {
			if (!$redis->ping()) {
				$redis = RedisDatabase::getInstance();
			}

			if ($count > 0) {
				$data = $redis->lPop($key);
				call_user_func_array($function, [$data]);
			} elseif (!$daemon) {
				break;
			}
			sleep($delay);
		}
	}
}