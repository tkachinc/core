<?php
namespace TkachInc\Core\Auth\Core;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
trait CheckAuthTrait
{
	protected $role = [];
	protected $groups = [];
	protected $reads = [];
	protected $writes = [];

	/**
	 * @param $role
	 */
	protected function setRole($role)
	{
		$this->role = $role;
	}

	/**
	 * @param $groups
	 */
	protected function setGroups($groups)
	{
		$this->groups = $groups;
	}

	/**
	 * @param $reads
	 */
	protected function setReads($reads)
	{
		$this->reads = (array)$reads;
	}

	/**
	 * @param $writes
	 */
	protected function setWrites($writes)
	{
		$this->writes = (array)$writes;
	}

	/**
	 * @return array
	 */
	public function getRole()
	{
		return $this->role;
	}

	/**
	 * @return array
	 */
	public function getGroups()
	{
		return $this->groups;
	}

	/**
	 * @param $group
	 * @return bool
	 */
	public function hasGroup($group)
	{
		return (array_search($group, $this->groups) !== false);
	}

	/**
	 * @param $model
	 * @return bool
	 */
	public function canRead($model)
	{
		return (isset($this->reads[$model]));
	}

	/**
	 * @param $model
	 * @return bool
	 */
	public function canWrite($model)
	{
		return (isset($this->writes[$model]));
	}
}