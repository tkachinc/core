<?php
namespace TkachInc\Core\Auth\Core;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
interface IAuth
{
	/**
	 * @param $login
	 * @param $password
	 * @return mixed
	 */
	public function login($login, $password);

	public function logout();

	/**
	 * @param $password
	 * @return mixed
	 */
	public function getHash($password);

	/**
	 * @param $password
	 * @param $hash
	 * @return mixed
	 */
	public function checkPass($password, $hash);
}
