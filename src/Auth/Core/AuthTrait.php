<?php
namespace TkachInc\Core\Auth\Core;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
trait AuthTrait
{
	/**
	 * @param       $password
	 * @param int $algo
	 * @param array $options
	 * @return bool|string
	 */
	public function passwordGenerateHash($password, $algo = PASSWORD_DEFAULT, Array $options = [])
	{
		return password_hash(
			$password,
			$algo,
			$options
		);
	}

	/**
	 * @param      $password
	 * @param      $hash
	 * @return bool|string
	 */
	public function passwordVerify($password, $hash)
	{
		return password_verify(
			$password,
			$hash
		);
	}

	/**
	 * @param       $hash
	 * @param int $algo
	 * @param array $options
	 * @return string
	 */
	public function passwordNeedsReHash($hash, $algo = PASSWORD_DEFAULT, Array $options = [])
	{
		return password_needs_rehash($hash, $algo, $options);
	}

	/**
	 * @param $hash
	 * @return array
	 */
	public function passwordGetInfo($hash)
	{
		return password_get_info($hash);
	}

	/**
	 * @param      $password
	 * @return bool|string
	 */
	public function getHash($password)
	{
		return $this->passwordGenerateHash($password);
	}

	/**
	 * @param $password
	 * @param $hash
	 * @return bool|string Hash or false
	 */
	public function checkPass($password, $hash)
	{
		if ($this->passwordVerify($password, $hash)) {
			if ($this->passwordNeedsReHash($hash)) {
				return $this->passwordGenerateHash($password);
			}

			return $hash;
		}

		return false;
	}
}