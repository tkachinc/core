<?php
namespace TkachInc\Core\Auth\Core;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
interface ICheckAuth
{
	/**
	 * @return array
	 */
	public function getRole();

	/**
	 * @param $group
	 * @return mixed
	 */
	public function hasGroup($group);

	/**
	 * @param $page
	 * @return bool
	 */
	public function canRead($page);

	/**
	 * @param $page
	 * @return bool
	 */
	public function canWrite($page);
}
