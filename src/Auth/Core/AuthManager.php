<?php
namespace TkachInc\Core\Auth\Core;

use TkachInc\Core\Storage\SessionStorage;

/**
 * Class AuthManager
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AuthManager
{
	const SESSION_USER = 'user';

	/**
	 * Виставлення сессії
	 *
	 * @param int $sessionTimeout
	 */
	public static function setSessionMaxLifetime($sessionTimeout = 86400)
	{
		ini_set('session.gc_maxlifetime', $sessionTimeout);
	}

	/**
	 * @param $login
	 */
	public static function login($login)
	{
		SessionStorage::set(self::SESSION_USER, $login, SessionStorage::TYPE_BASE);
	}

	/**
	 * Вийти з системи
	 */
	public static function logout()
	{
		SessionStorage::destroySession(SessionStorage::TYPE_BASE);
	}

	/**
	 * Чи увійшов користувач в систему
	 *
	 * @return bool
	 */
	public static function loggedIn()
	{
		return SessionStorage::issetSession(self::SESSION_USER, SessionStorage::TYPE_BASE);
	}

	/**
	 * Отримати АйДі користувача
	 *
	 * @return $login
	 */
	public static function getUser()
	{
		return SessionStorage::get(self::SESSION_USER, SessionStorage::TYPE_BASE);
	}
}