<?php
namespace TkachInc\Core\Auth\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @property array params
 * @property int time
 * @property bool|string hash
 * @property string _id
 * @property string role
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AdminUser extends ObjectModel
{
	protected static $_collection = 'admin_users';
	protected static $_pk = '_id';
	protected static $_sort = ['_id' => 1];
	protected static $_indexes = [
		[
			'name'   => '_id',
			'keys'   => ['_id' => 1],
			'unique' => true,
		],
	];
	protected static $_fieldsDefault = [
		'_id'    => '',
		'hash'   => '',
		'role'   => '',
		'groups' => [],
		'reads'  => [],
		'writes' => [],
		'params' => [],
		'time'   => 0,
	];
	protected static $_fieldsValidate = [
		'_id'    => self::TYPE_STRING,
		'hash'   => self::TYPE_STRING,
		'role'   => self::TYPE_STRING,
		'groups' => self::TYPE_JSON,
		'reads'  => self::TYPE_JSON,
		'writes' => self::TYPE_JSON,
		'params' => self::TYPE_JSON,
		'time'   => self::TYPE_TIMESTAMP,
	];
	protected static $_fieldsPrivate = ['hash' => 1, 'reads' => 1, 'writes' => 1];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;
	protected static $_updateMethod = self::UPDATE_METHOD_SET;
}