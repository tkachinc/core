<?php
namespace TkachInc\Core\Auth;

/**
 * Class AuthException
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AuthException extends \Exception
{

}