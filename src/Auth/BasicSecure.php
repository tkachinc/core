<?php
namespace TkachInc\Core\Auth;

use TkachInc\Core\Auth\Core\AuthManager;
use TkachInc\Core\Auth\Model\AdminUser;
use TkachInc\Core\Auth\Types\AbstractAuthType;

/**
 * Class BasicSecure
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class BasicSecure extends AbstractAuthType
{
	public function login($login, $password)
	{
		if ($this->isLogged()) {
			throw new AuthException('You are logged');
		}

		$user = new AdminUser($login);
		if (!$user->isLoadedObject()) {
			$user->_id = 'admin';
			$user->hash = $this->getHash('fhak321');
			$user->role = 'root';
			$user->time = time();
			$user->save();
		}

		if ($hash = $this->checkPass($password, $user->hash)) {
			if ($hash !== $user->hash) {
				$user->hash = $hash;
				$user->save();
			}
			AuthManager::login($login);
		} else {
			$_SESSION['logout'] = true;
			throw new AuthException('Error password');
		}
	}

	public function logout()
	{
		if (!$this->isLogged()) {
			throw new AuthException('You are not logged');
		}
		AuthManager::logout();
		$_SESSION['logout'] = true;
	}
}