<?php
namespace TkachInc\Core\Auth\Types;

use TkachInc\Core\Auth\Core\AuthManager;
use TkachInc\Core\Auth\Core\AuthTrait;
use TkachInc\Core\Auth\Core\CheckAuthTrait;
use TkachInc\Core\Auth\Core\IAuth;
use TkachInc\Core\Auth\Core\ICheckAuth;
use TkachInc\Core\Auth\Model\AdminUser;

/**
 * Class AbstractType
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class AbstractAuthType implements ICheckAuth, IAuth
{
	use CheckAuthTrait;
	use AuthTrait;

	protected static $instance;

	/**
	 * @return AbstractType
	 */
	public static function getInstance()
	{
		if (!isset(static::$instance)) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * @param AdminUser $user
	 */
	protected function setUserParams(AdminUser $user)
	{
		$this->setRole($user->role);
		$this->setGroups($user->groups);
		$this->setReads($user->reads);
		$this->setWrites($user->writes);
	}

	/**
	 * AbstractType constructor.
	 */
	protected function __construct()
	{
		if ($this->isLogged()) {
			$admin = new AdminUser($this->getUser());
			$this->setUserParams($admin);
		}
	}

	public function isLogged()
	{
		return AuthManager::loggedIn();
	}

	public function getUser()
	{
		return AuthManager::getUser();
	}

	/**
	 * @param $name
	 * @return mixed
	 * @throws \Exception
	 */
	public function checkWrite($name)
	{
		if (empty($name)) {
			throw new \Exception('Name not found');
		}

		if ($this->getRole() !== 'root') {
			if (!$this->canWrite($name)) {
				throw new \Exception('Not have permitted');
			}
		}

		$model = str_replace('_', '\\', $name);
		if (!class_exists($model)) {
			throw new \Exception('Namespace error');
		}

		return $model;
	}

	/**
	 * @param $name
	 * @return mixed
	 * @throws \Exception
	 */
	public function checkRead($name)
	{
		if (empty($name)) {
			throw new \Exception('Name not found');
		}

		if ($this->getRole() !== 'root') {
			if (!$this->canRead($name)) {
				throw new \Exception('Not have permitted');
			}
		}

		$model = str_replace('_', '\\', $name);
		if (!class_exists($model)) {
			throw new \Exception('Namespace error');
		}

		return $model;
	}
}