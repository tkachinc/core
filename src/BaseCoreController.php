<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 1/31/17
 * Time: 16:16
 */

namespace TkachInc\Core;

use TkachInc\Engine\Application\BaseApplication;
use TkachInc\Engine\Application\BaseController;
use TkachInc\Engine\ServerContainerFacade;

abstract class BaseCoreController extends BaseController
{
	public function __construct(BaseApplication $application = null)
	{
		parent::__construct($application);
		$this->application->addService(CoreServiceProvider::class);
	}
}