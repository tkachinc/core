<?php
namespace TkachInc\Core\CSRF;

use TkachInc\Core\Storage\Wrappers\Cookie;
use TkachInc\Engine\Services\Helpers\Generator\HashGenerator;

/**
 * @author maxim
 */
class OpenKey
{
	protected $openSessionKey1;
	protected $openSessionKey2;

	public function __construct()
	{
		$this->openSessionKey1 = $_COOKIE['getOpenSessionKey1']??null;
		$this->openSessionKey2 = $_COOKIE['getOpenSessionKey2']??null;
	}

	public function isInit()
	{
		return $this->openSessionKey1 && $this->openSessionKey2;
	}

	public function init()
	{
		$openSessionKey = HashGenerator::generatePasswordByHash(64);
		$openSessionKey2 = HashGenerator::generatePasswordByHash(16);
		$cookie = new Cookie();
		$cookie->set('openSessionKey1', $openSessionKey);
		$cookie->set('openSessionKey2', $openSessionKey2);
		$_COOKIE['openSessionKey1'] = $openSessionKey;
		$_COOKIE['openSessionKey2'] = $openSessionKey2;
	}

	public function getOpenSessionKey1()
	{
		return $this->openSessionKey1;
	}

	public function getOpenSessionKey2()
	{
		return $this->openSessionKey2;
	}
}