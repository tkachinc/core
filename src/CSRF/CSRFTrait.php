<?php
namespace TkachInc\Core\CSRF;

use TkachInc\Core\Storage\Wrappers\InterfaceStorage;

/**
 * @author maxim
 */
trait CSRFTrait
{
	/**
	 * @param InterfaceStorage $storage
	 * @param null $clientCSRF
	 * @param bool $create
	 * @param bool $errorIfNull
	 * @return bool|null|string
	 * @throws \Exception
	 */
	protected function checkCSRF(InterfaceStorage $storage, $clientCSRF = null, $create = true, $errorIfNull = false)
	{
		$csrf = new CSRF($storage);
		$result = $csrf->check($clientCSRF);

		if ($result === null && $create) {
			$result = $csrf->init();
		}

		if (($errorIfNull && !$result) || $result === false) {
			throw new \Exception('CSRF security error!');
		}

		return $result;
	}
}