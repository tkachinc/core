<?php
namespace TkachInc\Core\CSRF;

use TkachInc\Core\Storage\Wrappers\InterfaceStorage;
use TkachInc\Engine\Services\AlgorithmsForCoding\FeistelSecure;
use TkachInc\Engine\Services\AlgorithmsForCoding\XORSecure;
use TkachInc\Engine\Services\Helpers\Generator\HashGenerator;
use TkachInc\Engine\Services\Request\Request;

/**
 * @author maxim
 */
class CSRF
{
	protected $storage;

	/**
	 * CSRF constructor.
	 * @param InterfaceStorage $storage
	 */
	public function __construct(InterfaceStorage $storage)
	{
		$this->storage = $storage;
	}

	public function init()
	{
		$openKey = new OpenKey();
		if (!$openKey->isInit()) {
			$openKey->init();
		}

		return $this->renewCSRF($openKey);
	}

	/**
	 * @param OpenKey $key
	 * @return string
	 */
	protected function renewCSRF(OpenKey $key)
	{
		$token = HashGenerator::getStringUniqueId();

		$token = $this->encryptCSRF($token, $key);

		$this->storage->set('CSRFToken', $token);

		return $token;
	}


	/**
	 * @return bool
	 */
	public function check($clientCSRF)
	{
		$clientToken = $clientCSRF??Request::getRequest('CSRFToken', Request::getRequest('_token', Request::getServerParam('HTTP_X_CSRF_TOKEN', Request::getServerParam('HTTP_X_XSRF_TOKEN'))));

		$openKey = new OpenKey();
		if (!$openKey->isInit()) {
			return null;
		}

		$token = $this->decryptCSRF($clientToken, $openKey);

		$validToken = $this->storage->get('CSRFToken', null);
		if (!$validToken) {
			return null;
		}

		if ($validToken !== $token) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * @param $token
	 * @param OpenKey $key
	 * @return string
	 */
	protected function encryptCSRF($token, OpenKey $key)
	{
		$encrypt2 = new XORSecure($key->getOpenSessionKey1(), $key->getOpenSessionKey2());
		$encrypt = new FeistelSecure();
		return $encrypt2->encode($encrypt->encode($token));
	}

	/**
	 * @param $CSRFToken
	 * @param OpenKey $key
	 * @return string
	 */
	protected function decryptCSRF($CSRFToken, OpenKey $key)
	{
		$encrypt2 = new XORSecure($key->getOpenSessionKey1(), $key->getOpenSessionKey2());
		$encrypt = new FeistelSecure();
		return $encrypt->decode($encrypt2->decode($CSRFToken));
	}
}