<?php
namespace TkachInc\Core\Response;

use TkachInc\Core\URLFacade;
use TkachInc\Engine\Router\Route;


/**
 * Class TwigExtension
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class URLTwigExtension extends \Twig_Extension
{
	protected $route;

	public function __construct(Route $route)
	{
		$this->route = $route;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'URLUpdate';
	}

	/**
	 * @return array
	 */
	public function getFilters()
	{
		return [
			new \Twig_SimpleFilter(
				'addCurrentPrefix', [URLFacade::class, 'addPrefix']),
			new \Twig_SimpleFilter(
				'addQuery', [URLFacade::class, 'addQuery']),
			new \Twig_SimpleFilter(
				'setURLWithoutGET', [URLFacade::class, 'setURLWithoutGET']),
			new \Twig_SimpleFilter(
				'setPrefix', [URLFacade::class, 'setLangPrefix']),
		];
	}

	/**
	 * @return array
	 */
	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction(
				'getScheme', function () {
				return URLFacade::getScheme();
			}
			),
		];
	}
}