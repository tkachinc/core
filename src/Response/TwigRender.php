<?php
namespace TkachInc\Core\Response;

use TkachInc\Core\Config\Config;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class TwigRender
{
	protected $useCache;
	protected $isTestHost;
	/**
	 * @var \Twig_Environment
	 */
	protected $twig;

	/**
	 * TwigRender constructor.
	 */
	public function __construct()
	{
		$this->isTestHost = Config::getInstance()->get(['isTestHost'], false);
		$this->useCache = Config::getInstance()->get(['useCache'], false);
		$this->twig = new \Twig_Environment(
			null, [
				'cache' => $this->useCache ? APP_PATH . '/cache' : false,
				'debug' => $this->isTestHost,
			]
		);
	}

	/**
	 * @param \Twig_ExtensionInterface $extension
	 */
	public function addExtension(\Twig_ExtensionInterface $extension)
	{
		$this->twig->addExtension($extension);
	}

	/**
	 * @param       $function
	 * @param array $options
	 */
	public function addFunction($function, Array $options = [])
	{
		if (function_exists($function)) {
			$this->twig->addFunction($function, new \Twig_Function_Function($function, $options));
		}
	}

	/**
	 * @return \Twig_Environment
	 */
	public function getTwig()
	{
		return $this->twig;
	}

	/**
	 * @param array $data
	 * @param null $templateName
	 * @param       $templateDir
	 * @return string
	 */
	public function render($templateDir, Array $data = [], $templateName = null)
	{
		if ($templateName === null) {
			$templateArr = explode('/', $templateDir);
			$templateName = array_pop($templateArr);
			$templateDir = implode('/', $templateArr);
		}
		$this->twig->setLoader(new \Twig_Loader_Filesystem($templateDir));

		return $this->twig->render($templateName, $data);
	}
} 