<?php
namespace TkachInc\Core\Export;

use Evenement\EventEmitter;
use Pimple\Container;
use TkachInc\CLI\Process\ProcessHelper;
use TkachInc\Engine\ServerContainerFacade;
use TkachInc\Engine\Services\Helpers\Generator\HashGenerator;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ExportManager extends EventEmitter
{
	const TRIGGER_SAVE = 'trigger.save';

	protected $container;
	protected $config;
	protected $isExport;
	protected $exportId;
	/** @var ExportModel|resource */
	protected $outstream;
	protected $prepareDBCallback;
	protected $specialPidId;

	/**
	 * @return string
	 * @throws \Exception
	 */
	public static function checkCurrentExport()
	{
		$pid = (string)getmypid();
		$name = ProcessHelper::getProcessName($pid);
		$_id = HashGenerator::getStringUniqueId();
		$lock = new ExportPids($_id);
		if (!$lock->isLoadedObject()) {
			$lock->_id = $_id;
		}
		$lock->pid = $pid;
		$lock->name = $name;
		$lock->time = time();
		$lock->save();

		return $_id;
	}

	/**
	 * @param Container$container
	 * @param InterfaceExportConfig $config
	 * @param bool $isExport
	 * @param \Closure $prepareDBCallback
	 * @param null $specialPidId
	 */
	public function __construct(Container $container,
	                            InterfaceExportConfig $config,
	                            $isExport = true,
	                            \Closure $prepareDBCallback = null,
	                            $specialPidId = null)
	{
		$this->config = $config;
		$this->isExport = $isExport;
		if ($specialPidId !== null) {
			$this->exportId = $specialPidId;
		}
		$this->prepareDBCallback = $prepareDBCallback;
		$this->specialPidId = $specialPidId;

		ExportHistory::start($this->exportId);

		ExportModel::disableCache();
	}

	/**
	 * @return string
	 */
	public function getExportId()
	{
		return $this->exportId;
	}

	/**
	 * @param array $fields
	 */
	public function sendToDb(Array $fields)
	{
		$this->outstream = new ExportModel();
		$this->outstream->id = $this->exportId;
		$this->outstream->fields = $fields;
		$this->outstream->save();
	}

	/**
	 * @param array $fields
	 */
	public function sendToOutStream(Array $fields)
	{
		foreach ($fields as &$field) {
			if (is_array($field) || is_object($field)) {
				$field = json_encode($field);
			}
		}
		fputcsv($this->outstream, $fields);
	}

	public function prepare()
	{
		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		session_write_close();
		while (ob_get_level()) {
			ob_end_clean();
		}
	}

	public function prepareCSV()
	{
		setlocale(LC_ALL, 'ru_RU.utf8', 'ru_RU.UTF-8');
		setlocale(LC_NUMERIC, 'C');
		header('Content-type: text/csv');
		header('Content-Disposition: attachment; filename="export_' . $this->exportId . '.csv"');
		/** @var resource outstream */
		$this->outstream = fopen("php://output", 'w');
		$this->on(self::TRIGGER_SAVE, [$this, 'sendToOutStream']);
	}

	public function prepareDB()
	{
		if ($this->prepareDBCallback !== null) {
			call_user_func_array($this->prepareDBCallback, [$this->exportId, $this->isExport]);
		}
		if (function_exists('fastcgi_finish_request')) {
			fastcgi_finish_request();
		}
		$this->on(self::TRIGGER_SAVE, [$this, 'sendToDb']);
	}

	public function end()
	{
		if ($this->isExport) {
			fclose($this->outstream);
		}
		ob_start();
	}

	public function run()
	{
		$this->prepare();
		if ($this->isExport) {
			$this->prepareCSV();
		} else {
			$this->prepareDB();
		}

		foreach ($this->config->export($this->isExport) as $object) {
			$this->emit(self::TRIGGER_SAVE, [$object]);
			usleep(10);
		}

		$this->end();
		if ($this->specialPidId !== null) {
			register_shutdown_function(
				function () {
					$pid = new ExportPids($this->specialPidId);
					if ($pid->isLoadedObject()) {
						$pid->delete();
					}
				}
			);
		}
		ExportHistory::end($this->exportId);
	}
}