<?php
namespace TkachInc\Core\Export;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ExportException extends \Exception
{
	const ERROR_NOT_FOUND_CLASS = 1;
} 