<?php
namespace TkachInc\Core\Export;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @property mixed id
 * @property bool complete
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ExportHistory extends ObjectModel
{
	protected static $_collection = 'export_history';
	protected static $_pk = 'id';
	protected static $_indexes = [
		[
			'keys' => ['id' => 1],
		],
		[
			'keys' => ['complete' => 1],
		],
	];

	protected static $_fieldsDefault = [
		'id'       => '',
		'complete' => false,
		'emails'   => [],
	];

	protected static $_fieldsValidate = [
		'id'       => self::TYPE_STRING,
		'complete' => self::TYPE_BOOL,
		'emails'   => self::TYPE_JSON,
	];

	/**
	 * @param $exportId
	 */
	public static function start($exportId)
	{
		$history = new ExportHistory($exportId);
		if (!$history->isLoadedObject()) {
			$history->id = $exportId;
			$history->save();
		}
		$history->clearStaticCache();
	}

	/**
	 * @param $exportId
	 */
	public static function end($exportId)
	{
		$history = new ExportHistory($exportId);
		if ($history->isLoadedObject()) {
			$history->complete = true;
			$history->save();
		}

		$history->clearStaticCache();
	}

	/**
	 * @param $exportId
	 * @return bool
	 */
	public static function isComplete($exportId)
	{
		$history = new ExportHistory($exportId);
		if ($history->isLoadedObject()) {
			return $history->complete;
		}

		return false;
	}
}