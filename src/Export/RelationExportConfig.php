<?php
namespace TkachInc\Core\Export;
use TkachInc\Engine\Services\Helpers\ArrayHelper;


/**
 * Class Classes
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class RelationExportConfig implements InterfaceExportConfig
{
	protected $generator;
	protected $isExport;
	protected $defaultFields;

	/**
	 * RelationExportConfig constructor.
	 *
	 * @param \Generator $generator
	 * @param array $defaultFields
	 */
	public function __construct(\Generator $generator, Array $defaultFields)
	{
		$this->generator = $generator;
		$this->defaultFields = $defaultFields;
	}

	/**
	 * @param bool $isExport
	 * @return \Generator
	 */
	public function export($isExport)
	{
		$i = 0;
		foreach ($this->generator as $dbobject) {
			if ($i === 0 && $isExport) {
				yield array_keys($this->defaultFields);
			}
			yield ArrayHelper::filterOnlyRequiredParams($dbobject, $this->defaultFields);
			$i++;
		}
	}
}