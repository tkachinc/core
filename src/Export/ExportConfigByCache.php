<?php
namespace TkachInc\Core\Export;

use TkachInc\Core\Database\MongoDB\ObjectModel;
use TkachInc\Engine\Services\Helpers\ArrayHelper;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ExportConfigByCache extends ExportConfig
{
	/**
	 * ExportConfigByCache constructor.
	 *
	 * @param $id
	 * @throws ExportException
	 */
	public function __construct($id)
	{
		/** @var ObjectModel $model */
		$this->model = ExportModel::class;
		$this->filter = [];
		$this->params = ['id' => $id];
		$this->fieldsExport = ['fields' => 1];

		if (!class_exists($this->model) || !method_exists($this->model, 'getFieldsValidate')) {
			throw new ExportException('Not found class', ExportException::ERROR_NOT_FOUND_CLASS);
		}

		$model = $this->model;
		$this->fieldsValidate = $model::getFieldsValidate();
	}

	/**
	 * @param bool $isExport
	 * @return \Generator
	 */
	public function export($isExport)
	{
		$cursor = $this->getModelCursor();
		$fieldsTitle = [];
		$fields = [];
		$itr = 0;
		while ($cursor->hasNext()) {
			$dbobject = $cursor->getNext();
			$object = $dbobject['fields'];
			if (empty($fieldsTitle)) {
				$fieldsTitle = array_keys($object);
				foreach ($object as $key => $item) {
					$fields[$key] = '';
				}
				yield $fieldsTitle;
			}
			foreach ($object as &$field) {
				if (is_array($field) || is_object($field)) {
					$field = json_encode($field);
				}
			}
			yield ArrayHelper::filterOnlyRequiredParams($object, $fields);
			$itr++;
		}
	}
}