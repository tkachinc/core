<?php
namespace TkachInc\Core\Export;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * Class ExportPids
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ExportPids extends ObjectModel
{
	protected static $_collection = 'export_pids';
	protected static $_pk = '_id';
	protected static $_indexes = [
		[
			'keys' => ['_id' => 1],
		],
	];

	protected static $_fieldsDefault = [
		'_id'  => '',
		'pid'  => '',
		'name' => '',
		'time' => 0,
	];

	protected static $_fieldsValidate = [
		'_id'  => self::TYPE_STRING,
		'pid'  => self::TYPE_STRING,
		'name' => self::TYPE_STRING,
		'time' => self::TYPE_TIMESTAMP,
	];

}