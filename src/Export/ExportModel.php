<?php
namespace TkachInc\Core\Export;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @property mixed id
 * @property array fields
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ExportModel extends ObjectModel
{
	protected static $_collection = 'export_model';
	protected static $_pk = '_id';
	protected static $_indexes = [
		[
			'keys' => ['id' => 1],
		],
		[
			'keys' => ['action' => 1],
		],
	];

	protected static $_fieldsDefault = [
		'_id'    => '',
		'id'     => '',
		'fields' => [],
	];

	protected static $_fieldsValidate = [
		'_id'    => self::TYPE_MONGO_ID,
		'id'     => self::TYPE_STRING,
		'fields' => self::TYPE_JSON,
	];
}