<?php
namespace TkachInc\Core\Export;

use TkachInc\Core\Database\MongoDB\Helper\MongoConditionChecker;
use TkachInc\Core\Database\MongoDB\ObjectModel;
use TkachInc\Engine\Services\Helpers\ArrayHelper;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ExportConfig implements InterfaceExportConfig
{
	protected $model;
	protected $filter;
	protected $params;
	protected $fieldsExport;
	protected $fieldsDefault;
	protected $fieldsValidate;

	/**
	 * @param       $model
	 * @param array $filter
	 * @param array $params
	 * @param array $fieldsDefault
	 * @param array $fieldsExport
	 * @throws ExportException
	 */
	public function __construct($model,
	                            Array $filter = [],
	                            Array $params = [],
	                            Array $fieldsDefault = [],
	                            Array $fieldsExport = [])
	{
		/** @var ObjectModel $model */
		$this->model = $model;
		$this->filter = $filter;
		$this->params = $params;
		$this->fieldsDefault = $fieldsDefault;
		$this->fieldsExport = $fieldsExport;

		if (!class_exists($this->model) || !method_exists($this->model, 'getFieldsValidate')) {
			throw new ExportException('Not found class', ExportException::ERROR_NOT_FOUND_CLASS);
		}

		$this->fieldsValidate = $model::getFieldsValidate();
	}

	/**
	 * @return array
	 */
	public function getFieldsExport()
	{
		return $this->fieldsExport;
	}

	public function getFieldsDefault()
	{
		return $this->fieldsDefault;
	}

	/**
	 * @return array
	 */
	public function getFieldsValidate()
	{
		return $this->fieldsValidate;
	}

	/**
	 * @param       $model
	 * @param array $params
	 * @param array $fields
	 * @return mixed
	 */
	protected function getCursor($model, Array $params, Array $fields)
	{
		/** @var ObjectModel $model */
		$model::disableCache();
		$cursor = $model::getAllAdvanced(
			$params,
			$fields,
			false,
			true,
			false,
			false,
			true
		);

		return $cursor;
	}

	/**
	 * @return mixed
	 */
	public function getModelCursor()
	{
		return $this->getCursor($this->model, $this->params, $this->fieldsExport);
	}

	/**
	 * @param array $dbobject
	 * @return bool
	 */
	public function checkParam(Array $dbobject)
	{
		if (!empty($this->filter)) {
			return MongoConditionChecker::evaluate($this->filter, $dbobject);
		}

		return true;
	}

	/**
	 * @param bool $isExport
	 * @return \Generator
	 */
	public function export($isExport)
	{
		$fieldsValidate = $this->getFieldsValidate();
		$fieldsExport = $this->getFieldsExport();
		$fieldsDefault = $this->getFieldsDefault();
		$fieldsTitle = array_keys($this->fieldsExport);

		$modelCursor = $this->getModelCursor();
		$i = 0;
		foreach ($modelCursor as $dbobject) {
			if ($i === 0 && $isExport) {
				yield $fieldsTitle;
			}
			if ($this->checkParam($dbobject)) {
				$object = [];
				foreach ($fieldsExport as $field => $isset) {
					if (!isset($dbobject[$field])) {
						$dbobject[$field] = null;
					}
					$object[$field] = ObjectModel::encode($dbobject[$field], $fieldsValidate[$field]);
				}
				if (is_array($object)) {
					yield ArrayHelper::filterOnlyRequiredParams($object, $fieldsDefault);
				}
			}
			$i++;
		}
	}
}