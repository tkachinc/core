<?php
namespace TkachInc\Core\Export;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
interface InterfaceExportConfig
{
	/**
	 * @param bool $isExport
	 * @return \Generator
	 */
	public function export($isExport);
}