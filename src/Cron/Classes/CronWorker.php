<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 1/4/17
 * Time: 16:10
 */

namespace TkachInc\Core\Cron\Classes;

use Jobby\Jobby;
use TkachInc\CLI\Process\ProcessHelper;
use TkachInc\Core\Cron\Model\CronModel;

trait CronWorker
{
	public function run()
	{
		$jobby = new Jobby();

		$cursor = CronModel::getAllAdvanced([], [], false, true, false, false, true);
		foreach ($cursor as $item) {
			if (empty($item['output'])) {
				$item['output'] = '/dev/null';
			}

			if (empty($item['attributes'])) {
				$item['attributes'][] = [''];
			}

			if (empty($item['flag'])) {
				$item['flag'] = '-r';
			}

			$php = ProcessHelper::getPHPBinary();

			foreach ($item['attributes'] as $attribute) {
				$jobby->add((string)$item['_id'], [
					// Run a shell commands
					'command'  => $php . ' ' . APP_PATH . '/main.php -e ' . $item['flag'] . ' ' . $item['command'] . ' -a ' . str_replace(['&', '='], ['+', '='], http_build_query($attribute)),

					// Ordinary crontab schedule format is supported.
					// This schedule runs every hour.
					// You could also insert DateTime string in the format of Y-m-d H:i:s.
					'schedule' => $item['schedule'],

					// Stdout and stderr is sent to the specified file
					'output'   => $item['output'],

					// You can turn off a job by setting 'enabled' to false
					'enabled'  => $item['enabled'],
				]);
			}
		}

		$jobby->run();
	}
}