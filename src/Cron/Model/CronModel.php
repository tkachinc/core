<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 1/4/17
 * Time: 16:13
 */

namespace TkachInc\Core\Cron\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

class CronModel extends ObjectModel
{
	protected static $_collection = 'cron_model';
	protected static $_pk = '_id';
	protected static $_sort = ['_id' => -1];
	protected static $_indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true,
		],
	];
	protected static $_fieldsDefault = [
		'_id'     => '',
		'name'     => '',
		'flag'     => '-r',
		'command'     => '',
		'schedule'     => '',
		'output'     => '',
		'enabled'     => '',
		'attributes'     => [],
	];
	protected static $_fieldsValidate = [
		'_id'     => self::TYPE_MONGO_ID,
		'name'     => self::TYPE_STRING,
		'flag'     => self::TYPE_STRING,
		'command'     => self::TYPE_STRING,
		'schedule'     => self::TYPE_STRING,
		'output'     => self::TYPE_STRING,
		'enabled'     => self::TYPE_BOOL,
		'attributes'     => self::TYPE_JSON,
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;
	protected static $_updateMethod = self::UPDATE_METHOD_SET;
}