<?php
namespace TkachInc\Core\MetaData;

use TkachInc\Core\Config\Config;

/**
 * @author maxim
 */
class MetaData
{
	const DEFAULT_META = 'default';

	const INDEX_THIS_PAGE = 'INDEX';
	const NOINDEX_THIS_PAGE = 'NOINDEX';
	const FOLLOW_THIS_PAGE = 'FOLLOW';
	const NOFOLLOW_THIS_PAGE = 'NOFOLLOW';

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @var string
	 */
	protected $title;

	/**
	 * @var string
	 */
	protected $siteName;

	/**
	 * @var array
	 */
	protected $meta = [];

	/**
	 * @var array
	 */
	protected $custom = [];

	/**
	 * @var bool
	 */
	protected $index = true;

	/**
	 * @var bool
	 */
	protected $follow = true;

	/**
	 * MetaData constructor.
	 *
	 * @param string      $type
	 * @param string|null $title
	 */
	public function __construct(string $type, string $title = null)
	{
		$this->siteName = (string)ucfirst(Config::getInstance()->get(['siteName']));

		if (empty($title)) {
			$title = $this->getDefaultValue('title');
		}

		$this->type = $type;

		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getSiteName(): string
	{
		return $this->siteName;
	}

	/**
	 * @param string      $name
	 * @param string|null $content
	 * @param string      $nametag
	 *
	 * @return MetaData
	 */
	public function addMeta(string $name, string $content = null, string $nametag = 'name'): MetaData
	{
		if (!empty($nametag) && !empty($name) && !empty($content)) {
			$this->meta[] = ['nametag' => $nametag, 'name' => $name, 'content' => $content];
		}

		return $this;
	}

	/**
	 * @param string $attr
	 *
	 * @return MetaData
	 */
	public function addMetaCustom(string $attr = ''): MetaData
	{
		if (!empty($attr)) {
			$this->custom[] = ['attr' => $attr];
		}

		return $this;
	}

	/**
	 * @return array
	 */
	public function get(): array
	{
		$result = [
			'type'     => $this->type,
			'siteName' => $this->siteName,
			'data'     => $this->meta,
			'custom'   => $this->custom,
			'title'    => $this->getTitle(),
			'robot'    => $this->getRobot(),
		];

		return $result;
	}

	/**
	 * @return string
	 */
	public function getRobot()
	{
		$robot = [];
		if($this->index) {
			$robot[] = self::INDEX_THIS_PAGE;
		} else {
			$robot[] = self::NOINDEX_THIS_PAGE;
		}

		if($this->follow) {
			$robot[] = self::FOLLOW_THIS_PAGE;
		} else {
			$robot[] = self::NOFOLLOW_THIS_PAGE;
		}
		return implode(',', $robot);
	}

	/**
	 * @param bool $withSiteName
	 *
	 * @return string
	 */
	public function getTitle($withSiteName = true)
	{
		if (empty($this->title)) {
			$title = $this->siteName;
		} else {
			if ($withSiteName) {
				$title = $this->title . ' | ' . $this->siteName;
			} else {
				$title = $this->title;
			}
		}

		return $title;
	}

	/**
	 * @param $title
	 *
	 * @return $this
	 */
	public function setTitle($title)
	{
		$this->title = $title;

		return $this;
	}

	public function getDefaultValue($tag, $default = '')
	{
		return Config::getInstance()->get(['meta',
			'tags',
			$this->type,
			$tag],
			Config::getInstance()->get(['meta',
				'tags',
				self::DEFAULT_META,
				$tag,
			],
				$default));
	}

	/**
	 * @return string
	 */
	public function getBlock(): string
	{
		return 'templates/meta.html.twig';
	}

	/**
	 * @param bool $value
	 *
	 * @return $this
	 */
	public function setIndexThisPage(bool $value = true)
	{
		$this->index = $value;

		return $this;
	}

	/**
	 * @param bool $value
	 *
	 * @return $this
	 */
	public function setFollowThisPage(bool $value = true)
	{
		$this->follow = $value;

		return $this;
	}
}