<?php
namespace TkachInc\Core\MetaData\Schema;

/**
 * @author maxim
 */
abstract class AbstractScheme
{

	/**
	 * @var string
	 */
	protected static $context = 'http://schema.org';

	/**
	 * @var null
	 */
	protected static $type = null;

	/**
	 * @var array
	 */
	protected static $fields = [];

	/**
	 * @var array
	 */
	protected static $fieldMerged = [];

	/**
	 * @var array
	 */
	protected $result = [];

	/**
	 * @return array
	 */
	protected static function getFields()
	{
		$current = get_called_class();

		if (array_search($current . 'getFields', static::$fieldMerged) === false) {
			static::$fields = static::mergeField('getFields', static::$fields);
			static::$fieldMerged[] = $current . 'fields';
		}

		return static::$fields;
	}

	/**
	 * @param $funcName
	 * @param $data
	 *
	 * @return array
	 */
	protected static function mergeField($funcName, $data)
	{
		$current = get_called_class();
		$parent = get_parent_class($current);

		if ($parent && !empty($parent)) {
			return array_merge($parent::$funcName(), $data);
		} else {
			return $data;
		}
	}

	/**
	 * @param      $key
	 * @param      $value
	 * @param bool $force
	 *
	 * @return $this
	 */
	public function add($key, $value, $force = false)
	{
		if ($force || array_key_exists($key, static::getFields())) {
			$this->result[$key] = $value;
		}

		return $this;
	}

	/**
	 * @return array
	 */
	public function get()
	{
		$this->result['@type'] = static::$type;

		return $this->result;
	}

	/**
	 * @return string
	 */
	public function json()
	{
		return json_encode($this->getWithContext());
	}

	/**
	 * @param null $context
	 *
	 * @return array
	 */
	public function getWithContext($context = null)
	{
		$result = $this->get();

		if (!$context) {
			$context = static::$context;
		}

		$result['@context'] = $context;

		return $result;
	}

	/**
	 * @return string
	 */
	public function getHtml()
	{
		$html = '<script type="application/ld+json">';
		$html .= $this->json();
		$html .= '</script>';

		return $html;
	}
}