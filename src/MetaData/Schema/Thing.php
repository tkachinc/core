<?php
namespace TkachInc\Core\MetaData\Schema;

/**
 * @author maxim
 */
class Thing extends AbstractScheme
{

	protected static $type = 'Thing';

	protected static $fields = [
		'additionalType'            => null,
		'alternateName'             => null,
		'description'               => null,
		'disambiguatingDescription' => null,
		'image'                     => null,
		'mainEntityOfPage'          => null,
		'name'                      => null,
		'potentialAction'           => null,
		'sameAs'                    => null,
		'url'                       => null,
	];
}