<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks;

use TkachInc\Core\MetaData\Schema\Things\CreativeWork;

/**
 * @author maxim
 */
class Question extends CreativeWork
{

	protected static $type = 'Question';

	protected static $fields = [
		'acceptedAnswer'  => null,
		'answerCount'     => null,
		'downvoteCount'   => null,
		'suggestedAnswer' => null,
		'upvoteCount'     => null,
	];
}