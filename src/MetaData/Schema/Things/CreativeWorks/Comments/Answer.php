<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks\Comments;

use TkachInc\Core\MetaData\Schema\Things\CreativeWorks\Comment;

/**
 * @author maxim
 */
class Answer extends Comment
{

	protected static $type = 'Answer';
}