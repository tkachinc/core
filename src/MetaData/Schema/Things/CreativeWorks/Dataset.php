<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks;

use TkachInc\Core\MetaData\Schema\Things\CreativeWork;

/**
 * @author maxim
 */
class Dataset extends CreativeWork
{

	protected static $type = 'Dataset';

	protected static $fields = [
		'distribution'          => null,
		'includedInDataCatalog' => null,
	];
}