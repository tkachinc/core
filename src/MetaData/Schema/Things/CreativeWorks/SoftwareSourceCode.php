<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks;

use TkachInc\Core\MetaData\Schema\Things\CreativeWork;

/**
 * @author maxim
 */
class SoftwareApplication extends CreativeWork
{

	protected static $type = 'SoftwareApplication';

	protected static $fields = [
		'applicationCategory'    => null,
		'applicationSubCategory' => null,
		'applicationSuite'       => null,
		'availableOnDevice'      => null,
		'countriesNotSupported'  => null,
		'countriesSupported'     => null,
		'downloadUrl'            => null,
		'featureList'            => null,
		'fileSize'               => null,
		'installUrl'             => null,
		'memoryRequirements'     => null,
		'operatingSystem'        => null,
		'permissions'            => null,
		'processorRequirements'  => null,
		'releaseNotes'           => null,
		'screenshot'             => null,
		'softwareAddOn'          => null,
		'softwareHelp'           => null,
		'softwareRequirements'   => null,
		'softwareVersion'        => null,
		'storageRequirements'    => null,
		'supportingData'         => null,
	];
}