<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks;

use TkachInc\Core\MetaData\Schema\Things\CreativeWork;

/**
 * @author maxim
 */
class Article extends CreativeWork
{

	protected static $type = 'Article';

	protected static $fields = [
		'articleBody'    => null,
		'articleSection' => null,
		'pageEnd'        => null,
		'pageStart'      => null,
		'pagination'     => null,
		'wordCount'      => null,
	];
}