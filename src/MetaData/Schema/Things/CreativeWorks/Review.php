<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks;

use TkachInc\Core\MetaData\Schema\Things\CreativeWork;

/**
 * @author maxim
 */
class Review extends CreativeWork
{

	protected static $type = 'Review';

	protected static $fields = [
		'itemReviewed' => null,
		'reviewBody'   => null,
		'reviewRating' => null,
	];
}