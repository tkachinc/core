<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks\WebPages;

use TkachInc\Core\MetaData\Schema\Things\CreativeWorks\WebPage;

/**
 * @author maxim
 */
class ItemPage extends WebPage
{

	protected static $type = 'ItemPage';
}