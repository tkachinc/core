<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks;

use TkachInc\Core\MetaData\Schema\Things\CreativeWork;

/**
 * @author maxim
 */
class DataCatalog extends CreativeWork
{

	protected static $type = 'DataCatalog';

	protected static $fields = [
		'dataset' => null,
	];
}