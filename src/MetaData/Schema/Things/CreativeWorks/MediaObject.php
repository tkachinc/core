<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks;

use TkachInc\Core\MetaData\Schema\Things\CreativeWork;

/**
 * @author maxim
 */
class MediaObject extends CreativeWork
{

	protected static $type = 'MediaObject';

	protected static $fields = [
		'associatedArticle'    => null,
		'bitrate'              => null,
		'contentSize'          => null,
		'contentUrl'           => null,
		'duration'             => null,
		'embedUrl'             => null,
		'encodesCreativeWork'  => null,
		'encodingFormat'       => null,
		'expires'              => null,
		'height'               => null,
		'playerType'           => null,
		'productionCompany'    => null,
		'regionsAllowed'       => null,
		'requiresSubscription' => null,
		'uploadDate'           => null,
		'width'                => null,
	];
}