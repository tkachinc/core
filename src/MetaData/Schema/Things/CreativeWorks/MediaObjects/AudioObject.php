<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks\MediaObjects;

use TkachInc\Core\MetaData\Schema\Things\CreativeWorks\MediaObject;

/**
 * @author maxim
 */
class AudioObject extends MediaObject
{

	protected static $type = 'AudioObject';

	protected static $fields = [
		'transcript' => null,
	];
}