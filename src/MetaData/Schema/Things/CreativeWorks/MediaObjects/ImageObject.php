<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks\MediaObjects;

use TkachInc\Core\MetaData\Schema\Things\CreativeWorks\MediaObject;

/**
 * @author maxim
 */
class ImageObject extends MediaObject
{

	protected static $type = 'ImageObject';

	protected static $fields = [
		'caption'              => null,
		'exifData'             => null,
		'representativeOfPage' => null,
		'thumbnail'            => null,
	];
}