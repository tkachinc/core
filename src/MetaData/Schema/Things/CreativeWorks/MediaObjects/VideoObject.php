<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks\MediaObjects;

use TkachInc\Core\MetaData\Schema\Things\CreativeWorks\MediaObject;

/**
 * @author maxim
 */
class VideoObject extends MediaObject
{

	protected static $type = 'VideoObject';

	protected static $fields = [
		'actor'          => null,
		'caption'        => null,
		'director'       => null,
		'musicBy'        => null,
		'thumbnail'      => null,
		'transcript'     => null,
		'videoFrameSize' => null,
		'videoQuality'   => null,
	];
}