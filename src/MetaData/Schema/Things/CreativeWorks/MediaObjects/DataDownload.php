<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks\MediaObjects;

use TkachInc\Core\MetaData\Schema\Things\CreativeWorks\MediaObject;

/**
 * @author maxim
 */
class DataDownload extends MediaObject
{

	protected static $type = 'DataDownload';
}