<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks;

use TkachInc\Core\MetaData\Schema\Things\CreativeWork;

/**
 * @author maxim
 */
class WebPage extends CreativeWork
{

	protected static $type = 'WebPage';

	protected static $fields = [
		'breadcrumb'         => null,
		'lastReviewed'       => null,
		'mainContentOfPage'  => null,
		'primaryImageOfPage' => null,
		'relatedLink'        => null,
		'reviewedBy'         => null,
		'significantLink'    => null,
		'specialty'          => null,
	];
}