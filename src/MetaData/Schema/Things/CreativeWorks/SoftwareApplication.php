<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks;

use TkachInc\Core\MetaData\Schema\Things\CreativeWork;

/**
 * @author maxim
 */
class SoftwareSourceCode extends CreativeWork
{

	protected static $type = 'SoftwareSourceCode';

	protected static $fields = [
		'codeRepository'      => null,
		'codeSampleType'      => null,
		'programmingLanguage' => null,
		'runtimePlatform'     => null,
		'targetProduct'       => null,
	];
}