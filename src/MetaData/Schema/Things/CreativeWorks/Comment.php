<?php
namespace TkachInc\Core\MetaData\Schema\Things\CreativeWorks;

use TkachInc\Core\MetaData\Schema\Things\CreativeWork;

/**
 * @author maxim
 */
class Comment extends CreativeWork
{

	protected static $type = 'Comment';

	protected static $fields = [
		'downvoteCount' => null,
		'parentItem'    => null,
		'upvoteCount'   => null,
	];
}