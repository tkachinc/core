<?php
namespace TkachInc\Core\MetaData\Schema\Things;

use TkachInc\Core\MetaData\Schema\Thing;

/**
 * @author maxim
 */
class Place extends Thing
{

	protected static $type = 'Place';

	protected static $fields = [
		'additionalProperty'               => null,
		'address'                          => null,
		'aggregateRating'                  => null,
		'amenityFeature'                   => null,
		'branchCode'                       => null,
		'containedInPlace'                 => null,
		'containsPlace'                    => null,
		'event'                            => null,
		'faxNumber'                        => null,
		'geo'                              => null,
		'globalLocationNumber'             => null,
		'hasMap'                           => null,
		'isicV4'                           => null,
		'logo'                             => null,
		'openingHoursSpecification'        => null,
		'photo'                            => null,
		'review'                           => null,
		'smokingAllowed'                   => null,
		'specialOpeningHoursSpecification' => null,
		'telephone'                        => null,
	];
}