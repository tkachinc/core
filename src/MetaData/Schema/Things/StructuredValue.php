<?php
namespace TkachInc\Core\MetaData\Schema\Things;

use TkachInc\Core\MetaData\Schema\Thing;

/**
 * @author maxim
 */
class StructuredValue extends Thing
{

	protected static $type = 'StructuredValue';
}