<?php
namespace TkachInc\Core\MetaData\Schema\Things;

use TkachInc\Core\MetaData\Schema\Thing;

/**
 * @author maxim
 */
class Person extends Thing
{

	protected static $type = 'Person';

	protected static $fields = [
		'additionalName'       => null,
		'address'              => null,
		'affiliation'          => null,
		'alumniOf'             => null,
		'award'                => null,
		'birthDate'            => null,
		'birthPlace'           => null,
		'brand'                => null,
		'children'             => null,
		'colleague'            => null,
		'contactPoint'         => null,
		'deathDate'            => null,
		'deathPlace'           => null,
		'duns'                 => null,
		'email'                => null,
		'familyName'           => null,
		'faxNumber'            => null,
		'follows'              => null,
		'funder'               => null,
		'gender'               => null,
		'givenName'            => null,
		'globalLocationNumber' => null,
		'hasOfferCatalog'      => null,
		'hasPOS'               => null,
		'height'               => null,
		'homeLocation'         => null,
		'honorificPrefix'      => null,
		'honorificSuffix'      => null,
		'isicV4'               => null,
		'jobTitle'             => null,
		'knows'                => null,
		'makesOffer'           => null,
		'memberOf'             => null,
		'naics'                => null,
		'nationality'          => null,
		'netWorth'             => null,
		'owns'                 => null,
		'parent'               => null,
		'performerIn'          => null,
		'relatedTo'            => null,
		'seeks'                => null,
		'sibling'              => null,
		'sponsor'              => null,
		'spouse'               => null,
		'taxID'                => null,
		'telephone'            => null,
		'vatID'                => null,
		'weight'               => null,
		'workLocation'         => null,
		'worksFor'             => null,
	];
}