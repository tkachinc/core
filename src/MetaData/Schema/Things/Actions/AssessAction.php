<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions;

use TkachInc\Core\MetaData\Schema\Things\Action;

/**
 * @author maxim
 */
class AssessAction extends Action
{

	protected static $type = 'AssessAction';
}