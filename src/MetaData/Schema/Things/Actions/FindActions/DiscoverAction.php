<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\FindActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\FindAction;

/**
 * @author maxim
 */
class DiscoverAction extends FindAction
{

	protected static $type = 'DiscoverAction';
}