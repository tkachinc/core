<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\FindActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\FindAction;

/**
 * @author maxim
 */
class TrackAction extends FindAction
{

	protected static $type = 'TrackAction';

	protected static $fields = [
		'deliveryMethod' => null,
	];
}