<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\AssessActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\AssessAction;

/**
 * @author maxim
 */
class ReviewAction extends AssessAction
{

	protected static $type = 'ReviewAction';

	protected static $fields = [
		'resultReview' => null,
	];
}