<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\AssessActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\AssessAction;

/**
 * @author maxim
 */
class IgnoreAction extends AssessAction
{

	protected static $type = 'IgnoreAction';
}