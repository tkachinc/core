<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\AssessActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\AssessAction;

/**
 * @author maxim
 */
class ChooseAction extends AssessAction
{

	protected static $type = 'ChooseAction';

	protected static $fields = [
		'actionOption' => null,
	];
}