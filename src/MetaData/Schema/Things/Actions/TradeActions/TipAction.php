<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\TradeActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\TradeAction;

/**
 * @author maxim
 */
class TipAction extends TradeAction
{

	protected static $type = 'TipAction';

	protected static $fields = [
		'recipient' => null,
	];
}