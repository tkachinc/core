<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\TradeActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\TradeAction;

/**
 * @author maxim
 */
class RentAction extends TradeAction
{

	protected static $type = 'RentAction';

	protected static $fields = [
		'landlord'        => null,
		'realEstateAgent' => null,
	];
}