<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\TradeActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\TradeAction;

/**
 * @author maxim
 */
class SellAction extends TradeAction
{

	protected static $type = 'SellAction';

	protected static $fields = [
		'buyer' => null,
	];
}