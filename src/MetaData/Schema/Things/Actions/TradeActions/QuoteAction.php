<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\TradeActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\TradeAction;

/**
 * @author maxim
 */
class QuoteAction extends TradeAction
{

	protected static $type = 'QuoteAction';

	protected static $fields = [
		'price'              => null,
		'priceSpecification' => null,
	];
}