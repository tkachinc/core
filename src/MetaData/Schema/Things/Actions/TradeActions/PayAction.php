<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\TradeActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\TradeAction;

/**
 * @author maxim
 */
class PayAction extends TradeAction
{

	protected static $type = 'PayAction';

	protected static $fields = [
		'recipient' => null,
	];
}