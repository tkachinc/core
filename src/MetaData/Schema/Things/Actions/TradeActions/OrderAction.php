<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\TradeActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\TradeAction;

/**
 * @author maxim
 */
class OrderAction extends TradeAction
{

	protected static $type = 'OrderAction';

	protected static $fields = [
		'deliveryMethod' => null,
	];
}