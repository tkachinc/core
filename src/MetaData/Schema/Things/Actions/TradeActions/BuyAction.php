<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\TradeActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\TradeAction;

/**
 * @author maxim
 */
class BuyAction extends TradeAction
{

	protected static $type = 'BuyAction';

	protected static $fields = [
		'seller' => null,
	];
}