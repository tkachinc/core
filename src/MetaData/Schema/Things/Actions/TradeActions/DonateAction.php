<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions\TradeActions;

use TkachInc\Core\MetaData\Schema\Things\Actions\TradeAction;

/**
 * @author maxim
 */
class DonateAction extends TradeAction
{

	protected static $type = 'DonateAction';

	protected static $fields = [
		'recipient' => null,
	];
}