<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions;

use TkachInc\Core\MetaData\Schema\Things\Action;

/**
 * @author maxim
 */
class SearchAction extends Action
{

	protected static $type = 'SearchAction';

	protected static $fields = [
		'query'       => null,
		'query-input' => null,
	];
}