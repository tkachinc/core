<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions;

use TkachInc\Core\MetaData\Schema\Things\Action;

/**
 * @author maxim
 */
class FindAction extends Action
{

	protected static $type = 'FindAction';
}