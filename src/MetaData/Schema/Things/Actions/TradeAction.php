<?php
namespace TkachInc\Core\MetaData\Schema\Things\Actions;

use TkachInc\Core\MetaData\Schema\Things\Action;

/**
 * @author maxim
 */
class TradeAction extends Action
{

	protected static $type = 'TradeAction';

	protected static $fields = [
		'price'              => null,
		'priceSpecification' => null,
	];
}