<?php
/**
 * Created by PhpStorm.
 * User: Maksym Tkach
 * Date: 5/3/17
 * Time: 11:58
 */

namespace TkachInc\Core\MetaData\Schema\Things;


use TkachInc\Core\MetaData\Schema\Thing;

class LocalBusiness extends Thing
{
	protected static $type = 'LocalBusiness';

	protected static $fields = [
		'currenciesAccepted' => null,
		'openingHours'       => null,
		'paymentAccepted'    => null,
		'priceRange'         => null,

		'address'              => null,
		'aggregateRating'      => null,
		'alumni'               => null,
		'areaServed'           => null,
		'award'                => null,
		'brand'                => null,
		'contactPoint'         => null,
		'department'           => null,
		'dissolutionDate'      => null,
		'duns'                 => null,
		'email'                => null,
		'employee'             => null,
		'event'                => null,
		'faxNumber'            => null,
		'founder'              => null,
		'foundingDate'         => null,
		'foundingLocation'     => null,
		'funder'               => null,
		'globalLocationNumber' => null,
		'hasOfferCatalog'      => null,
		'hasPOS'               => null,
		'isicV4'               => null,
		'legalName'            => null,
		'leiCode'              => null,
		'location'             => null,
		'logo'                 => null,
		'makesOffer'           => null,
		'member'               => null,
		'memberOf'             => null,
		'naics'                => null,
		'numberOfEmployees'    => null,
		'owns'                 => null,
		'parentOrganization'   => null,
		'review'               => null,
		'seeks'                => null,
		'sponsor'              => null,
		'subOrganization'      => null,
		'taxID'                => null,
		'telephone'            => null,
		'vatID'                => null,

		'additionalProperty'               => null,
		'amenityFeature'                   => null,
		'branchCode'                       => null,
		'containedInPlace'                 => null,
		'containsPlace'                    => null,
		'geo'                              => null,
		'hasMap'                           => null,
		'openingHoursSpecification'        => null,
		'photo'                            => null,
		'smokingAllowed'                   => null,
		'specialOpeningHoursSpecification' => null,
	];
}