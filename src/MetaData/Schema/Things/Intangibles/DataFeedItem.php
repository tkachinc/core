<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class DataFeedItem extends Intangible
{

	protected static $type = 'DataFeedItem';

	protected static $fields = [
		'dateCreated'  => null,
		'dateDeleted'  => null,
		'dateModified' => null,
		'item'         => null,
	];
}