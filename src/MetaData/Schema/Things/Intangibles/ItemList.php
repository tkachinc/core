<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class ItemList extends Intangible
{

	protected static $type = 'ItemList';

	protected static $fields = [
		'itemListElement' => null,
		'itemListOrder'   => null,
		'numberOfItems'   => null,
	];
}