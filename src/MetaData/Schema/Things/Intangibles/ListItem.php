<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class ListItem extends Intangible
{

	protected static $type = 'ListItem';

	protected static $fields = [
		'item'         => null,
		'nextItem'     => null,
		'position'     => null,
		'previousItem' => null,
	];
}