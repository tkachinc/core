<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Invoice extends Intangible
{

	protected static $type = 'Invoice';

	protected static $fields = [
		'accountId'            => null,
		'billingPeriod'        => null,
		'broker'               => null,
		'category'             => null,
		'confirmationNumber'   => null,
		'customer'             => null,
		'minimumPaymentDue'    => null,
		'paymentDueDate'       => null,
		'paymentMethod'        => null,
		'paymentMethodId'      => null,
		'paymentStatus'        => null,
		'provider'             => null,
		'referencesOrder'      => null,
		'scheduledPaymentDate' => null,
		'totalPaymentDue'      => null,
	];
}