<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class OrderItem extends Intangible
{

	protected static $type = 'OrderItem';

	protected static $fields = [
		'orderDelivery'   => null,
		'orderItemNumber' => null,
		'orderItemStatus' => null,
		'orderQuantity'   => null,
		'orderedItem'     => null,
		'seller'          => null,
	];
}