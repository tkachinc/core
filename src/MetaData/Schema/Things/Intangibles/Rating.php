<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Rating extends Intangible
{

	protected static $type = 'Rating';

	protected static $fields = [
		'author'      => null,
		'bestRating'  => null,
		'ratingValue' => null,
		'worstRating' => null,
	];
}