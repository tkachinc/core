<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class ServiceChannel extends Intangible
{

	protected static $type = 'ServiceChannel';

	protected static $fields = [
		'availableLanguage'    => null,
		'processingTime'       => null,
		'providesService'      => null,
		'serviceLocation'      => null,
		'servicePhone'         => null,
		'servicePostalAddress' => null,
		'serviceSmsNumber'     => null,
		'serviceUrl'           => null,
	];
}