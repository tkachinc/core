<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Brand extends Intangible
{

	protected static $type = 'Brand';

	protected static $fields = [
		'aggregateRating' => null,
		'logo'            => null,
		'review'          => null,
	];
}