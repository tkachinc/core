<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Role extends Intangible
{

	protected static $type = 'Role';

	protected static $fields = [
		'endDate'   => null,
		'roleName'  => null,
		'startDate' => null,
	];
}