<?php
namespace TkachInc\Core\MetaData\Schema\Services\FinancialProducts;

/**
 * @author maxim
 */
class LoanOrCredit
{

	protected static $type = 'LoanOrCredit';

	protected static $fields = [
		'amount'             => null,
		'loanTerm'           => null,
		'requiredCollateral' => null,
	];
}