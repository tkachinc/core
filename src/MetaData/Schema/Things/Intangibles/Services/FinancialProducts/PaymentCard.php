<?php
namespace TkachInc\Core\MetaData\Schema\Services\FinancialProducts;

use TkachInc\Core\MetaData\Schema\Services\FinancialProduct;

/**
 * @author maxim
 */
class PaymentCard extends FinancialProduct
{

	protected static $type = 'PaymentCard';
}