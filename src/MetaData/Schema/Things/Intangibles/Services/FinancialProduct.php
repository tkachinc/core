<?php
namespace TkachInc\Core\MetaData\Schema\Services;

use TkachInc\Core\MetaData\Schema\Things\Intangibles\Service;

/**
 * @author maxim
 */
class FinancialProduct extends Service
{

	protected static $type = 'FinancialProduct';

	protected static $fields = [
		'annualPercentageRate'            => null,
		'feesAndCommissionsSpecification' => null,
		'interestRate'                    => null,
	];
}