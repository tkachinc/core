<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Reservation extends Intangible
{

	protected static $type = 'Reservation';

	protected static $fields = [
		'bookingTime'           => null,
		'broker'                => null,
		'modifiedTime'          => null,
		'priceCurrency'         => null,
		'programMembershipUsed' => null,
		'provider'              => null,
		'reservationFor'        => null,
		'reservationId'         => null,
		'reservationStatus'     => null,
		'reservedTicket'        => null,
		'totalPrice'            => null,
		'underName'             => null,
	];
}