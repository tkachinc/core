<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Service extends Intangible
{

	protected static $type = 'Service';

	protected static $fields = [
		'aggregateRating'  => null,
		'areaServed'       => null,
		'audience'         => null,
		'availableChannel' => null,
		'award'            => null,
		'brand'            => null,
		'category'         => null,
		'hasOfferCatalog'  => null,
		'hoursAvailable'   => null,
		'isRelatedTo'      => null,
		'isSimilarTo'      => null,
		'logo'             => null,
		'offers'           => null,
		'provider'         => null,
		'providerMobility' => null,
		'review'           => null,
		'serviceOutput'    => null,
		'serviceType'      => null,
	];
}