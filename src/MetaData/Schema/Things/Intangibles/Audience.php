<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Audience extends Intangible
{

	protected static $type = 'Audience';

	protected static $fields = [
		'audienceType'   => null,
		'geographicArea' => null,
	];
}