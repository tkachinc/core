<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class ComputerLanguage extends Intangible
{

	protected static $type = 'ComputerLanguage';
}