<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles\Ratings;

use TkachInc\Core\MetaData\Schema\Things\Intangibles\Rating;

/**
 * @author maxim
 */
class AggregateRating extends Rating
{

	protected static $type = 'AggregateRating';

	protected static $fields = [
		'itemReviewed' => null,
		'ratingCount'  => null,
		'reviewCount'  => null,
	];
}