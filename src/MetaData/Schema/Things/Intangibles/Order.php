<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Order extends Intangible
{

	protected static $type = 'Order';

	protected static $fields = [
		'acceptedOffer'      => null,
		'billingAddress'     => null,
		'broker'             => null,
		'confirmationNumber' => null,
		'customer'           => null,
		'discount'           => null,
		'discountCode'       => null,
		'discountCurrency'   => null,
		'isGift'             => null,
		'orderDate'          => null,
		'orderDelivery'      => null,
		'orderNumber'        => null,
		'orderStatus'        => null,
		'orderedItem'        => null,
		'partOfInvoice'      => null,
		'paymentDueDate'     => null,
		'paymentMethod'      => null,
		'paymentMethodId'    => null,
		'paymentUrl'         => null,
		'seller'             => null,
	];
}