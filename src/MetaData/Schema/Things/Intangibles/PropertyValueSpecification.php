<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class PropertyValueSpecification extends Intangible
{

	protected static $type = 'PropertyValueSpecification';

	protected static $fields = [
		'defaultValue'   => null,
		'maxValue'       => null,
		'minValue'       => null,
		'multipleValues' => null,
		'readonlyValue'  => null,
		'stepValue'      => null,
		'valueMaxLength' => null,
		'valueMinLength' => null,
		'valueName'      => null,
		'valuePattern'   => null,
		'valueRequired'  => null,
	];
}