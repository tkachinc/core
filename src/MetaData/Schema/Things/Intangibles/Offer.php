<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Offer extends Intangible
{

	protected static $type = 'Offer';

	protected static $fields = [
		'acceptedPaymentMethod'     => null,
		'addOn'                     => null,
		'advanceBookingRequirement' => null,
		'aggregateRating'           => null,
		'areaServed'                => null,
		'availability'              => null,
		'availabilityEnds'          => null,
		'availabilityStarts'        => null,
		'availableAtOrFrom'         => null,
		'availableDeliveryMethod'   => null,
		'businessFunction'          => null,
		'category'                  => null,
		'deliveryLeadTime'          => null,
		'eligibleCustomerType'      => null,
		'eligibleDuration'          => null,
		'eligibleRegion'            => null,
		'eligibleTransactionVolume' => null,
		'gtin12'                    => null,
		'gtin13'                    => null,
		'gtin14'                    => null,
		'gtin8'                     => null,
		'includesObject'            => null,
		'inventoryLevel'            => null,
		'itemCondition'             => null,
		'itemOffered'               => null,
		'mpn'                       => null,
		'offeredBy'                 => null,
		'price'                     => null,
		'priceCurrency'             => null,
		'priceSpecification'        => null,
		'priceValidUntil'           => null,
		'review'                    => null,
		'seller'                    => null,
		'serialNumber'              => null,
		'sku'                       => null,
		'validFrom'                 => null,
		'validThrough'              => null,
		'warranty'                  => null,
	];
}