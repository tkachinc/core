<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles\Quantities;

use TkachInc\Core\MetaData\Schema\Things\Intangibles\Quantity;

/**
 * @author maxim
 */
class Distance extends Quantity
{

	protected static $type = 'Distance';

	protected static $fields = [
		'additionalType'            => null,
		'alternateName'             => null,
		'description'               => null,
		'disambiguatingDescription' => null,
		'image'                     => null,
		'mainEntityOfPage'          => null,
		'name'                      => null,
		'potentialAction'           => null,
		'sameAs'                    => null,
		'url'                       => null,
	];
}