<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles\Quantities;

use TkachInc\Core\MetaData\Schema\Things\Intangibles\Quantity;

/**
 * @author maxim
 */
class Duration extends Quantity
{

	protected static $type = 'Duration';
}