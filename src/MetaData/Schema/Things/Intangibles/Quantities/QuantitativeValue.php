<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles\Quantities;

use TkachInc\Core\MetaData\Schema\Things\Intangibles\Quantity;

/**
 * @author maxim
 */
class QuantitativeValue extends Quantity
{

	protected static $type = 'QuantitativeValue';

	protected static $fields = [
		'additionalProperty' => null,
		'maxValue'           => null,
		'minValue'           => null,
		'unitCode'           => null,
		'unitText'           => null,
		'value'              => null,
		'valueReference'     => null,
	];
}