<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class EntryPoint extends Intangible
{

	protected static $type = 'EntryPoint';

	protected static $fields = [
		'actionApplication' => null,
		'actionPlatform'    => null,
		'contentType'       => null,
		'encodingType'      => null,
		'httpMethod'        => null,
		'urlTemplate'       => null,
	];
}