<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Seat extends Intangible
{

	protected static $type = 'Seat';

	protected static $fields = [
		'seatNumber'  => null,
		'seatRow'     => null,
		'seatSection' => null,
		'seatingType' => null,
	];
}