<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles;

use TkachInc\Core\MetaData\Schema\Things\Intangible;

/**
 * @author maxim
 */
class Demand extends Intangible
{

	protected static $type = 'Demand';

	protected static $fields = [
		'acceptedPaymentMethod'     => null,
		'advanceBookingRequirement' => null,
		'areaServed'                => null,
		'availability'              => null,
		'availabilityEnds'          => null,
		'availabilityStarts'        => null,
		'availableAtOrFrom'         => null,
		'availableDeliveryMethod'   => null,
		'businessFunction'          => null,
		'deliveryLeadTime'          => null,
		'eligibleCustomerType'      => null,
		'eligibleDuration'          => null,
		'eligibleQuantity'          => null,
		'eligibleRegion'            => null,
		'eligibleTransactionVolume' => null,
		'gtin12'                    => null,
		'gtin13'                    => null,
		'gtin14'                    => null,
		'gtin8'                     => null,
		'includesObject'            => null,
		'ineligibleRegion'          => null,
		'inventoryLevel'            => null,
		'itemCondition'             => null,
		'itemOffered'               => null,
		'mpn'                       => null,
		'priceSpecification'        => null,
		'seller'                    => null,
		'serialNumber'              => null,
		'sku'                       => null,
		'validFrom'                 => null,
		'validThrough'              => null,
		'warranty'                  => null,
	];
}