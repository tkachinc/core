<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles\Offers;

use TkachInc\Core\MetaData\Schema\Things\Intangibles\Offer;

/**
 * @author maxim
 */
class AggregateOffer extends Offer
{

	protected static $type = 'AggregateOffer';

	protected static $fields = [
		'highPrice'     => null,
		'lowPrice'      => null,
		'offerCount'    => null,
		'offers'        => null,
		'priceCurrency' => null,
	];
}