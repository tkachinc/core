<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles\ItemLists;

use TkachInc\Core\MetaData\Schema\Things\Intangibles\ItemList;

/**
 * @author maxim
 */
class BreadcrumbList extends ItemList
{

	protected static $type = 'BreadcrumbList';
}