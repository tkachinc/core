<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles\Reservations;

use TkachInc\Core\MetaData\Schema\Things\Intangibles\Reservation;

/**
 * @author maxim
 */
class EventReservation extends Reservation
{

	protected static $type = 'EventReservation';
}