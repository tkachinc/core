<?php
namespace TkachInc\Core\MetaData\Schema\Things\Intangibles\Reservations;

use TkachInc\Core\MetaData\Schema\Things\Intangibles\Reservation;

/**
 * @author maxim
 */
class ReservationPackage extends Reservation
{

	protected static $type = 'ReservationPackage';

	protected static $fields = [
		'subReservation' => null,
	];
}