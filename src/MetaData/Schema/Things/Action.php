<?php
namespace TkachInc\Core\MetaData\Schema\Things;

use TkachInc\Core\MetaData\Schema\Thing;

/**
 * @author maxim
 */
class Action extends Thing
{

	protected static $type = 'Action';

	protected static $fields = [
		'actionStatus' => null,
		'agent'        => null,
		'endTime'      => null,
		'error'        => null,
		'instrument'   => null,
		'location'     => null,
		'object'       => null,
		'participant'  => null,
		'result'       => null,
		'startTime'    => null,
		'target'       => null,
	];
}