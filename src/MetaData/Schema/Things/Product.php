<?php
namespace TkachInc\Core\MetaData\Schema\Things;

use TkachInc\Core\MetaData\Schema\Thing;

/**
 * @author maxim
 */
class Product extends Thing
{

	protected static $type = 'Product';

	protected static $fields = [
		'additionalProperty'        => null,
		'aggregateRating'           => null,
		'audience'                  => null,
		'award'                     => null,
		'brand'                     => null,
		'category'                  => null,
		'color'                     => null,
		'depth'                     => null,
		'gtin12'                    => null,
		'gtin13'                    => null,
		'gtin14'                    => null,
		'gtin8'                     => null,
		'height'                    => null,
		'isAccessoryOrSparePartFor' => null,
		'isConsumableFor'           => null,
		'isRelatedTo'               => null,
		'isSimilarTo'               => null,
		'itemCondition'             => null,
		'logo'                      => null,
		'manufacturer'              => null,
		'model'                     => null,
		'mpn'                       => null,
		'offers'                    => null,
		'productID'                 => null,
		'productionDate'            => null,
		'purchaseDate'              => null,
		'releaseDate'               => null,
		'review'                    => null,
		'sku'                       => null,
		'weight'                    => null,
		'width'                     => null,
	];
}