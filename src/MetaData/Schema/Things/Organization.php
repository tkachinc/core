<?php
namespace TkachInc\Core\MetaData\Schema\Things;

use TkachInc\Core\MetaData\Schema\Thing;

/**
 * @author maxim
 */
class Organization extends Thing
{

	protected static $type = 'Organization';

	protected static $fields = [
		'address'              => null,
		'aggregateRating'      => null,
		'alumni'               => null,
		'areaServed'           => null,
		'award'                => null,
		'brand'                => null,
		'contactPoint'         => null,
		'department'           => null,
		'dissolutionDate'      => null,
		'duns'                 => null,
		'email'                => null,
		'employee'             => null,
		'event'                => null,
		'faxNumber'            => null,
		'founder'              => null,
		'foundingDate'         => null,
		'foundingLocation'     => null,
		'funder'               => null,
		'globalLocationNumber' => null,
		'hasOfferCatalog'      => null,
		'hasPOS'               => null,
		'isicV4'               => null,
		'legalName'            => null,
		'leiCode'              => null,
		'location'             => null,
		'logo'                 => null,
		'makesOffer'           => null,
		'member'               => null,
		'memberOf'             => null,
		'naics'                => null,
		'numberOfEmployees'    => null,
		'owns'                 => null,
		'parentOrganization'   => null,
		'review'               => null,
		'seeks'                => null,
		'sponsor'              => null,
		'subOrganization'      => null,
		'taxID'                => null,
		'telephone'            => null,
		'vatID'                => null,
	];
}