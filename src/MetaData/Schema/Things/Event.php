<?php
namespace TkachInc\Core\MetaData\Schema\Things;

use TkachInc\Core\MetaData\Schema\Thing;

/**
 * @author maxim
 */
class Event extends Thing
{

	protected static $type = 'Event';

	protected static $fields = [
		'actor'               => null,
		'aggregateRating'     => null,
		'attendee'            => null,
		'composer'            => null,
		'contributor'         => null,
		'director'            => null,
		'doorTime'            => null,
		'duration'            => null,
		'endDate'             => null,
		'eventStatus'         => null,
		'funder'              => null,
		'isAccessibleForFree' => null,
		'location'            => null,
		'offers'              => null,
		'organizer'           => null,
		'performer'           => null,
		'previousStartDate'   => null,
		'recordedIn'          => null,
		'review'              => null,
		'sponsor'             => null,
		'startDate'           => null,
		'subEvent'            => null,
		'superEvent'          => null,
		'translator'          => null,
		'typicalAgeRange'     => null,
		'workFeatured'        => null,
		'workPerformed'       => null,
	];
}