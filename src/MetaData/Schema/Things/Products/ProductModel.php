<?php
namespace TkachInc\Core\MetaData\Schema\Things\Products;

use TkachInc\Core\MetaData\Schema\Things\Product;

/**
 * @author maxim
 */
class ProductModel extends Product
{

	protected static $type = 'ProductModel';

	protected static $fields = [
		'isVariantOf'   => null,
		'predecessorOf' => null,
		'successorOf'   => null,
	];
}