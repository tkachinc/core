<?php
namespace TkachInc\Core\MetaData\Schema\Things\Products;

use TkachInc\Core\MetaData\Schema\Things\Product;

/**
 * @author maxim
 */
class SomeProducts extends Product
{

	protected static $type = 'SomeProducts';

	protected static $fields = [
		'inventoryLevel' => null,
	];
}