<?php
namespace TkachInc\Core\MetaData\Schema\Things\Places\AdministrativeAreas;

use TkachInc\Core\MetaData\Schema\Things\Places\AdministrativeArea;

/**
 * @author maxim
 */
class Country extends AdministrativeArea
{

	protected static $type = 'Country';
}