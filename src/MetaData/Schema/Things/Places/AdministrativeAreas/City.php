<?php
namespace TkachInc\Core\MetaData\Schema\Things\Places\AdministrativeAreas;

use TkachInc\Core\MetaData\Schema\Things\Places\AdministrativeArea;

/**
 * @author maxim
 */
class City extends AdministrativeArea
{

	protected static $type = 'City';
}