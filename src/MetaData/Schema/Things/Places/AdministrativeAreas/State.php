<?php
namespace TkachInc\Core\MetaData\Schema\Things\Places\AdministrativeAreas;

use TkachInc\Core\MetaData\Schema\Things\Places\AdministrativeArea;

/**
 * @author maxim
 */
class State extends AdministrativeArea
{

	protected static $type = 'State';
}