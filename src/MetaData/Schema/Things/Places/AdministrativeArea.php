<?php
namespace TkachInc\Core\MetaData\Schema\Things\Places;

use TkachInc\Core\MetaData\Schema\Things\Place;

/**
 * @author maxim
 */
class AdministrativeArea extends Place
{

	protected static $type = 'AdministrativeArea';
}