<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues;

use TkachInc\Core\MetaData\Schema\Things\StructuredValue;

/**
 * @author maxim
 */
class ContactPoint extends StructuredValue
{

	protected static $type = 'ContactPoint';

	protected static $fields = [
		'areaServed'        => null,
		'availableLanguage' => null,
		'contactOption'     => null,
		'contactType'       => null,
		'email'             => null,
		'faxNumber'         => null,
		'hoursAvailable'    => null,
		'productSupported'  => null,
		'telephone'         => null,
	];
}