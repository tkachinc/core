<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues\PricesSpecifications;

/**
 * @author maxim
 */
class PaymentChargeSpecification
{

	protected static $type = 'PaymentChargeSpecification';

	protected static $fields = [
		'appliesToDeliveryMethod' => null,
		'appliesToPaymentMethod'  => null,
	];
}