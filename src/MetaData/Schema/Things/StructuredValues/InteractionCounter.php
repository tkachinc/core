<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues;

use TkachInc\Core\MetaData\Schema\Things\StructuredValue;

/**
 * @author maxim
 */
class InteractionCounter extends StructuredValue
{

	protected static $type = 'InteractionCounter';

	protected static $fields = [
		'interactionService'   => null,
		'interactionType'      => null,
		'userInteractionCount' => null,
	];
}