<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues;

use TkachInc\Core\MetaData\Schema\Things\StructuredValue;

/**
 * @author maxim
 */
class GeoCoordinates extends StructuredValue
{

	protected static $type = 'GeoCoordinates';

	protected static $fields = [
		'address'        => null,
		'addressCountry' => null,
		'elevation'      => null,
		'latitude'       => null,
		'longitude'      => null,
		'postalCode'     => null,
	];
}