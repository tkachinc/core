<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues;

use TkachInc\Core\MetaData\Schema\Things\StructuredValue;

/**
 * @author maxim
 */
class QuantitativeValue extends StructuredValue
{

	protected static $type = 'QuantitativeValue';

	protected static $fields = [
		'additionalProperty' => null,
		'maxValue'           => null,
		'minValue'           => null,
		'unitCode'           => null,
		'unitText'           => null,
		'value'              => null,
		'valueReference'     => null,
	];
}