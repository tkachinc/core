<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues;

use TkachInc\Core\MetaData\Schema\Things\StructuredValue;

/**
 * @author maxim
 */
class GeoShape extends StructuredValue
{

	protected static $type = 'GeoShape';

	protected static $fields = [
		'address'        => null,
		'addressCountry' => null,
		'box'            => null,
		'circle'         => null,
		'elevation'      => null,
		'line'           => null,
		'polygon'        => null,
		'postalCode'     => null,
	];
}