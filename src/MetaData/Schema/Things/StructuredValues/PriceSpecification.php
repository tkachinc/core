<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues;

use TkachInc\Core\MetaData\Schema\Things\StructuredValue;

/**
 * @author maxim
 */
class PriceSpecification extends StructuredValue
{

	protected static $type = 'PriceSpecification';

	protected static $fields = [
		'eligibleQuantity'          => null,
		'eligibleTransactionVolume' => null,
		'maxPrice'                  => null,
		'minPrice'                  => null,
		'price'                     => null,
		'priceCurrency'             => null,
		'validFrom'                 => null,
		'validThrough'              => null,
		'valueAddedTaxIncluded'     => null,
	];
}