<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues;

use TkachInc\Core\MetaData\Schema\Things\StructuredValue;

/**
 * @author maxim
 */
class PropertyValue extends StructuredValue
{

	protected static $type = 'PropertyValue';

	protected static $fields = [
		'maxValue'                  => null,
		'minValue'                  => null,
		'propertyID'                => null,
		'unitCode'                  => null,
		'unitText'                  => null,
		'value'                     => null,
		'valueReference'            => null,
		'depth'                     => null,
		'gtin12'                    => null,
		'gtin13'                    => null,
		'gtin14'                    => null,
		'gtin8'                     => null,
		'height'                    => null,
		'isAccessoryOrSparePartFor' => null,
	];
}