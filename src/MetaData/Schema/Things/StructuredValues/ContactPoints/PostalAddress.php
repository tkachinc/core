<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues\ContactPoints;

use TkachInc\Core\MetaData\Schema\Things\StructuredValues\ContactPoint;

/**
 * @author maxim
 */
class PostalAddress extends ContactPoint
{

	protected static $type = 'PostalAddress';

	protected static $fields = [
		'addressCountry'      => null,
		'addressLocality'     => null,
		'addressRegion'       => null,
		'postOfficeBoxNumber' => null,
		'postalCode'          => null,
		'streetAddress'       => null,
	];
}