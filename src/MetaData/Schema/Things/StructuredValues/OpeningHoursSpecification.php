<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues;

use TkachInc\Core\MetaData\Schema\Things\StructuredValue;

/**
 * @author maxim
 */
class OpeningHoursSpecification extends StructuredValue
{

	protected static $type = 'OpeningHoursSpecification';

	protected static $fields = [
		'closes'       => null,
		'dayOfWeek'    => null,
		'opens'        => null,
		'validFrom'    => null,
		'validThrough' => null,
	];
}