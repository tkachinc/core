<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues;

use TkachInc\Core\MetaData\Schema\Things\StructuredValue;

/**
 * @author maxim
 */
class DatedMoneySpecification extends StructuredValue
{

	protected static $type = 'DatedMoneySpecification';

	protected static $fields = [
		'amount'    => null,
		'currency'  => null,
		'endDate'   => null,
		'startDate' => null,
	];
}