<?php
namespace TkachInc\Core\MetaData\Schema\Things\StructuredValues;

use TkachInc\Core\MetaData\Schema\Things\StructuredValue;

/**
 * @author maxim
 */
class MonetaryAmount extends StructuredValue
{

	protected static $type = 'MonetaryAmount';

	protected static $fields = [
		'currency'     => null,
		'maxValue'     => null,
		'minValue'     => null,
		'validFrom'    => null,
		'validThrough' => null,
		'value'        => null,
	];
}