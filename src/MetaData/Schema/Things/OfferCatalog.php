<?php
namespace TkachInc\Core\MetaData\Schema\Things;
use TkachInc\Core\MetaData\Schema\Thing;

/**
 * Created by PhpStorm.
 * User: Maksym Tkach
 * Date: 5/3/17
 * Time: 11:47
 */
class OfferCatalog extends Thing
{
	protected static $type = 'OfferCatalog';

	protected static $fields = [
		'itemListElement' => null,
		'itemListOrder'   => null,
		'numberOfItems'   => null,
	];
}