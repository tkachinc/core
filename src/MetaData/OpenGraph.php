<?php
namespace TkachInc\Core\MetaData;

/**
 * @author maxim
 */
class OpenGraph
{

	/**
	 * @var MetaData
	 */
	protected $metaData;

	/**
	 * OpenGraph constructor.
	 *
	 * @param MetaData $metaData
	 */
	public function __construct(MetaData $metaData)
	{
		$this->metaData = $metaData;
	}

	/**
	 * @param string $title
	 *
	 * @return OpenGraph
	 */
	public function addTitle(string $title):OpenGraph
	{
		if (!empty($title)) {
			$this->metaData->addMetaCustom('property="og:title" content="' . $title . '"');
		}

		return $this;
	}

	/**
	 * @param string $url
	 *
	 * @return OpenGraph
	 */
	public function addUrl(string $url):OpenGraph
	{
		if (!empty($url)) {
			$this->metaData->addMetaCustom('property="og:url" content="' . $url . '"');
		}

		return $this;
	}

	/**
	 * @param string $url
	 *
	 * @return OpenGraph
	 */
	public function addImage(string $url):OpenGraph
	{
		if (!empty($url)) {
			$this->metaData->addMetaCustom('property="og:image" content="' . $url . '"');
		}

		return $this;
	}

	/**
	 * @return OpenGraph
	 */
	public function addSiteName():OpenGraph
	{
		$siteName = $this->metaData->getSiteName();
		if (!empty($siteName)) {
			$this->metaData->addMetaCustom('property="og:site_name" content="' . $siteName . '"');
		}

		return $this;
	}

	/**
	 * @param string $description
	 *
	 * @return OpenGraph
	 */
	public function addDescription(string $description):OpenGraph
	{
		if (!empty($description)) {
			$this->metaData->addMetaCustom('property="og:description" content="' . $description . '"');
		}

		return $this;
	}

	/**
	 * @param string $property
	 * @param string $content
	 *
	 * @return OpenGraph
	 */
	public function addCustom(string $property, string $content):OpenGraph
	{
		if (!empty($property) && !empty($content)) {
			$this->metaData->addMetaCustom('property="' . $property . '" content="' . $content . '"');
		}

		return $this;
	}

	/**
	 * @return MetaData
	 */
	public function getMetaData(): MetaData
	{
		return $this->metaData;
	}
}