<?php
namespace TkachInc\Core\Semaphore;

/**
 * @author maxim
 */
class Shm
{

	protected $key;
	protected $perm;
	protected $memsize;
	protected $shmId;

	public static function ftok($pathname, $proj)
	{
		return ftok($pathname, $proj);
	}

	public function __construct($key, $memsize = null, $perm = 0666)
	{
		$this->key = $key;
		$this->memsize = $memsize;
		$this->perm = $perm;
		$this->shmId = shm_attach($this->key, $this->memsize, $this->perm);
	}

	public function remove()
	{
		shm_remove($this->shmId);
	}

	/**
	 * @param int $varKey
	 * @return bool
	 */
	public function getVar($varKey)
	{
		return shm_get_var($this->shmId, $varKey);
	}

	/**
	 * @param int $varKey
	 * @param $var
	 * @return bool
	 */
	public function putVar($varKey, $var)
	{
		return shm_put_var($this->shmId, $varKey, $var);
	}

	public function removeVar($varKey)
	{
		shm_remove_var($this->shmId, $varKey);
	}

	public function __destruct()
	{
		shm_detach($this->shmId);
	}
}