<?php
namespace TkachInc\Core\Semaphore;

/**
 * @author maxim
 */
class Msg
{
	protected $res;

	public function __construct($key, $perms = 0666)
	{
		$this->res = msg_get_queue($key, $perms = 0666);
	}

	public function queueExists($key)
	{
		$this->res = msg_queue_exists($key);
	}

	public function receive($desiredmsgtype, &$msgtype, $maxsize, &$message, $unserialize = true, $flags = 0, &$errorcode = null)
	{
		return msg_receive($this->res, $desiredmsgtype, $msgtype, $maxsize, $message, $unserialize, $flags, $errorcode);
	}

	public function removeQueue()
	{
		return msg_remove_queue($this->res);
	}

	public function send($msgtype, $message, $serialize = true, $blocking = true, &$errorcode = null)
	{
		return msg_send($this->res, $msgtype, $message, $serialize, $blocking, $errorcode);
	}

	public function setQueue(array $data)
	{
		return msg_set_queue($this->res, $data);
	}

	public function statQueue()
	{
		return msg_stat_queue($this->res);
	}
}