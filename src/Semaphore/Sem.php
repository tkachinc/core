<?php
namespace TkachInc\Core\Semaphore;

/**
 * @author maxim
 */
class Sem
{
	protected $key;
	protected $res;

	public function __construct($key, $maxAcquire = 1, $perm = 0666, $autoRelease = 1)
	{
		$this->key = $key;
		$this->res = sem_get($this->key, $maxAcquire, $perm, $autoRelease);
	}

	public function acquire()
	{
		return sem_acquire($this->res);
	}

	public function release()
	{
		return sem_release($this->res);
	}

	public function __destruct()
	{
		sem_remove($this->res);
	}
}