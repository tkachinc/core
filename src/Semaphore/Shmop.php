<?php
namespace TkachInc\Core\Semaphore;

/**
 * @author maxim
 */
class Shmop
{
	protected $shmId;

	public function __construct($key, $flags, $mode, $size)
	{
		$this->shmId = shmop_open($key, $flags, $mode, $size);
	}

	public function delete()
	{
		return shmop_delete($this->shmId);
	}

	public function read($start, $count)
	{
		return shmop_read($this->shmId, $start, $count);
	}

	public function size()
	{
		return shmop_size($this->shmId);
	}

	public function write($data, $offset)
	{
		return shmop_write($this->shmId, $data, $offset);
	}

	public function __destruct()
	{
		shmop_close($this->shmId);
	}
}