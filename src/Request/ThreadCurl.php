<?php
namespace TkachInc\Core\Request;

use Thread;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ThreadCURL extends Thread
{
	public $url;
	public $data = '';
	protected $ch;
	protected $err = 0;
	protected $errmsg = '';

	/**
	 * @param            $url
	 * @param bool|true $return
	 * @param bool|false $requireSsl
	 * @param int $timeout
	 * @param array $setoptArray
	 * @param bool|false $sendPost
	 * @param array $paramsRequest
	 */
	public function __construct($url,
	                            $return = true,
	                            $requireSsl = false,
	                            $timeout = 3,
	                            Array $setoptArray = [],
	                            $sendPost = false,
	                            Array $paramsRequest = [])
	{
		$this->url = $url;
		$this->ch = curl_init($this->url);

		if (!empty($setoptArray)) {
			curl_setopt_array($this->ch, $setoptArray);
		}

		if ($return) {
			$this->setopt(CURLOPT_RETURNTRANSFER, 1);
			$this->setopt(CURLOPT_FOLLOWLOCATION, 1);
		}
		if ($requireSsl === false) {
			$this->setopt(CURLOPT_SSL_VERIFYHOST, 0);
			$this->setopt(CURLOPT_SSL_VERIFYPEER, 0);
		}
		$this->setopt(CURLOPT_TIMEOUT, $timeout);

		if (!empty($paramsRequest)) {
			$this->sendPost($sendPost, $paramsRequest);
		}
	}

	public function run()
	{
		if (is_resource($this->ch)) {
			/*
			* If a large amount of data is being requested, you might want to
			* fsockopen and read using usleep in between reads
			*/
			$this->data = curl_exec($this->ch);
		} else {
			printf("Thread #%lu was not provided a URL\n", $this->getThreadId());
		}
	}

	/**
	 * @param            $url
	 * @param bool|true $return
	 * @param bool|false $requireSsl
	 * @param int $timeout
	 * @param array $setoptArray
	 * @param bool|false $sendPost
	 * @param array $paramsRequest
	 * @return ThreadCURL
	 */
	public static function requestURL($url,
	                                  $return = true,
	                                  $requireSsl = false,
	                                  $timeout = 3,
	                                  Array $setoptArray = [],
	                                  $sendPost = false,
	                                  Array $paramsRequest = [])
	{
		$thread = new ThreadCURL($url, $return, $requireSsl, $timeout, $setoptArray, $sendPost, $paramsRequest);
		$thread->start();

		return $thread;
	}

	/**
	 * @param bool $post
	 * @param      $paramsRequest
	 * @return $this
	 */
	public function sendPost($post = false, $paramsRequest)
	{
		if ($post === false) {
			curl_setopt($this->ch, CURLOPT_URL, $this->url . '?' . http_build_query($paramsRequest));
			curl_setopt($this->ch, CURLOPT_HTTPGET, 1);
		} else {
			curl_setopt($this->ch, CURLOPT_URL, $this->url);
			curl_setopt($this->ch, CURLOPT_POST, 1);
			curl_setopt($this->ch, CURLOPT_POSTFIELDS, http_build_query($paramsRequest));
		}

		return $this;
	}

	/**
	 * @param      $flag
	 * @param null $param
	 * @return $this
	 */
	public function setopt($flag, $param = null)
	{
		curl_setopt($this->ch, $flag, $param);

		return $this;
	}

	/**
	 * @return $this
	 */
	public function close()
	{
		if (is_resource($this->ch)) {
			curl_close($this->ch);
		}

		return $this;
	}

	/**
	 * @param array $options
	 * @return $this
	 */
	public function setoptArray(Array $options)
	{
		curl_setopt_array($this->ch, $options);

		return $this;
	}

	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->data;
	}

	/**
	 * @return string
	 */
	public function getErr()
	{
		$this->err = curl_errno($this->ch);

		return $this->err;
	}

	/**
	 * @return string
	 */
	public function getErrmsg()
	{
		$this->errmsg = curl_error($this->ch);

		return $this->errmsg;
	}

	public function __destruct()
	{
		$this->close();
	}
}