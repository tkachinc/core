<?php
namespace TkachInc\Core\Log\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @package TkachInc\Core\Log\Model
 */
class LogModel extends ObjectModel
{
	protected static $_updateMethod = self::UPDATE_METHOD_SET;
	protected static $_isCacheOn = false;
	protected static $_writeConcernMode = 0;
	protected static $_readPreference = ObjectModel::RP_SECONDARY_PREFERRED;
	protected static $_collection = 'log_model';
	protected static $_pk = '_id';
	protected static $_sort = ['time' => -1];
	protected static $_capped = true;
	protected static $_cappedSize = 1000000000;
	protected static $_cappedMax = 1000000;
	protected static $_indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['point' => 1],
		],
		[
			'keys' => ['time' => 1],
		],
		[
			'keys' => ['tags' => 1],
		],
	];

	protected static $_fieldsDefault = [
		'_id'      => '',
		'level'    => 0,
		'time'     => 0,
		'point'    => '',
		'tags'     => [],
		'data'     => [],
		'context'  => [],
		'execute'  => 0,
		'memory'   => 0,
		'_REQUEST' => [],
		'_SERVER'  => [],
	];

	protected static $_fieldsValidate = [
		'_id'      => self::TYPE_MONGO_ID,
		'level'    => self::TYPE_UNSIGNED_INT,
		'time'     => self::TYPE_MONGO_DATE,
		'point'    => self::TYPE_STRING,
		'tags'     => self::TYPE_JSON,
		'data'     => self::TYPE_JSON,
		'context'  => self::TYPE_JSON,
		'execute'  => self::TYPE_UNSIGNED_INT,
		'memory'   => self::TYPE_UNSIGNED_INT,
		'_REQUEST' => self::TYPE_JSON,
		'_SERVER'  => self::TYPE_JSON,
	];
}