<?php
namespace TkachInc\Core\Log;

use TkachInc\Core\Database\MongoDB\ObjectModel;
use TkachInc\Core\Log\Model\LogModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class LogPoint
{
	protected $point;
	protected $data = [];
	protected $time;
	protected $execute;
	protected $memory;
	protected $saved;
	protected $tags;
	protected $startTime;
	protected $_REQUEST, $_SERVER;
	protected $model;
	protected $level = 0;
	protected $init = false;
	protected $context = [];

	/**
	 * @param      $point
	 * @param null $startTime
	 * @throws \Exception
	 */
	public function __construct($point, $startTime = null)
	{
		if (empty($point)) {
			throw new \Exception('Name is empty');
		}
		$this->point = $point;

		if (is_numeric($startTime)) {
			$this->startTime = $startTime;
		}
	}

	/**
	 * @param array $data
	 * @param array $tags
	 * @param array $context
	 */
	public function add(Array $data = [], Array $tags = [], Array $context = [])
	{
		if (empty($this->startTime)) {
			$this->startTime = microtime(true);
		}

		if ($this->init === false) {
			$this->context = $context;
			$this->init = true;
			$this->data = $data;
			$this->time = ObjectModel::getMongoDate();
			$this->_SERVER = $_SERVER;
			$this->_REQUEST = $_REQUEST;
			$this->tags = $tags;
		} else {
			if (!empty($data)) {
				$this->data = array_merge($this->data, $data);
			}
			if (!empty($tags)) {
				$this->tags = array_merge($this->tags, $tags);
			}
			if (!empty($context)) {
				$this->context = array_merge($this->context, $context);
			}
		}

		$this->execute += round(
			abs(microtime(true) - $this->startTime),
			5
		);
		$this->memory += (function_exists('memory_get_usage')) ? round(
				max(memory_get_usage(), memory_get_peak_usage()) / 1024 / 1024,
				3
			) * 1000 : 0;
		$this->level++;
		$this->saved = false;
	}

	public function save()
	{
		if ($this->saved === false) {
			if (!isset($this->model)) {
				$this->model = new LogModel();
				$this->model->level = $this->level;
				$this->model->time = $this->time;
				$this->model->point = $this->point;
				$this->model->tags = $this->tags;
				$this->model->context = $this->context;
				$this->model->data = $this->data;
				$this->model->execute = $this->execute;
				$this->model->memory = $this->memory;
				$this->model->_REQUEST = $this->_REQUEST;
				$this->model->_SERVER = $this->_SERVER;
			} else {
				$this->model->tags = $this->tags;
				$this->model->context = $this->context;
				$this->model->data = $this->data;
			}
			$this->saved = true;
			$this->model->save();
		}
	}

	public function __destruct()
	{
		$this->save();
	}
}