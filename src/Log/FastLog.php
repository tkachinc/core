<?php
namespace TkachInc\Core\Log;

use Psr\Log\AbstractLogger;
use TkachInc\Core\Config\Config;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class FastLog extends AbstractLogger
{
	static $time;

	public static function start()
	{
		static::$time = microtime(true);
	}

	/**
	 * @return mixed
	 */
	public static function getStart()
	{
		return static::$time;
	}

	/**
	 * @param       $name
	 * @param array $data
	 * @param array $tags
	 * @param array $context
	 */
	public static function add($name, Array $data, Array $tags = [], Array $context = [])
	{
		$ignored = Config::getInstance()->get(['ignoredLogPoint'], []);
		if (!isset($ignored[$name])) {
			$log = new LogPoint($name, static::getStart());
			$log->add($data, $tags, $context);
			$log->save();
		}
	}

	/**
	 * @param mixed $level
	 * @param string $message
	 * @param array $context
	 * @return null|void
	 */
	public function log($level, $message, array $context = [])
	{
		static::add($level, ['message' => $message], $context);
	}
}