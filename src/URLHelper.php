<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 1/22/17
 * Time: 23:32
 */

namespace TkachInc\Core;

use TkachInc\Engine\Router\Route;
use TkachInc\Engine\ServerContainerFacade;
use TkachInc\Engine\Services\Request\URL;

class URLHelper extends URL
{
	public function toWithPrefix($url, $secure = false)
	{
		return static::addPrefix($this->to($url, $secure));
	}

	public function toWithConcretePrefix($url, $prefix = '', $secure = false)
	{
		return static::setPrefix($this->to($url, $secure), $prefix);
	}

	public function addPrefix($url)
	{
		$prefix = $this->config->get(['lang', 'prefix']);
		$basePrefix = $this->config->get(['lang', 'base']);
		$setBasePrefix = $this->config->get(['lang', 'setBasePrefix'], false);

		$result = parse_url($url);
		if (isset($result['host'])) {
			$baseURL = rtrim($this->getBaseURL(), '/');
			$checkURL = rtrim($this->getScheme() . '://' . $result['host'], '/');
			if ($baseURL !== $checkURL) {
				return $url;
			}
		}

		if (!$prefix) {
			$prefix = $basePrefix;
		}

		if (isset($result['path'])) {
			if ($setBasePrefix || $basePrefix !== $prefix) {
				$result['path'] = '/' . $prefix . $result['path'];
			}
		}

		return $this->createURL($result);
	}

	public function addQuery($path, $prefix = '', $queryArgs = [])
	{
		$currentPrefix = $this->config->get(['lang', 'prefix']);
		$basePrefix = $this->config->get(['lang', 'base']);
		$setBasePrefix = $this->config->get(['lang', 'setBasePrefix'], false);
		//ob_end_clean();
		//var_dump($queryArgs);
		//ob_flush();
		$url = http_build_query(array_merge($_GET, $queryArgs));
		if (!empty($url)) {
			$url = '?' . $url;
		}
		$url = '/' . $path . $url;

		if (!empty($currentPrefix)) {
			$url = str_replace($currentPrefix . '/', '', $url);
		}

		if (empty($prefix)) {
			$prefix = $basePrefix;
		}

		if ($setBasePrefix || $basePrefix !== $prefix) {
			$url = '/' . $prefix . $url;
		}

		$baseURL = rtrim($this->getBaseURL(), '/');

		return $baseURL . $url;
	}

	public function setURLWithoutGET($path, $prefix = '', $remove = [])
	{
		$currentPrefix = $this->config->get(['lang', 'prefix']);
		$basePrefix = $this->config->get(['lang', 'base']);
		$setBasePrefix = $this->config->get(['lang', 'setBasePrefix'], false);
		//ob_end_clean();
		//var_dump($queryArgs);
		//ob_flush();
		$queryArgs = $_GET;
		foreach($remove as $item) {
			unset($queryArgs[$item]);
		}

		$url = http_build_query($queryArgs);
		if (!empty($url)) {
			$url = '?' . $url;
		}
		$url = '/' . $path . $url;

		if (!empty($currentPrefix)) {
			$url = str_replace($currentPrefix . '/', '', $url);
		}

		if (empty($prefix)) {
			$prefix = $basePrefix;
		}

		if ($setBasePrefix || $basePrefix !== $prefix) {
			$url = '/' . $prefix . $url;
		}

		$baseURL = rtrim($this->getBaseURL(), '/');

		return $baseURL . $url;
	}

	public function setPrefix($routeString, $prefix = '')
	{
		$currentPrefix = $this->config->get(['lang', 'prefix']);
		$basePrefix = $this->config->get(['lang', 'base']);
		$setBasePrefix = $this->config->get(['lang', 'setBasePrefix'], false);

		$url = http_build_query($_GET);
		if (!empty($url)) {
			$url = '?' . $url;
		}

		if ($currentPrefix) {
			$url = '/' . str_replace($currentPrefix . '/', '', $routeString) . $url;
		} else {
			$url = '/' . $routeString . $url;
		}


		if (empty($prefix)) {
			$prefix = $basePrefix;
		}

		if ($setBasePrefix || $basePrefix !== $prefix) {
			$url = '/' . $prefix . $url;
		}
		$baseURL = rtrim($this->getBaseURL(), '/');

		return $baseURL . $url;
	}

	/**
	 * @param string $prefix
	 *
	 * @param bool   $setBasePrefix
	 *
	 * @return string
	 */
	public function setLangPrefix($prefix = '')
	{
		$basePrefix = $this->config->get(['lang', 'base']);
		$setBasePrefix = $this->config->get(['lang', 'setBasePrefix'], false);
		$url = http_build_query($_GET);
		if (!empty($url)) {
			$url = '?' . $url;
		}

		$container = ServerContainerFacade::getInstance();
		if ($container[Route::class]->getPrefix()) {
			$routeString = str_replace($container[Route::class]->getPrefix() . '/', '', $container[Route::class]->getRouteString());
		} else {
			$routeString = $container[Route::class]->getRouteString();
		}

		$url = '/' . $routeString . $url;


		if (empty($prefix)) {
			$prefix = $basePrefix;
		}

		if ($setBasePrefix || $basePrefix !== $prefix) {
			$url = '/' . $prefix . $url;
		}

		$baseUrl = rtrim(URLFacade::getBaseURL(), '/');

		return $baseUrl . $url;
	}
}