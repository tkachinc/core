<?php
namespace TkachInc\Core\Helpers;

use TkachInc\Engine\Services\Helpers\Base64URL;

class SerializationHelper
{
	public static function pack($data, $base64URL = false)
	{
		if (function_exists('msgpack_pack')) {
			$result = msgpack_pack($data);
		} else {
			$result = serialize($data);
		}

		if ($base64URL) {
			$result = Base64URL::encode($result);
		}

		return $result;
	}

	public static function unpack($data, $base64URL = false)
	{
		if (function_exists('msgpack_unpack')) {
			$result = msgpack_unpack($data);
		} else {
			$result = unserialize($data);
		}

		if ($base64URL) {
			$result = Base64URL::decode($result);
		}

		return $result;
	}
}