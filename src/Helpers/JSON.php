<?php
namespace TkachInc\Core\Helpers;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class JSON
{
	/**
	 * @param      $value
	 * @param bool $validate
	 * @throws JSONException
	 * @return string
	 */
	public static function encodeInObject($value, $validate = false)
	{
		$encode = json_encode($value, JSON_FORCE_OBJECT);
		if ($validate) {
			$error = json_last_error();
			if ($error !== JSON_ERROR_NONE) {
				throw new JSONException('Not validate. Error code: ' . $error);
			}
		}

		return $encode;
	}

	/**
	 * @param      $value
	 * @param int $options 0 JSON_FORCE_OBJECT
	 * @param bool $validate
	 * @throws JSONException
	 * @return string
	 */
	public static function encode($value, $options = 0, $validate = false)
	{
		$encode = json_encode($value, $options);
		if ($validate) {
			$error = json_last_error();
			if ($error !== JSON_ERROR_NONE) {
				throw new JSONException('Not validate. Error code: ' . $error);
			}
		}

		return $encode;
	}

	/**
	 * @param      $json
	 * @param bool $assoc
	 * @param int $depth
	 * @param int $options
	 * @param bool $validate
	 * @throws JSONException
	 * @return mixed
	 */
	public static function decode($json, $assoc = true, $depth = 512, $options = 0, $validate = false)
	{
		$decode = json_decode($json, $assoc, $depth, $options);
		if ($validate) {
			$error = json_last_error();
			if ($error !== JSON_ERROR_NONE) {
				throw new JSONException('Error code: ' . $error . ' Error message: ' . json_last_error_msg());
			}
		}

		return $decode;
	}
} 