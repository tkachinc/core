<?php
namespace TkachInc\Core\Helpers;

/**
 * Класс валидации данных
 */
class Validate
{
	/**
	 * Проверить Ajax запрос, или нет
	 *
	 * @return bool
	 */
	public static function isXMLHttpRequest()
	{
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) ? true : false;
	}

	/**
	 * Проверить на валидность URI
	 *
	 * @param string $uri Проверяемый URI
	 * @return bool
	 */
	public static function isUri($uri)
	{
		return (filter_var($uri, FILTER_VALIDATE_URL)) ? true : false;
	}

	/**
	 * Проверить на валидность email
	 *
	 * @param string $email e-mail адресс для проверки
	 * @return boolean
	 */
	public static function isEmail($email)
	{
		return (filter_var($email, FILTER_VALIDATE_EMAIL)) ? true : false;
	}

	/**
	 * Проверить действительно ли число с плавающей точкой, или 0
	 *
	 * @param float $float число для проверки
	 * @return boolean
	 */
	public static function isFloat($float)
	{
		return (strval((float)($float)) == strval($float) OR $float === 0);
	}

	/**
	 * Проверить действительно ли число с плавающей точкой беззнаковое (больше или равно 0)
	 *
	 * @param float $float число для проверки
	 * @return boolean
	 */
	public static function isUnsignedFloat($float)
	{
		return strval((float)($float)) == strval($float) AND $float >= 0;
	}

	/**
	 * Проверить логический ли тип данных
	 *
	 * @param boolean $bool тип для проверки
	 * @return boolean
	 */
	public static function isBool($bool)
	{
		return is_null($bool) OR is_bool($bool) OR preg_match('/^0|1|true|false$/', $bool);
	}

	/**
	 * Проверить действительно ли число целое, или 0
	 *
	 * @param integer $value число для проверки
	 * @return boolean
	 */
	public static function isInt($value)
	{
		return filter_var($value, FILTER_VALIDATE_INT) !== false;
	}

	/**
	 * Проверить действительно ли число целое и беззнаковое
	 *
	 * @param integer $value число для проверки
	 * @return boolean
	 */
	public static function isUnsignedInt($value)
	{
		return filter_var($value, FILTER_VALIDATE_INT) !== false && $value >= 0;
	}

	/**
	 * Проверить является ли число временной меткой
	 *
	 * @param integer $value число для проверки
	 * @return boolean
	 */
	public static function isTimeStamp($value)
	{
		return (strtotime($value) !== false);
	}

	/**
	 * Проверить является ли число временной меткой
	 *
	 * @param integer $value число для проверки
	 * @return boolean
	 */
	public static function isTime($value)
	{
		return (strtotime($value) !== false);
	}

	/**
	 * Проверить UID на валидность
	 *
	 * @param integer $value число для проверки
	 * @return boolean
	 */
	public static function isUID($value)
	{
		return (preg_match('#^-?[0-9a-zA-Z_]*$#', (string)$value));
	}

	/**
	 * Проверить являются ли данные строкой
	 *
	 * @param string $data данные для проверки
	 * @return boolean
	 */
	public static function isString($data)
	{
		return is_string($data);
	}

	/**
	 * Проверить могут ли данные быть id в БД Mongo
	 *
	 * @param string $data данные для проверки
	 * @return boolean
	 */
	public static function isMongoId($data)
	{
		return is_string($data);
	}

	/**
	 * Проверить могут ли данные быть id в БД Mongo
	 *
	 * @param string $data данные для проверки
	 * @return boolean
	 */
	public static function isMongoDate($data)
	{
		return (strtotime($data) !== false);
	}

	/**
	 * Проверить являются ли данные json строкой
	 *
	 * @param string $data данные для проверки
	 * @return boolean
	 */
	public static function isJSON($data)
	{
		//return is_string($data) && preg_match('/^(\{|\[).*(\]|\})$/', $value);
		if (is_string($data)) {
			json_decode($data);
			$error = json_last_error();

			return ($error === JSON_ERROR_NONE);
		} else {
			return false;
		}
	}

	/**
	 * Проверить являются ли данные массивом
	 *
	 * @param array $data данные для проверки
	 * @return boolean
	 */
	public static function isArray($data)
	{
		return is_array($data);
	}

	/**
	 * Проверка являются ли данные объектом БД и загружены ли с БД
	 *
	 * @param object $obj данные для проверки
	 * @return boolean
	 */
	public static function isLoadedObject($obj)
	{
		return (is_object($obj) &&
			method_exists($obj, 'getPK') &&
			isset($obj->{$obj->getPK()}) &&
			!empty($obj->{$obj->getPK()}) &&
			$obj->{$obj->getPK()});
	}
}