<?php
namespace TkachInc\Core\Helpers;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class StringData
{
	/**
	 * Example to use bitap (shift-or) algorithm.
	 * DONT USE THIS METHOD! THIS IS EXAMPLE BITAP ALGORITHM. USE ORIGINAL strpos FUNCTION.
	 * @param $text
	 * @param $pattern
	 * @return int
	 */
	public static function strpos($text, $pattern)
	{
		$m = strlen($pattern);
		$textLen = strlen($text);
		$patternMask = [];

		if (empty($pattern)) {
			return 0;
		}
		if ($m > 31) {
			return -1; //Error: The pattern is too long!
		}

		$R = ~1;

		for ($i = 0; $i <= 127; ++$i) {
			$patternMask[$i] = ~0;
		}

		for ($i = 0; $i < $m; ++$i) {
			$patternMask[ord($pattern[$i])] &= ~(1 << $i);
		}

		for ($i = 0; $i < $textLen; ++$i) {
			$R |= $patternMask[ord($text[$i])];
			$R <<= 1;

			if (0 == ($R & (1 << $m))) {
				return ($i - $m) + 1;
			}
		}

		return -1;
	}

	/**
	 * @param $text
	 * @param $length
	 * @param string $encoding
	 * @return string
	 */
	public static function mbSubstrByWord($text, $length, $encoding = 'UTF-8')
	{
		$length = mb_strripos(mb_substr($text, 0, $length, $encoding), ' ', null, $encoding);

		return mb_substr($text, 0, $length, $encoding);
	}

	/**
	 * @param $text
	 * @param $length
	 * @return string
	 */
	public static function substrByWord($text, $length)
	{
		$length = strripos(substr($text, 0, $length), ' ', null);

		return substr($text, 0, $length);
	}

	/**
	 * @param $str
	 * @param int $l
	 * @param string $encoding
	 * @return array
	 */
	public static function mbStrSplit($str, $l = 0, $encoding = 'UTF-8')
	{
		if ($l > 0) {
			$ret = [];
			$len = mb_strlen($str, $encoding);
			for ($i = 0; $i < $len; $i += $l) {
				$ret[] = mb_substr($str, $i, $l, $encoding);
			}

			return $ret;
		}

		return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
	}

	/**
	 * Подготовка латинского значение в валидное строковое представление.
	 *
	 * @example
	 * First Test`s _#@% ok! -> FirstTestsOk
	 * @param $name
	 * @param bool $ucfirst
	 * @return string
	 */
	public static function prepareUppercaseName($name, $ucfirst = true)
	{
		$name = preg_replace('|[^\w ]|', '', trim($name));
		$name = str_replace(['-', '_'], ' ', $name);
		$nameArray = explode(' ', $name);

		foreach ($nameArray as $key => &$value) {
			$value = trim($value);
			if (empty($value)) {
				unset($nameArray[$key]);
				continue;
			}

			$value = strtolower($value);
			if ($ucfirst) {
				$value = ucfirst($value);
			}
			$ucfirst = true;
		}

		return implode('', $nameArray);
	}

	/**
	 * Подготовка латинского значение в валидное строковое представление.
	 *
	 * @example
	 * First Test`s _#@% ok! -> First_tests_ok
	 * @param $name
	 * @param bool $ucfirst
	 * @return string
	 */
	public static function prepareLowercaseName($name, $ucfirst = true)
	{
		$name = preg_replace('|[^\w ]|', '', trim($name));
		$name = str_replace(['-', '_'], ' ', $name);
		$nameArray = explode(' ', $name);

		foreach ($nameArray as $key => &$value) {
			$value = trim($value);
			if (empty($value)) {
				unset($nameArray[$key]);
				continue;
			}

			$value = strtolower($value);
		}
		$string = implode('_', $nameArray);
		if ($ucfirst) {
			$string = ucfirst($string);
		}

		return $string;
	}
} 