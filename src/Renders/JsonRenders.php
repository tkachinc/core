<?php
namespace TkachInc\Core\Renders;

/**
 * @author maxim
 */
class JsonRenders implements InterfaceRenders
{
	public function render(Array $data)
	{
		return json_encode($data);
	}
}