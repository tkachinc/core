<?php
namespace TkachInc\Core\Renders;

/**
 * @author maxim
 */
class WithoutRenders implements InterfaceRenders
{
	public function render(Array $data)
	{
		return $data;
	}
}