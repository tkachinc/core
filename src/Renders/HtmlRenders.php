<?php
namespace TkachInc\Core\Renders;

use TkachInc\Core\CMS\Classes\PageVariable;
use TkachInc\Engine\Router\Route;

/**
 * @author maxim
 */
class HtmlRenders implements InterfaceRenders
{
	protected $template;
	protected $lang;
	protected $render;

	public function __construct(Route $route, $template, $lang)
	{
		$this->template = PageVariable::getTemplateDir('', $template, true);
		$this->lang = $lang;
		$this->render = Twig::getRender($route, $this->lang);
	}

	public function render(Array $data)
	{
		return $this->render->render($this->template, $data);
	}
}