<?php
namespace TkachInc\Core\Renders;

/**
 * @author maxim
 */
interface InterfaceRenders
{
	public function render(Array $data);
}