<?php
namespace TkachInc\Core\Renders;

use TkachInc\Core\CMS\Classes\MultiLanguageTwigExtension;
use TkachInc\Core\Config\Config;
use TkachInc\Core\Response\TwigRender;
use TkachInc\Core\Response\URLTwigExtension;
use TkachInc\Engine\Router\Route;

/**
 * @author maxim
 */
class Twig
{
	protected static $render = null;

	/**
	 * @param Route $route
	 * @param null $langName
	 * @return TwigRender
	 */
	public static function getRender(Route $route, $langName = null)
	{
		if (static::$render === null) {
			static::$render = new TwigRender();
			static::$render->addExtension(
				new URLTwigExtension(
					$route
				)
			);

			if ($langName) {
				static::$render->addExtension(new MultiLanguageTwigExtension(Config::getInstance()->get(['locale', $langName], null), $langName));
			}
		}

		return static::$render;
	}

	public static function setLang($langName)
	{
		if (!static::$render) {
			static::getRender(null);
		}
		static::$render->addExtension(new MultiLanguageTwigExtension(Config::getInstance()->get(['locale', $langName], null), $langName));
	}
}