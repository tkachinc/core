<?php
namespace TkachInc\Core\Config\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @property mixed value
 * @property null id
 * @property string type
 */
class BaseConfigModel extends ObjectModel
{
	protected static $_pk = '_id';
	protected static $_collection = 'base_cfg';
	protected static $_indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true,
		],
		['keys' => ['tags' => 1]],
	];

	protected static $_fieldsDefault = [
		'_id'     => '',
		'tags'    => [],
		'comment' => '',
		'value'   => [],
	];

	protected static $_fieldsValidate = [
		'_id'     => self::TYPE_STRING,
		'tags'    => self::TYPE_JSON,
		'comment' => self::TYPE_STRING,
		'value'   => self::TYPE_JSON,
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;
	protected static $_updateMethod = self::UPDATE_METHOD_SET;
}