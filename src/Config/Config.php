<?php
namespace TkachInc\Core\Config;

use TkachInc\Core\Config\Model\BaseConfigModel;
use TkachInc\Engine\Services\Config\BaseConfig;
use TkachInc\Engine\Services\Helpers\ArrayHelper;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Config extends BaseConfig
{
	/**
	 * @param $tag
	 *
	 * @return array
	 */
	protected function getWithTagByDB($tag)
	{
		$conf = BaseConfigModel::getAllAdvanced(['tags' => $tag]);
		$conf = ArrayHelper::arrayByOneKey(
			$conf,
			'_id',
			false,
			function ($item, $key) {
				return isset($item['value']) ? $item['value'] : $item;
			}
		);
		return $conf;
	}
}