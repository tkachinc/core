<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 1/31/17
 * Time: 16:16
 */

namespace TkachInc\Core;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use TkachInc\Core\Config\Config;
use TkachInc\Engine\Services\Config\BaseConfig;

class CoreServiceProvider implements ServiceProviderInterface
{
	public function register(Container $pimple)
	{
		$pimple[BaseConfig::class] = function () {
			return Config::getInstance();
		};
	}
}