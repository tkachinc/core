function FilterManager() {
    var _fields = {};
    var _filter = {};
    var _sort = [];
    var _perPages = [25];
    var _perPage = 25;
    var _sorts = {"asc": 1, "desc": -1};
    var _opers = {
        "$eq": "==",
        "$lt": "<",
        "$gt": ">",
        "$lte": "<=",
        "$gte": ">=",
        "$ne": "!=",
        "$in": "$in",
        "$nin": "$nin",
        "$regex": "$regex"
    };

    this.removeFilter = function (field) {
        if (_filter[field] !== undefined) {
            delete _filter[field];
        }
    };

    this.init = function () {
        this.initFields();

        var sortHtml = '';

        var first = true;
        for (var key in _sorts) {
            if (first) {
                sortHtml += '<option name="' + key + '" value="' + _sorts[key] + '" selected>' + key + '</option>';
            } else {
                sortHtml += '<option name="' + key + '" value="' + _sorts[key] + '">' + key + '</option>';
            }
            first = false;
        }
        $('.sorts').html(sortHtml);

        var operHtml = '';


        var first = true;
        for (var key in _opers) {
            if (first) {
                operHtml += '<option name="' + key + '" value="' + key + '" class="oper' + key + '" selected>' + _opers[key] + '</option>';
            }
            else {
                operHtml += '<option name="' + key + '" value="' + key + '" class="oper' + key + '">' + _opers[key] + '</option>';
            }
            first = false;
        }
        $('.opers').html(operHtml);

        var perPageHtml = '';


        for (var key in _perPages) {
            if (_perPage === _perPages[key]) {
                perPageHtml += '<option value="' + _perPages[key] + '" selected>' + _perPages[key] + '</option>';
            }
            else {
                perPageHtml += '<option value="' + _perPages[key] + '">' + _perPages[key] + '</option>';
            }
        }
        $('.perPages').html(perPageHtml);
    };

    this.initFields = function () {
        var fieldsHtml = '';

        var first = true;
        for (var key in _fields) {
            if (first) {
                fieldsHtml += '<option name="' + key + '" value="' + key + '" selected>' + key + '</option>';
            }
            else {
                fieldsHtml += '<option name="' + key + '" value="' + key + '">' + key + '</option>';
            }
            first = false;
        }

        $('.fields').html(fieldsHtml);
    };

    this.setCurrentFilter = function () {
        for (var field in _filter) {
            var request = _filter[field];
            if (typeof request === 'object') {
                for (var oper in request) {
                    var value = request[oper];
                }
            }
            else {
                var value = request;
                var oper = "$eq";
            }
            $('form select[name=filterField]').find('option[name="' + field + '"]').each(function () {
                $(this).attr('selected', 'selected');
            });
            $('form select[name=filterOper]').find('option[name="' + oper + '"]').each(function () {
                $(this).attr('selected', 'selected');
            });
            $('form input[name=filterValue]').val(value);
        }
    };

    this.setCurrentSort = function () {
        for (var field in _sort) {
            var value = _sort[field];
            $(".sort" + field).attr('selected', 'selected');
        }

        $('form select[name=sortField]').find('option[value="' + field + '"]').each(function () {
            $(this).attr('selected', 'selected');
        });
        $('form select[name=sortValue]').find('option[value="' + value + '"]').each(function () {
            $(this).attr('selected', 'selected');
        });
    };

    this.setFilter = function (filter) {
        if (_filter.length === 0) {
            _filter = {};
        }
        _filter = $this.recursiveSet(_filter, filter);
    };

    this.replaceFilter = function (filter) {
        _filter = {};
        _filter = $this.recursiveSet(_filter, filter);
    };

    this.recursiveSet = function (data, filter) {
        for (var key in filter) {
            if (filter[key] instanceof Object || filter[key] instanceof Array) {
                if (data[key] === undefined) {
                    data[key] = {};
                }
                data[key] = $this.recursiveSet(data[key], filter[key]);
            }
            else {
                data[key] = filter[key];
            }
        }
        return data;
    };

    this.setSort = function (sort) {
        _sort = sort;
    };
    this.sendFilter = function () {
        var url = window.location.protocol + '//' + window.location.host + window.location.pathname;
        url += '?perPage=' + _perPage + '&sort=' + JSON.stringify(_sort) + '&filter=' + JSON.stringify(_filter);
        window.location.assign(url);
    };
    this.clearFilter = function () {
        _filter = {};
        $('#currentFilter').text('');
    };

    this.setPerPage = function (perPage) {
        _perPage = perPage;
    };

    this.setPerPages = function (perPages) {
        _perPages = perPages;
    };

    this.getSort = function () {
        return _sort;
    };
    this.getFilter = function () {
        return _filter;
    };
    this.setFields = function (fields) {
        _fields = fields;
    };

    this.resetFilters = function (filters) {
        _filter = filters;
    };

    var $this = this;
}