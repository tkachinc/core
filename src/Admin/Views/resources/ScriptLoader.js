function ScriptLoader() {
    var $apis = {};
    var $loads = {};

    this.setApiMap = function (apis) {
        $apis = apis;
    };

    this.include = function (deferred, className, callable) {
        if (typeof className !== 'undefined' && typeof $apis[className] !== 'undefined') {
            if (typeof $loads[className] === 'undefined') {
                $.getScript($apis[className], function () {
                    $loads[className] = 1;
                    if (typeof callable === 'function') {
                        callable($loads[className]);
                    }
                    deferred.resolve(className);
                });
            }
            else {
                deferred.resolve(className);
            }
        }
        else {
            deferred.reject(className);
        }
    };
    this.cloads = function ($arguments, def) {
        var promise = def.promise();
        var counter = 0;
        for (var key in $arguments) {
            var deferred = new $.Deferred();
            var className = $arguments[key];
            if (Array.isArray(className)) {
                $main.include(deferred, className[0], className[1], className[2]);
            }
            else {
                $main.include(deferred, className);
            }
            deferred.done(function (name) {
                //console.log('Load: ' + name);
                counter++;
                if ($arguments.length === counter) {
                    //console.log('Global done');
                    def.resolve();
                }
            });
        }
        return promise;
    }
}
var $main = new ScriptLoader();
function _map(apis) {
    $main.setApiMap(apis);
}
function _cloads() {
    var $arguments = arguments;
    var def = new $.Deferred();
    return $main.cloads($arguments, def);
}