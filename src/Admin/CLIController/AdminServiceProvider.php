<?php
/**
 * Created by PhpStorm.
 * User: Maksym Tkach
 * Date: 8/7/17
 * Time: 14:16
 */

namespace TkachInc\Core\Admin\CLIController;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
use TkachInc\Core\Auth\BasicSecure;
use TkachInc\Core\Auth\Types\AbstractAuthType;
use TkachInc\Core\CMS\Controller\PageController;
use TkachInc\Core\Response\URLTwigExtension;
use TkachInc\Engine\Application\BaseHandler;
use TkachInc\Engine\Application\ServerEvents;
use TkachInc\Engine\EventProvider\EventProviderInterface;
use TkachInc\Engine\Router\Route;
use TkachInc\Engine\ServerContainerFacade;
use TkachInc\Engine\Services\Response\Response;
use TkachInc\Engine\Services\Response\ResponseTypes\HTMLResponse;

class AdminServiceProvider implements ServiceProviderInterface, EventProviderInterface
{
	public function register(Container $pimple)
	{
		$pimple[AbstractAuthType::class] = function () {
			return BasicSecure::getInstance();
		};
		$pimple[Response::class] = function () {
			return new HTMLResponse();
		};
		$pimple[BaseHandler::class] = function () use ($pimple) {
			return new AdminHandler($pimple);
		};

		$this->listen($pimple[ServerEvents::class]);
	}

	public function listen(ServerEvents $listen)
	{
		$listen->on(
			PageController::EVENT_CONTROLLER_TWIG_EXTENSION,
			function (&$twig) {
				$container = ServerContainerFacade::getInstance();
				$twig->addExtension(
					new URLTwigExtension(
						$container[Route::class]
					)
				);
			}
		);
	}
}