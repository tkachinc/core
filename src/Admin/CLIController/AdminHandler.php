<?php
/**
 * Created by PhpStorm.
 * User: Maksym Tkach
 * Date: 8/7/17
 * Time: 14:31
 */

namespace TkachInc\Core\Admin\CLIController;

use TkachInc\Core\Log\FastLog;
use TkachInc\Engine\Application\BaseHandler;
use TkachInc\Engine\Application\ServerEvents;
use TkachInc\Engine\Application\ServerPrepare;
use TkachInc\Engine\ServerContainerFacade;
use TkachInc\Engine\Services\Handler\HandlerRegistry;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\Engine\Services\Response\ResponseTypes\CLIResponse;
use TkachInc\Engine\Services\Response\ResponseTypes\SpecificJSONResponse;

class AdminHandler extends BaseHandler
{
	public function handle()
	{
		$container = ServerContainerFacade::getInstance();
		$handler = new HandlerRegistry($container[ServerEvents::class], false);
		$container[ServerPrepare::class]->displayError(true);

		if (Request::isCLI()) {
			$send = function ($response) {
				FastLog::add($response['type'], $response);
				(new CLIResponse())->put($response, 500);
			};
		} else {
			$send = function ($response) {
				FastLog::add($response['type'], $response);
				(new SpecificJSONResponse())->put($response, 500);
			};
		}

		$container[ServerEvents::class]->on(HandlerRegistry::TYPE_THROWABLE, $send);
		$container[ServerEvents::class]->on(HandlerRegistry::TYPE_ERROR, $send);
		//$this->server->on(HandlerEmitRegistry::TYPE_LAST_ERROR, $send);

		$handler->registerHandler();
	}
}