<?php
namespace TkachInc\Core\Admin\CLIController;

use TkachInc\Core\Config\Config;
use TkachInc\Engine\Application\CLIController;
use TkachInc\Engine\Application\DefaultApplication;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AdminCLIController extends CLIController
{
	public function __construct()
	{
		parent::__construct(new DefaultApplication());
	}

	/**
	 * Create index
	 */
	public function install()
	{
		$menu = (array)Config::getInstance()->loadFile('adminMenu')->get(['modelMap']);
		$this->processingMenu($menu);

		$this->application->setData(['msg' => 'Success!']);
		$this->application->response();
	}

	/**
	 * @param array $value
	 */
	private function processingMenu(Array $value)
	{
		foreach ($value as $v) {
			if (is_array($v)) {
				$this->processingMenu($v);
			} else {
				$modelClassName = '\\' . str_replace('_', '\\', $v);
				if (class_exists($modelClassName) && method_exists($modelClassName, 'createIndex')) {
					$modelClassName::checkCappedCollection();
					$modelClassName::createIndex();
				}
			}
		}
	}
}