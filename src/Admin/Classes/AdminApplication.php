<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 1/31/17
 * Time: 23:39
 */
namespace TkachInc\Core\Admin\Classes;

use TkachInc\Core\Auth\Types\AbstractAuthType;
use TkachInc\Core\CMS\Controller\PageController;
use TkachInc\Core\Config\Config;
use TkachInc\Core\Response\URLTwigExtension;
use TkachInc\Engine\Application\BaseApplication;
use TkachInc\Engine\Application\ServerEvents;
use TkachInc\Engine\Router\Route;
use TkachInc\Engine\ServerContainerFacade;

class AdminApplication extends BaseApplication
{
	public function listen(ServerEvents $listen)
	{
		parent::listen($listen);
		$listen->on(
			PageController::EVENT_CONTROLLER_TWIG_EXTENSION,
			function (&$twig) {
				$container = ServerContainerFacade::getInstance();
				$twig->addExtension(
					new URLTwigExtension(
						$container[Route::class]
					)
				);
			}
		);
	}

	public function recursiveCheckRuleInMenu(&$menu, AbstractAuthType $secure)
	{
		foreach ($menu as $key => $item) {
			if (is_array($item)) {
				$this->recursiveCheckRuleInMenu($menu[$key], $secure);
				if (empty($menu[$key])) {
					unset($menu[$key]);
				}
			} else {
				if (!$secure->canRead($item) && !$secure->canWrite($item)) {
					unset($menu[$key]);
				}
			}
		}
	}

	/**
	 * @return array|mixed
	 */
	public function getMenuUser(AbstractAuthType $secure)
	{
		$menu = Config::getInstance()->loadFile('adminMenu')->get(['modelMap'], []);
		if ($secure->getRole() === 'root') {
			return $menu;
		}
		$this->recursiveCheckRuleInMenu($menu, $secure);

		return $menu;
	}

	/**
	 * @return array
	 */
	public function processingMenu($value)
	{
		$models = [];
		if (is_array($value)) {
			foreach ($value as $name => $v) {
				if (is_array($v)) {
					$models = array_merge($models, $this->processingMenu($v));
				} else {
					$modelClassName = '\\' . str_replace('_', '\\', $v);
					if (class_exists($modelClassName)) {
						$models[] = ['model' => $modelClassName, 'title' => $name];
					}
				}
			}
		} else {
			$modelClassName = '\\' . str_replace('_', '\\', $value);
			if (class_exists($modelClassName)) {
				$models[] = ['model' => $modelClassName, 'title' => 'main'];
			}
		}

		return $models;
	}
}