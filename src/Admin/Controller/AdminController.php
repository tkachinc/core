<?php
namespace TkachInc\Core\Admin\Controller;

use TkachInc\Core\Admin\Classes\AdminApplication;
use TkachInc\Core\Auth\BasicSecure;
use TkachInc\Core\Auth\Types\AbstractAuthType;
use TkachInc\Core\BaseCoreController;
use TkachInc\Core\CMS\Controller\PageController;
use TkachInc\Core\Response\TwigRender;
use TkachInc\Engine\Application\ServerEvents;
use TkachInc\Engine\ServerContainerFacade;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\Engine\Services\Response\ResponseTypes\HTMLResponse;
use TkachInc\Engine\Services\Response\ResponseTypes\SpecificJSONResponse;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AdminController extends BaseCoreController
{
	/** @var  AdminApplication */
	protected $application;

	/**
	 * @var AbstractAuthType
	 */
	protected $secure;

	/**
	 * @throws \Exception
	 */
	public function __construct()
	{
		parent::__construct(new AdminApplication());

		$this->secure = BasicSecure::getInstance();
		if (!$this->secure->isLogged()) {
			if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']) || isset($_SESSION['logout'])) {
				unset($_SESSION['logout']);
				header('HTTP/1.1 401 Access Denied');
				header('WWW-Authenticate: Basic realm="Password protected"');
				header('HTTP/1.0 401 Unauthorized');
				throw new \Exception('Unauthorized');
			} else {
				BasicSecure::getInstance()->login($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
				Request::redirect('/AP');
			}
		}
	}

	public function index()
	{
		$container = ServerContainerFacade::getInstance();

		$render = new TwigRender();
		$container[ServerEvents::class]->emit(PageController::EVENT_CONTROLLER_TWIG_EXTENSION, [&$render]);
		$content = $render->render(
			__DIR__ . '/../Views/',
			[
				'body' => '<div class="col-lg-12"><div class="well">Welcome in Admin!</div></div>',
			],
			'components/main.html.twig'
		);
		$navbar = $render->render(
			__DIR__ . '/../Views/',
			['modelMap' => $this->application->getMenuUser($this->secure)],
			'components/navbar.html.twig'
		);
		$result = $render->render(
			__DIR__ . '/../Views/',
			['content' => $content, 'navbar' => $navbar],
			'main.html.twig'
		);

		(new HTMLResponse())->put($result);
	}

	public function generateHash()
	{
		$basic = BasicSecure::getInstance();
		$key = Request::getRequest('key');
		if (!empty($key)) {
			(new SpecificJSONResponse())->put(['hash' => $basic->getHash($key)]);
		} else {
			(new SpecificJSONResponse())->put([]);
		}
	}

	public function logout()
	{
		$basic = BasicSecure::getInstance();
		$basic->logout();
		Request::redirect('/AP');
	}
}