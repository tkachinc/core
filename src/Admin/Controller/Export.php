<?php
namespace TkachInc\Core\Admin\Controller;

use Exception;
use TkachInc\Core\CMS\Controller\PageController;
use TkachInc\Core\Config\Config;
use TkachInc\Core\Database\MongoDB\Helper\AggregateHelper;
use TkachInc\Core\Database\MongoDB\Helper\DecodeMongoRequest;
use TkachInc\Core\Database\MongoDB\ObjectModel;
use TkachInc\Core\Export\ExportConfig;
use TkachInc\Core\Export\ExportConfigByCache;
use TkachInc\Core\Export\ExportHistory;
use TkachInc\Core\Export\ExportManager;
use TkachInc\Core\Export\RelationExportConfig;
use TkachInc\Core\Response\TwigRender;
use TkachInc\Engine\Application\ServerEvents;
use TkachInc\Engine\ServerContainerFacade;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\Engine\Services\Response\ResponseTypes\HTMLResponse;
use TkachInc\Engine\Services\Response\ResponseTypes\SpecificJSONResponse;

/**
 * Управление пользователем и ботами в админке
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Export extends AdminController
{

	public function getModelFields()
	{
		$fields = [];
		/** @var ObjectModel $model */
		$model = (string)Request::getGetOrPost('model', '');
		if (class_exists($model) && method_exists($model, 'getFieldsDefault')) {
			$fields = $model::getFieldsDefault();
		}

		(new SpecificJSONResponse())->put($fields);
	}

	public function getModelDateFields()
	{
		$fields = [];
		/** @var ObjectModel $model */
		$model = (string)Request::getGetOrPost('model', '');
		if (class_exists($model) && method_exists($model, 'getFieldsDefault')) {
			$fields = $model::getFieldsValidate();
			foreach ($fields as $key => $value) {
				if ($value !== ObjectModel::TYPE_MONGO_DATE && $value !== ObjectModel::TYPE_TIMESTAMP) {
					unset($fields[$key]);
				}
			}
		}

		(new SpecificJSONResponse())->put($fields);
	}

	public function checkId()
	{
		$checkId = Request::getGetOrPost('checkId');
		$result = ExportHistory::isComplete($checkId);
		(new SpecificJSONResponse())->put(['result' => $result]);
	}

	public function exportById()
	{
		$id = Request::getGetOrPost('id');

		$config = new ExportConfigByCache($id);
		$specialId = ExportManager::checkCurrentExport();
		$container = ServerContainerFacade::getInstance();
		$export = new ExportManager(
			$container, $config, true, function ($exportId, $isExport) {
			if (!$isExport) {
				(new SpecificJSONResponse())->put(['exportId' => $exportId]);
			}
		}, $specialId
		);
		$export->run();
	}

	public function exportByGroup()
	{
		$model = Request::getGetOrPost('model', '');
		$requestUserFields = Request::getGetOrPost('fields', []);
		//$params = (array)json_decode(Request::getGetOrPost('params', '[]'), true);
		$cache = Request::getRequest('cacheInDb', false);
		$filter = (string)Request::getRequest('filter', '');
		//$sort = (string)Request::getRequest('sort', '');
		$groupType = Request::getGetOrPost('groupType', '');
		$dateGroupField = Request::getGetOrPost('dateGroupField', '');

		$cache = ObjectModel::decode($cache, ObjectModel::TYPE_BOOL);
		if ($cache === true) {
			$cache = false;
		} else {
			$cache = true;
		}

		if (is_array($requestUserFields)) {
			$userFields = $requestUserFields;
		} else {
			$userFields = [$requestUserFields];
		}

		/** @var ObjectModel $model */
		if (!class_exists($model) || !method_exists($model, 'getFieldsValidate')) {
			throw new Exception('Not found model');
		}

		$decode = new DecodeMongoRequest();

		$validate = $model::getFieldsValidate();
		$decode->setValidate($validate);

		$filters = $sourceFilter = $decode->jsonDecodeFilter($filter);
		$decode->recursiveDecode($filters);
		//$sort = $decode->getSortMongo($sort);

		$project = [];
		foreach ($userFields as $field) {
			$project[$field] = 1;
		}

		$group = [];
		$groupFlag = false;
		if (isset($validate[$dateGroupField]) &&
			$groupType !== 'default' &&
			$validate[$dateGroupField] === ObjectModel::TYPE_TIMESTAMP ||
			$validate[$dateGroupField] === ObjectModel::TYPE_MONGO_DATE
		) {
			$offset = (new \DateTime())->getOffset();
			if ($validate[$dateGroupField] === ObjectModel::TYPE_MONGO_DATE) {
				$project[$dateGroupField] = [
					'$add' => ['$' . $dateGroupField, $offset],
				];
			} else {
				$project[$dateGroupField] = [
					'$add' => [
						new \MongoDate($offset),
						['$multiply' => ['$' . $dateGroupField, 1000]],
					],
				];
			}

			switch ($groupType) {
				case 'byMonth':
					$group = [
						'_id' => [
							'year'  => ['$year' => '$' . $dateGroupField],
							'month' => ['$month' => '$' . $dateGroupField],
						],
					];
					break;
				default:
					$group = [
						'_id' => [
							'year'  => ['$year' => '$' . $dateGroupField],
							'month' => ['$month' => '$' . $dateGroupField],
							'day'   => ['$dayOfMonth' => '$' . $dateGroupField],
						],
					];
					break;
			}

			foreach ($project as $field => $item) {
				if ($field === '_id' || $field === $dateGroupField) {
					continue;
				}
				if ($validate[$field] === ObjectModel::TYPE_INT ||
					$validate[$field] === ObjectModel::TYPE_UNSIGNED_INT ||
					$validate[$field] === ObjectModel::TYPE_FLOAT ||
					$validate[$field] === ObjectModel::TYPE_UNSIGNED_FLOAT
				) {
					$group[$field] = ['$sum' => '$' . $field];
				} else {
					$group[$field] = ['$last' => '$' . $field];
				}
			}
			unset($validate['_id']);
			$groupFlag = true;
		}

		$relation = new AggregateHelper();
		if (!empty($filters)) {
			$relation->addMatch($filters);
		}
		$relation->addProject($project);
		if (!empty($group)) {
			$relation->addGroup($group);
		}

		$aggregateIntance = Config::getInstance()->get(['aggregateIntance'], '');
		$generator = $relation->generate(
			$model::getCollectionName(),
			function ($item) use ($validate, $groupType, $groupFlag) {
				if ($groupFlag) {
					if ($groupType === 'byMonth') {
						$date = new \DateTime();
						$date->setDate(
							isset($item['_id']['year']) ? $item['_id']['year'] : 1970,
							isset($item['_id']['month']) ? $item['_id']['month'] : 1,
							isset($item['_id']['day']) ? $item['_id']['day'] : 1
						);
						$item['_id'] = $date->format('Y-m');
					} else {
						$date = new \DateTime();
						$date->setDate(
							isset($item['_id']['year']) ? $item['_id']['year'] : 1970,
							isset($item['_id']['month']) ? $item['_id']['month'] : 1,
							isset($item['_id']['day']) ? $item['_id']['day'] : 1
						);
						$item['_id'] = $date->format('Y-m-d');
					}
				}

				foreach ($item as $field => $value) {
					if (isset($validate[$field])) {
						$item[$field] = ObjectModel::encode($item[$field], $validate[$field]);
					}
				}

				return $item;
			},
			$aggregateIntance
		);

		$dfields = $model::getFieldsDefault();
		$resultFields = [];
		foreach ($project as $field => $v) {
			$resultFields[$field] = isset($dfields[$field]) ? $dfields[$field] : '';
		}

		$config = new RelationExportConfig($generator, $resultFields);

		$specialId = ExportManager::checkCurrentExport();
		$container = ServerContainerFacade::getInstance();
		$export = new ExportManager(
			$container, $config, $cache, function ($exportId, $isExport) {
			if (!$isExport) {
				(new SpecificJSONResponse())->put(['exportId' => $exportId]);
			}
		}, $specialId
		);
		$export->run();
	}

	public function export()
	{
		$model = Request::getGetOrPost('model', '');
		$requestUserFields = Request::getGetOrPost('fields', []);
		//$params = (array)json_decode(Request::getGetOrPost('params', '[]'), true);
		$cache = Request::getRequest('cacheInDb', false);
		$filter = (string)Request::getRequest('filter', '');
		//$sort = (string)Request::getRequest('sort', '');
		$cache = ObjectModel::decode($cache, ObjectModel::TYPE_BOOL);
		if ($cache === true) {
			$cache = false;
		} else {
			$cache = true;
		}

		if (is_array($requestUserFields)) {
			$userFields = $requestUserFields;
		} else {
			$userFields = [$requestUserFields];
		}

		/** @var ObjectModel $model */
		if (!class_exists($model) || !method_exists($model, 'getFieldsValidate')) {
			throw new Exception('Not found model');
		}

		$decode = new DecodeMongoRequest();

		$validate = $model::getFieldsValidate();
		$default = $model::getFieldsDefault();
		$decode->setValidate($validate);

		$filters = $sourceFilter = $decode->jsonDecodeFilter($filter);
		$decode->recursiveDecode($filters);
		//$sort = $decode->getSortMongo($sort);

		$fields = [];
		foreach ($userFields as $field) {
			$fields[$field] = isset($default[$field]) ? $default[$field] : '';
		}
		$fieldsQuery = [];
		foreach ($userFields as $field) {
			$fieldsQuery[$field] = 1;
		}

		$config = new ExportConfig($model, [], $filters, $fields, $fieldsQuery);
		$specialId = ExportManager::checkCurrentExport();
		$container = ServerContainerFacade::getInstance();
		$export = new ExportManager(
			$container, $config, $cache, function ($exportId, $isExport) {
			if (!$isExport) {
				(new SpecificJSONResponse())->put(['exportId' => $exportId]);
			}
		}, $specialId
		);
		$export->run();
	}

	/**
	 * @throws \Exception
	 */
	public function index()
	{
		$urlMap = Config::getInstance()->loadFile('adminMenu')->get(['modelMap']);
		$models = $this->application->processingMenu($urlMap);

		$render = new TwigRender();

		$container = ServerContainerFacade::getInstance();
		$container[ServerEvents::class]->emit(PageController::EVENT_CONTROLLER_TWIG_EXTENSION, [&$render]);

		$navbar = $render->render(
			__DIR__ . '/../Views/',
			['modelMap' => $this->application->getMenuUser($this->secure)],
			'components/navbar.html.twig'
		);
		(new HTMLResponse())->put(
			$render->render(
				__DIR__ . '/../Views/exportHelper.html.twig',
				[
					'models' => $models,
					'navbar' => $navbar,
				]
			)
		);
	}

	public function merge()
	{
		$urlMap = Config::getInstance()->loadFile('adminMenu')->get(['modelMap']);
		$models = $this->application->processingMenu($urlMap);

		$render = new TwigRender();
		$container = ServerContainerFacade::getInstance();
		$container[ServerEvents::class]->emit(PageController::EVENT_CONTROLLER_TWIG_EXTENSION, [&$render]);
		$navbar = $render->render(
			__DIR__ . '/../Views/',
			['modelMap' => $this->application->getMenuUser($this->secure)],
			'components/navbar.html.twig'
		);
		(new HTMLResponse())->put(
			$render->render(
				__DIR__ . '/../Views/exportMergeHelper.html.twig',
				[
					'models' => $models,
					'navbar' => $navbar,
				]
			)
		);
	}

	public function byGroup()
	{
		$urlMap = Config::getInstance()->loadFile('adminMenu')->get(['modelMap']);
		$models = $this->application->processingMenu($urlMap);

		$render = new TwigRender();
		$container = ServerContainerFacade::getInstance();
		$container[ServerEvents::class]->emit(PageController::EVENT_CONTROLLER_TWIG_EXTENSION, [&$render]);
		$navbar = $render->render(
			__DIR__ . '/../Views/',
			['modelMap' => $this->application->getMenuUser($this->secure)],
			'components/navbar.html.twig'
		);
		(new HTMLResponse())->put(
			$render->render(
				__DIR__ . '/../Views/exportHelperByGroup.html.html.twig',
				[
					'models' => $models,
					'navbar' => $navbar,
				]
			)
		);
	}

	public function relation()
	{
		$toModel = Request::getGetOrPost('toModel', '');
		$fromModel = Request::getGetOrPost('fromModel', '');
		$exportFields = Request::getGetOrPost('exportFields', []);
		$exportFields2 = Request::getGetOrPost('exportFields2', []);
		$localField = Request::getGetOrPost('localField', '');
		$foreignField = Request::getGetOrPost('foreignField', '');
		$cache = Request::getRequest('cacheInDb', false);
		$filter = (string)Request::getRequest('filter', '');
		//$sort = (string)Request::getRequest('sort', '');
		$cache = ObjectModel::decode($cache, ObjectModel::TYPE_BOOL);
		if ($cache === true) {
			$cache = false;
		} else {
			$cache = true;
		}
		if (empty($localField) || empty($foreignField)) {
			throw new Exception('Error key');
		}
		if (!class_exists($toModel) || !method_exists($fromModel, 'getFieldsDefault')) {
			throw new Exception('Not found model');
		}
		if (!class_exists($fromModel) || !method_exists($fromModel, 'getFieldsDefault')) {
			throw new Exception('Not found model');
		}

		$decode = new DecodeMongoRequest();
		$validate = $toModel::getFieldsValidate();
		$decode->setValidate($validate);
		$filters = $sourceFilter = $decode->jsonDecodeFilter($filter);
		$decode->recursiveDecode($filters);
		//$sort = $decode->getSortMongo($sort);

		$toCollectionName = $toModel::getCollectionName();
		$fromCollectionName = $fromModel::getCollectionName();

		$aggregate = [];
		$fields = $toModel::getFieldsDefault();
		foreach ($fields as $field => $default) {
			$aggregate['$project'][$field] = 1;
		}
		$fields = $fromModel::getFieldsDefault();
		foreach ($fields as $field => $default) {
			$aggregate['$project']['m_' . $field] = '$' . $fromCollectionName . '.' . $field;
		}

		$aggregateHelper = new AggregateHelper();

		if (!empty($filters)) {
			$aggregateHelper->addMatch($filters);
		}

		$aggregateHelper->addRelationModel($fromCollectionName, $localField, $foreignField, $fromCollectionName);
		$aggregateHelper->addUnwind('$' . $fromCollectionName);
		$aggregateHelper->addCustomBlock($aggregate);

		$fields = [];
		foreach ($exportFields as $exportField) {
			$fields[$exportField] = 1;
		}
		foreach ($exportFields2 as $exportField) {
			$fields['m_' . $exportField] = 1;
		}
		$aggregateHelper->addProject($fields);
		//(new SpecificJSONResponse())->put($aggregateHelper->getQuery());exit;
		$aggregateInstance = Config::getInstance()->get(['aggregateInstance'], '');
		$generator = $aggregateHelper->generate(
			$toCollectionName,
			function ($item) {
				return $item;
			},
			$aggregateInstance
		);

		$dfields = $toModel::getFieldsDefault();
		$dfields2 = $fromModel::getFieldsDefault();
		$resultFields = [];
		foreach ($fields as $field => $v) {
			$resultFields[$field] = isset($dfields[$field]) ? $dfields[$field] : isset($dfields2['m_' .
				$field]) ? $dfields2['m_' . $field] : '';
		}

		$config = new RelationExportConfig($generator, $resultFields);

		$specialId = ExportManager::checkCurrentExport();
		$container = ServerContainerFacade::getInstance();
		$export = new ExportManager(
			$container, $config, $cache, function ($exportId, $isExport) {
			if (!$isExport) {
				(new SpecificJSONResponse())->put(['exportId' => $exportId]);
			}
		}, $specialId
		);
		$export->run();
	}
}
