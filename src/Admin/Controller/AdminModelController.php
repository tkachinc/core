<?php
namespace TkachInc\Core\Admin\Controller;

use TkachInc\Core\CMS\Controller\PageController;
use TkachInc\Core\Database\MongoDB\Helper\DecodeMongoRequest;
use TkachInc\Core\Database\MongoDB\ObjectModel;
use TkachInc\Core\Response\TwigRender;
use TkachInc\Core\URLFacade;
use TkachInc\Engine\Application\ServerEvents;
use TkachInc\Engine\ServerContainerFacade;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\Engine\Services\Response\ResponseTypes\HTMLResponse;
use TkachInc\Engine\Services\Response\ResponseTypes\SpecificJSONResponse;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class AdminModelController extends AdminController
{
	protected static $perPages = [10, 25, 50, 100, 150, 200, 300];
	protected $decode;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();
		$this->decode = new DecodeMongoRequest();
	}

	/**
	 * @param $name
	 * @param $attr
	 */
	public function __call($name, $attr)
	{
		$_REQUEST['name'] = $name;
		$action = Request::getRequest('action');

		if (method_exists($this, $action)) {
			$this->$action();
		} else {
			$this->view();
		}
	}

	/**
	 *
	 */
	public function view()
	{
		/** @var ObjectModel $model */
		$name = Request::getRequest('name');
		$model = $this->secure->checkRead($name);

		$page = Request::getRequest('page');

		$perPage = (int)Request::getRequest('perPage', 25);
		if (!in_array($perPage, static::$perPages)) {
			$perPage = 25;
		}

		/** @var ObjectModel $model */
		$fields = $model::getFieldsDefault();
		$validate = $model::getFieldsValidate();
		$this->decode->setValidate($validate);

		$filter = (string)Request::getRequest('filter', '');
		$sort = (string)Request::getRequest('sort', '');
		$filter = $sourceFilter = $this->decode->jsonDecodeFilter($filter);
		$this->decode->recursiveDecode($filter);
		$sort = $this->decode->getSortMongo($sort);

		$data = $model::getAllAdvanced($filter, [], false, $sort, $page, $perPage);
		$count = $model::count($filter);
		$pk = $model::getPK();
		$pages = ceil($count / $perPage) - 1;

		foreach ($data as $key => $item) {
			foreach ($fields as $key2 => $default) {
				if (!isset($data[$key][$key2])) {
					$data[$key][$key2] = $default;
				}

				//var_dump($data[$key]);ob_flush();
				//var_dump($key2, $item[$key][$key2], $validate[$key2]);
				if (isset($validate[$key2])) {
					$data[$key][$key2] = ObjectModel::encode($data[$key][$key2], $validate[$key2]);
				}
			}
		}
		//var_dump($data);
		//ob_flush();
		$container = ServerContainerFacade::getInstance();
		$render = new TwigRender();
		$container[ServerEvents::class]->emit(PageController::EVENT_CONTROLLER_TWIG_EXTENSION, [&$render]);
		foreach ($fields as $key => &$item) {
			if (isset($validate[$key])) {
				$item = ObjectModel::encode($item, $validate[$key]);
			}
		}

		$paginationArray = $this->pagination($page, $pages);
		$query = http_build_query($_GET);
		if (!empty($query)) {
			$query = '?' . $query;
		}

		if (!empty($data)) {
			$indexes = [];
			$itr = $model::getExistingIndexes();
			/** @var \MongoDB\Model\IndexInfo $index */
			foreach ($itr as $index) {
				$indexes[] = $index->__debugInfo();
			}

			$indexes = $render->render(
				__DIR__ . '/../Views/',
				[
					'indexes' => $indexes,
					'name'    => $name,
				],
				'components/collection_indexes.html.twig'
			);
		} else {
			$indexes = $render->render(
				__DIR__ . '/../Views/',
				[
					'indexes' => [],
					'name'    => $name,
				],
				'components/collection_indexes.html.twig'
			);
		}

		$pagination = $render->render(
			__DIR__ . '/../Views/',
			[
				'pagination' => $paginationArray,
				'page'       => $page,
				'pages'      => $pages,
				'name'       => $name,
				'query'      => $query,
			],
			'components/pagination.html.twig'
		);
		$content = $render->render(
			__DIR__ . '/../Views/',
			[
				'baseURL'  => URLFacade::getBaseURL(),
				'name'     => $name,
				'model'    => basename($model),
				'fields'   => $fields,
				'hints'    => $model::getFieldsHint(),
				'data'     => $data,
				'validate' => $validate,
				'pk'       => $pk,
			],
			'components/collection.html.twig'
		);
		$navbar = $render->render(
			__DIR__ . '/../Views/',
			['modelMap' => $this->application->getMenuUser($this->secure)],
			'components/navbar.html.twig'
		);
		$filterComponent = $render->render(
			__DIR__ . '/../Views/',
			[
				'name'     => $name,
				'query'    => $query,
				'fields'   => $fields,
				'perPage'  => $perPage,
				'filter'   => $sourceFilter,
				'sort'     => $sort,
				'perPages' => static::$perPages,
			],
			'components/filter.html.twig'
		);
		$result = $render->render(
			__DIR__ . '/../Views/',
			[
				'content'    => $content,
				'pagination' => $pagination,
				'navbar'     => $navbar,
				'filter'     => $filterComponent,
				'indexes'    => $indexes,
			],
			'index.html.twig'
		);

		(new HTMLResponse())->put($result);
	}

	/**
	 * @param $page
	 * @param $pages
	 * @return array
	 */
	protected function pagination($page, $pages)
	{
		if ($pages < 0) {
			$pages = 0;
		}

		if ($pages > 11) {
			if ($page < 5) {
				$pagination = range(0, 7);
				$pagination[] = -1;
				$pagination = array_merge($pagination, range($pages - 2, $pages));
			} elseif ($page >= $pages - 5) {
				$pagination = range(0, 2);
				$pagination[] = -1;
				$pagination = array_merge($pagination, range($pages - 7, $pages));
			} else {
				$start = $page - 2;
				$pagination = range(0, 2);
				$pagination[] = -1;
				$pagination = array_merge($pagination, range($start, $page + 2));
				$pagination[] = -1;
				$pagination = array_merge($pagination, range($pages - 2, $pages));
			}
		} else {
			$pagination = range(0, $pages);
		}

		return $pagination;
	}

	public function edit()
	{
		/** @var ObjectModel $model */
		$model = $this->secure->checkWrite(Request::getRequest('name'));

		$doc = Request::getRequest('doc', null);
		if (!isset($doc)) {
			throw new \Exception('Not found doc');
		}
		$fields = $model::getFieldsDefault();
		$validate = $model::getFieldsValidate();

		$systemFields = ['_newId' => 1];
		foreach ($doc as $field => &$item) {
			if (isset($systemFields[$field])) {
				continue;
			}

			if (!isset($fields[$field]) || !isset($validate[$field])) {
				throw new \Exception('Not found fields');
			}
			if (!is_array($item) && !is_object($item)) {
				$item = trim($item);
				$item = ObjectModel::decode($item, $validate[$field]);
			}
		}

		$pk = $model::getPK();
		if (!isset($doc[$pk])) {
			throw new \Exception('Not found pk');
		}
		$id = $doc[$pk];
		if ($id !== 0 && empty($id)) {
			throw new \Exception('Empty id');
		}
		if (empty($doc)) {
			throw new \Exception('Empty doc');
		}

		if (isset($doc['_newId'])) {
			$newId = trim($doc['_newId']);
			$newId = ObjectModel::decode($newId, $validate[$pk]);
			if (!empty($newId)) {
				$model::renamePk($id, $newId);
			}
			$obj = new $model([$pk => $newId]);
		} else {
			/** @var ObjectModel $obj */
			$obj = new $model([$pk => $id]);
			$obj->setFieldsArray($doc);
			$obj->save();
		}

		(new SpecificJSONResponse())->put([$obj->getFieldsArray()]);
	}

	public function delete()
	{
		/** @var ObjectModel $model */
		$model = $this->secure->checkWrite(Request::getRequest('name'));

		$ids = Request::getRequest('ids', []);
		if (empty($ids)) {
			throw new \Exception('Not found ids');
		}
		$pk = $model::getPK();
		$validate = $model::getFieldsValidate();

		foreach ($ids as &$id) {
			$id = ObjectModel::decode($id, $validate[$pk]);
		}

		$model::deleteAllAdvanced([$pk => ['$in' => $ids]]);

		(new SpecificJSONResponse())->put([]);
	}

	public function createIndex()
	{
		/** @var ObjectModel $model */
		$model = $this->secure->checkWrite(Request::getRequest('name'));

		$model::checkCappedCollection();
		$model::createIndex();
		(new SpecificJSONResponse())->put([]);
	}

	public function import()
	{
		/** @var ObjectModel $model */
		$model = $this->secure->checkWrite(Request::getRequest('name'));

		if (!isset($_FILES['import']) || !isset($_FILES['import']['tmp_name']) || empty($_FILES['import']['tmp_name'])) {
			header('Location: ' . $_SERVER['HTTP_REFERER']);
			exit;
		}

		$fileImport = $_FILES['import']['tmp_name'];

		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		ini_set("auto_detect_line_endings", true);

		$file = new \SplFileObject($fileImport, 'r');
		$csvFields = $file->fgetcsv();

		$fieldsDefault = $model::getFieldsDefault();
		$fieldsValidate = $model::getFieldsValidate();
		$model::disableCache();

		while ($csvObject = $file->fgetcsv()) {
			if (count($csvFields) != count($csvObject)) {
				continue;
			}
			$dbObject = array_combine($csvFields, $csvObject);

			foreach ($fieldsDefault as $field => $default) {
				if (!isset($dbObject[$field])) {
					$dbObject[$field] = $default;
				} else {
					$dbObject[$field] = trim($dbObject[$field]);
					$dbObject[$field] = ObjectModel::decode($dbObject[$field], $fieldsValidate[$field]);
				}
			}

			/**
			 * @var ObjectModel $object
			 */
			$object = new $model();
			$object->setFieldsArray($dbObject);
			$object->save();
		}
		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}

	public function export()
	{
		/** @var ObjectModel $model */
		$name = Request::getRequest('name');
		$model = $this->secure->checkRead($name);

		/** @var ObjectModel $model */
		$fieldsDefault = $model::getFieldsDefault();
		$fieldsValidate = $model::getFieldsValidate();
		$this->decode->setValidate($fieldsValidate);

		$filter = (string)Request::getRequest('filter', '');
		$sort = (string)Request::getRequest('sort', '');
		$filter = $sourceFilter = $this->decode->jsonDecodeFilter($filter);
		$this->decode->recursiveDecode($filter);
		$sort = $this->decode->getSortMongo($sort);

		$outstream = fopen("php://output", 'w');

		$getGenerator = function () use ($model, $filter, $sort) {
			$cursor = $model::getAllAdvanced($filter, [], false, $sort, false, false, true);
			foreach ($cursor as $item) {
				yield $item;
			}
		};

		ini_set('memory_limit', '1024M');
		set_time_limit(0);
		session_write_close();
		while (ob_get_level()) {
			ob_end_clean();
		}
		setlocale(LC_ALL, 'ru_RU.utf8', 'ru_RU.UTF-8');
		setlocale(LC_NUMERIC, 'C');
		if (!Request::isCLI()) {
			header('Content-type: text/csv');
			header('Content-Disposition: attachment; filename="' . $name . '_' . date('YmdHis') . '_' . mt_rand(0, 1000) . '.csv"');
		}

		$generator = $getGenerator();

		fputcsv($outstream, array_keys($fieldsDefault));
		$dbobject = [];
		foreach ($generator as $item) {
			foreach ($fieldsDefault as $field => $default) {
				if (!isset($item[$field])) {
					$item[$field] = $default;
				}
				$dbobject[$field] = ObjectModel::encode($item[$field], $fieldsValidate[$field]);
				$dbobject[$field] = trim($dbobject[$field]);
			}
			fputcsv($outstream, $dbobject);
		}
		fclose($outstream);
	}
}