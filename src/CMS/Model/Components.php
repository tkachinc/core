<?php
namespace TkachInc\Core\CMS\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @property mixed method
 * @property mixed position
 * @property mixed data
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Components extends ObjectModel
{
	/** @var string Метод обновления записи - full, inc, set, cacheOnly */
	//	protected static $_updateMethod = self::UPDATE_METHOD_CACHE_ONLY;
	/** @var string $collection Название модели */
	protected static $_collection = 'components_model';
	protected static $_readPreference = ObjectModel::RP_SECONDARY_PREFERRED;
	/** @var string $pk первичный ключ */
	protected static $_pk = '_id';
	/** @var array $sort параметры сортировки */
	protected static $_sort = ['priority' => 1];
	/** @var array $indexes индексы коллекции */
	protected static $_indexes = [
		[
			'keys'   => ['name' => 1, 'langs' => 1],
			'unique' => true,
		],
		[
			'keys' => ['position' => 1],
		],
		[
			'keys' => ['group' => 1],
		],
	];

	/** @var array $fieldsDefault список значений коллекции */
	protected static $_fieldsDefault = [
		'_id'      => '',
		'langs'    => [],
		'name'     => '',
		'template' => '',
		'position' => '',
		'data'     => [],
		'callback' => [],
		'priority' => 0,
		'group'    => '',
	];
	/** @var array $fieldsValidate типы значений для валидации */
	protected static $_fieldsValidate = [
		'_id'      => self::TYPE_MONGO_ID,
		'langs'    => self::TYPE_JSON,
		'name'     => self::TYPE_STRING,
		'template' => self::TYPE_STRING,
		'position' => self::TYPE_STRING,
		'data'     => self::TYPE_JSON,
		'callback' => self::TYPE_JSON,
		'priority' => self::TYPE_INT,
		'group'    => self::TYPE_STRING,
	];

	protected static $_fieldsHint = [
		'_id' => 'Идентификатор БД',
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;
	protected static $_fields = [];
	protected static $_fieldsPrivate = [];
	protected static $_fieldsSearch = [];
	protected static $_updateMethod = self::UPDATE_METHOD_SET;
} 