<?php
namespace TkachInc\Core\CMS\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * Class RouterModel
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class RouterModel extends ObjectModel
{
	protected static $_writeConcernMode = 1;
	protected static $_readPreference = ObjectModel::RP_SECONDARY_PREFERRED;
	protected static $_updateMethod = self::UPDATE_METHOD_SET;
	protected static $_collection = 'router_map';
	protected static $_pk = '_id';
	protected static $_sort = ['_id' => 1];
	protected static $_indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true,
		],
	];
	/** @var array $fieldsDefault список значений коллекции */
	protected static $_fieldsDefault = [
		'_id'  => '',
		'langs'  => [],
		'name'  => '',
		'data' => [],
		'type' => '',
		'template' => '',
		'sitemap' => false,
	];
	/** @var array $fieldsValidate типы значений для валидации */
	protected static $_fieldsValidate = [
		'_id'  => self::TYPE_MONGO_ID,
		'langs'  => self::TYPE_JSON,
		'name'  => self::TYPE_STRING,
		'data' => self::TYPE_JSON,
		'type' => self::TYPE_STRING,
		'template' => self::TYPE_STRING,
		'sitemap' => self::TYPE_BOOL,
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;
}