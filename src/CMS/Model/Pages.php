<?php
namespace TkachInc\Core\CMS\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @property mixed components
 * @property mixed data
 * @property mixed template
 * @property array callback
 * @property mixed name
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Pages extends ObjectModel
{
	/** @var string Метод обновления записи - full, inc, set, cacheOnly */
	//	protected static $_updateMethod = self::UPDATE_METHOD_CACHE_ONLY;
	/** @var string $collection Название модели */
	protected static $_collection = 'pages_model';
	protected static $_readPreference = ObjectModel::RP_SECONDARY_PREFERRED;
	/** @var string $pk первичный ключ */
	protected static $_pk = '_id';
	/** @var array $sort параметры сортировки */
	protected static $_sort = ['_id' => 1];
	/** @var array $indexes индексы коллекции */
	protected static $_indexes = [
		[
			'keys'   => ['name' => 1, 'langs' => 1],
			'unique' => true,
		],
		[
			'keys' => ['group' => 1],
		],
	];

	/** @var array $fieldsDefault список значений коллекции */
	protected static $_fieldsDefault = [
		'_id'          => '',
		'langs'        => [],
		'name'         => '',
		'file'         => '',
		'template'     => '',
		'components'   => [],
		'data'         => [],
		'requiredAuth' => false,
		'onlyByRoute' => false,
		'callback'     => [],
		'group'        => '',
	];
	/** @var array $fieldsValidate типы значений для валидации */
	protected static $_fieldsValidate = [
		'_id'          => self::TYPE_MONGO_ID,
		'langs'        => self::TYPE_JSON,
		'name'         => self::TYPE_STRING,
		'file'         => self::TYPE_STRING,
		'template'     => self::TYPE_STRING,
		'components'   => self::TYPE_JSON,
		'data'         => self::TYPE_JSON,
		'requiredAuth' => self::TYPE_BOOL,
		'onlyByRoute' => self::TYPE_BOOL,
		'callback'     => self::TYPE_JSON,
		'group'        => self::TYPE_STRING,
	];

	protected static $_fieldsHint = [
		'_id' => 'Идентификатор БД',
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;
	protected static $_updateMethod = self::UPDATE_METHOD_SET;
}