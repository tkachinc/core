<?php
namespace TkachInc\Core\CMS\Classes;

use Pimple\Container;
use TkachInc\Core\CMS\Controller\PageController;
use TkachInc\Core\CMS\Model\Components;
use TkachInc\Core\CMS\Model\Pages;
use TkachInc\Core\Config\Config;
use TkachInc\Core\Response\TwigRender;
use TkachInc\Engine\Application\ServerEvents;
use TkachInc\Engine\Services\Request\Request;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Page
{
	/**
	 * @param Container                  $container
	 * @param                            $langName
	 * @param                            $pageName
	 * @param bool                       $rendering
	 * @param null                       $templateDir
	 *
	 * @return array
	 * @throws PageException
	 */
	public static function generatePage(Container $container,
	                                    $langName,
	                                    $pageName,
	                                    $rendering = true,
	                                    &$templateDir = null, $byRoute = false)
	{
		/** @var ServerEvents $events */
		$events = $container[ServerEvents::class];

		Pages::setUpdateMethod(Pages::UPDATE_METHOD_CACHE_ONLY);
		$page = new Pages(['$or' => [['name' => $pageName, 'langs' => $langName], ['name' => $pageName, 'langs' => ['$size' => 0]]]]);
		if (!$page->isLoadedObject() || ($page->onlyByRoute === true && $byRoute === false)) {
			throw new PageException('Page not found', PageException::PAGE_NOT_FOUND);
		}

		$events->emit('page_call', [&$page, &$rendering]);
		static::callback($page->callback, [$container, &$page, &$rendering]);

		$templateDir = $page->template;

		$additionalComponents = Request::getRequest('additionalComponents', null);
		if ($additionalComponents !== null) {
			$additionalComponents = json_decode((string)$additionalComponents, true);
			if (json_last_error() === JSON_ERROR_NONE && is_array($additionalComponents) && is_array($page->components)) {
				$page->components = array_merge($page->components, $additionalComponents);
			}
		}
		$globalComponents = Config::getInstance()->get(['additionalComponents'], []);
		if (is_array($globalComponents)) {
			$page->components = array_merge($page->components, $globalComponents);
		}

		// Получаем все компоненты страницы
		$components = Components::getAllAdvanced(
			['$or' => [['name' => ['$in' => array_keys($page->components)], 'langs' => $langName], ['name' => ['$in' => array_keys($page->components)], 'langs' => ['$size' => 0]]]],
			[],
			false,
			true
		);
		$pathToComponentTemplate = PageVariable::getTemplateDir($page->template, '', true);
		$result = [];
		foreach ($components as $component) {
			$events->emit('component_call', [&$component, &$page, &$rendering]);
			$hook = Config::getInstance()->get(['cms', 'componentHook', 'callback']);
			static::callback($hook, [$container, &$component, &$page, $rendering]);
			static::callback($component['callback'], [$container, &$component, &$page, $rendering]);

			if (isset($component['data']) && isset($component['position']) && !empty($component['position'])) {
				if ($rendering === true && isset($component['template']) && !empty($component['template'])) {
					$twig = new TwigRender();
					$events->emit(PageController::EVENT_CONTROLLER_TWIG_EXTENSION, [&$twig]);
					$result[$component['position']][] = $twig->render(
						$pathToComponentTemplate,
						$component['data'],
						$component['template']
					);
				} else {
					$result[$component['position']][] = $component['data'];
				}
			}
		}

		unset($components);

		$events->emit('meta_inject', [&$page->name, &$page->data]);

		return [
			'template'      => $page->template,
			'content'       => $page->data,
			'pageName'      => $pageName,
			'langName'      => $langName,
			'result'        => $result,
			'componentList' => $page->components,
			'file' => $page->file,
		];
	}

	/**
	 * @param       $callback
	 * @param array $attributes
	 */
	protected static function callback($callback, Array $attributes)
	{
		if (is_callable($callback)) {
			call_user_func_array($callback, $attributes);
		} elseif (is_array($callback)) {
			foreach ($callback as $item) {
				if (is_callable($item)) {
					call_user_func_array($item, $attributes);
				}
			}
		}
	}
}