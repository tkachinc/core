<?php
namespace TkachInc\Core\CMS\Classes;

use TkachInc\Core\Config\Config;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PageVariable
{
	const DIR_TEMPLATES = 'templates';
	const DIR_COMPONENTS = 'components';

	public static $pageName;
	public static $langName;

	public static function getPageName()
	{
		return static::$pageName;
	}

	public static function getLangName()
	{
		return static::$langName;
	}

	/**
	 * @param $pageName
	 */
	public static function setPageName($pageName)
	{
		static::$pageName = $pageName;
	}

	/**
	 * @param $langName
	 */
	public static function setLangName($langName)
	{
		static::$langName = $langName;
	}

	/**
	 * @param        $templateDir
	 * @param string $file
	 * @param bool $isComponent
	 * @return mixed|string
	 */
	public static function getTemplateDir($templateDir, $file = '', $isComponent = false)
	{
		$path = Config::getInstance()->get(['cms', 'dir', 'path'], APP_PATH . '/' . PageVariable::DIR_TEMPLATES);
		if (empty($templateDir)) {
			$templateDir = Config::getInstance()->get(['cms', 'dir', 'template'], '');
		}

		if (!empty($templateDir)) {
			$path = $path . '/' . $templateDir;
		}

		if ($isComponent) {
			$path = $path . '/' . Config::getInstance()->get(['cms', 'dir', 'component'], PageVariable::DIR_COMPONENTS);
		}

		if (!empty($file)) {
			$path = $path . '/' . trim($file, '/');
		}

		return $path;
	}
} 