<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 1/31/17
 * Time: 18:43
 */

namespace TkachInc\Core\CMS\Classes;

use Pimple\Container;
use TkachInc\Core\CMS\Controller\PageController;
use TkachInc\Core\Config\Config;
use TkachInc\Core\Response\TwigRender;
use TkachInc\Core\Response\URLTwigExtension;
use TkachInc\Engine\Application\BaseApplication;
use TkachInc\Engine\Application\ServerEvents;
use TkachInc\Engine\Application\ServerPrepare;
use TkachInc\Engine\Router\Route;
use TkachInc\Engine\ServerContainerFacade;
use TkachInc\Engine\Services\Handler\HandlerRegistry;
use TkachInc\Engine\Services\Response\Response;
use TkachInc\Engine\Services\Response\ResponseTypes\HTMLResponse;
use TkachInc\Engine\Services\Response\ResponseTypes\SpecificJSONResponse;

class CMSApplication extends BaseApplication
{
	public $templateDir = null;

	const EVENT_APPLICATION_ERROR_HANDLER = 'event.application.error.handler';

	public function listen(ServerEvents $listen)
	{
		parent::listen($listen);
		$listen->on(
			PageController::EVENT_CONTROLLER_TWIG_EXTENSION,
			function (&$twig) {
				$container = ServerContainerFacade::getInstance();
				$twig->addExtension(
					new URLTwigExtension(
						$container[Route::class]
					)
				);
			}
		);
	}

	public function register(Container $pimple)
	{
		$this->initHTMLResponse();
	}

	protected function handler()
	{
		$container = ServerContainerFacade::getInstance();
		/** @var HandlerRegistry $handler */
		$handler = $container[HandlerRegistry::class];
		/** @var ServerEvents $events */
		$events = $container[ServerEvents::class];

		$container[ServerPrepare::class]->displayError(false);

		$events->on(
			HandlerRegistry::TYPE_THROWABLE,
			function (Array $response) {
				//var_dump($response);ob_flush();
				$code = (string)$response['code'];
				if (empty($code)) {
					$code = '404';
				} else {
					$this->code = (int)$code;
				}

				if($this->code > 599) {
					$this->code = 500;
				}

				if($code === '404') {
					$this->code = 404;
				}


				$renderFile = PageVariable::getTemplateDir($this->templateDir, $code, false);
				if (!file_exists($renderFile) || !is_file($renderFile)) {
					$renderFile = PageVariable::getTemplateDir($this->templateDir, '404', false);
					if ($code === PageException::PAGE_NOT_FOUND || !file_exists($renderFile) || !is_file($renderFile)) {
						$path = Config::getInstance()->get(
							['cms', 'dir', 'path'],
							APP_PATH . '/' . PageVariable::DIR_TEMPLATES
						);
						$renderFile = $path . '/404.html.twig';
					}
				}
				$this->sendResponseByParams(
					[
						'message' => $response['message'],
					],
					$renderFile
				);
				exit;
			}
		);

		$events->emit(self::EVENT_APPLICATION_ERROR_HANDLER, [$this, $handler]);

		$handler->registerHandler();
	}

	public function setDefaultParams(&$pageName, &$langName)
	{
		$pageName = Config::getInstance()->get(['page', 'base'], 'index');
		$langName = Config::getInstance()->get(['lang', 'prefix'], 'en');

		return $this;
	}

	public function initJSONResponse()
	{
		$container = ServerContainerFacade::getInstance();
		unset($container[Response::class]);
		$container[Response::class] = function () {
			return new SpecificJSONResponse();
		};

		return $this;
	}

	public function initHTMLResponse()
	{
		$container = ServerContainerFacade::getInstance();
		unset($container[Response::class]);
		$container[Response::class] = function () {
			return new HTMLResponse();
		};

		return $this;
	}

	public function sendResponseByParams(Array $data = [], $renderFile = '')
	{
		switch (ServerContainerFacade::get('typeResponse')) {
			case PageController::RESPONSE_JSON:
				$content = $data;
				$this->initJSONResponse();
				break;
			case PageController::RESPONSE_HTML:
			default:
				if (!file_exists($renderFile) || !is_file($renderFile)) {
					$this->code = 404;
					$path = Config::getInstance()->get(
						['cms', 'dir', 'path'],
						APP_PATH . '/' . PageVariable::DIR_TEMPLATES
					);
					$render404File = $path . '/404.html.twig';
					if ($renderFile !== $render404File) {
						$this->sendResponseByParams(
							[
								'message' => 'Not found template',
							],
							$render404File
						);
						exit;
					} else {
						throw new \Exception('Not found template and not found 404 template');
					}
				}
				$twig = new TwigRender();

				$container = ServerContainerFacade::getInstance();
				$container[ServerEvents::class]->emit(PageController::EVENT_CONTROLLER_TWIG_EXTENSION, [&$twig]);

				$content = $twig->render($renderFile, $data);
				$this->initHTMLResponse();
				break;
		}

		$this->data = $content;

		$this->response();
	}
}