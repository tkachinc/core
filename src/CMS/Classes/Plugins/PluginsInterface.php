<?php
namespace TkachInc\Core\CMS\Classes\Plugins;

use Pimple\Container;
use TkachInc\Core\CMS\Model\Pages;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
interface PluginsInterface
{
	/**
	 * @param Container $container
	 * @param array $component
	 * @param Pages $page
	 * @param bool $rendering
	 * @return array
	 */
	public static function run(Container $container, Array &$component, Pages &$page, $rendering = false);
}