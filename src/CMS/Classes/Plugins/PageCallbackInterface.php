<?php
namespace TkachInc\Core\CMS\Classes\Plugins;

use Pimple\Container;
use TkachInc\Core\CMS\Model\Pages;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
interface PageCallbackInterface
{
	/**
	 * @param Container $container
	 * @param Pages $page
	 * @param bool $rendering
	 * @return array
	 */
	public static function run(Container $container, Pages &$page, $rendering = false);
}