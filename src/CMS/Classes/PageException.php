<?php
namespace TkachInc\Core\CMS\Classes;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PageException extends \Exception
{
	const PAGE_NOT_FOUND = 404;
	const TEMPLATE_NOT_FOUND = 502;
	const ERROR_PARAMS = 503;
} 