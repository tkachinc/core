<?php
namespace TkachInc\Core\CMS\Classes;

/**
 * Class CMSRouter
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class CMSRouter
{
	/**
	 * @param array $data
	 * @param array $unused
	 * @param       $page
	 * @param array $attributes
	 */
	public static function recursiveCMSRouter(&$attr, Array &$data, Array &$unused, &$page, &$defaultPage, Array &$attributes)
	{
		if (isset($data[$attr]) || (isset($data['%s']))) {
			$attr = array_shift($unused);

			$key = $attr;
			if (!isset($data[$attr])) {
				$attr = '%s';
			}

			if (isset($data[$attr]['_name'])) {
				$attributes[$data[$attr]['_name']] = $key;
			}

			$page = isset($data[$attr]['_page']) ? $data[$attr]['_page'] : null;
			$defaultPage = isset($data[$attr]['_defaultPage']) ? $data[$attr]['_defaultPage'] : $defaultPage;

			$arguments = isset($data[$attr]['_arguments']) ? $data[$attr]['_arguments'] : [];
			if (!empty($arguments)) {
				foreach ($arguments as $attr => $default) {
					if (!empty($unused)) {
						$attributes[$attr] = array_shift($params);
					} else {
						$attributes[$attr] = $default;
					}
				}
			}

			if (isset($data[$attr]['_data']) && !empty($unused)) {
				$attr2 = reset($unused);
				//var_dump($attr2);ob_flush();
				static::recursiveCMSRouter(
					$attr2,
					$data[$attr]['_data'],
					$unused,
					$page,
					$defaultPage,
					$attributes
				);
			}
		}
	}
}