<?php
namespace TkachInc\Core\CMS\Classes\Controls;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class PageControl
{
	protected static $instance = [];
	protected $data = [];

	/**
	 * @param $page
	 * @return mixed
	 */
	public static function getInstance($page)
	{
		if (!isset(static::$instance[$page])) {
			static::$instance[$page] = new static();
		}

		return static::$instance[$page];
	}

	/**
	 * @param array ...$params
	 * @return array
	 */
	abstract public function run(...$params);

	/**
	 * @param $key
	 * @return null
	 */
	public function get($key)
	{
		return isset($this->data[$key]) ? $this->data[$key] : null;
	}
}