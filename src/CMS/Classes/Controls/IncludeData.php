<?php
namespace TkachInc\Core\CMS\Classes\Controls;

/**
 * Class GetGroupData
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class IncludeData
{
	protected static $instance = [];

	/**
	 * @return $this
	 */
	public static function getInstance()
	{
		if (!isset(static::$instance[static::class])) {
			static::$instance[static::class] = new static();
		}

		return static::$instance[static::class];
	}

	public static function removeInstance()
	{
		if (isset(static::$instance[static::class])) {
			unset(static::$instance[static::class]);
		}
	}

	/**
	 * @param array ...$params
	 * @return mixed
	 */
	abstract public function run(...$params);
}