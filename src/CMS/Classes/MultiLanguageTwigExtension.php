<?php
namespace TkachInc\Core\CMS\Classes;

/**
 * Class MultiLanguageTwigFilter
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class MultiLanguageTwigExtension extends \Twig_Extension
{
	protected $locale;
	protected $langName;

	/**
	 * MultiLanguageTwigExtension constructor.
	 *
	 * @param $locale
	 * @param $langName
	 */
	public function __construct($locale, $langName)
	{
		$this->locale = $locale;
		$this->langName = $langName;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'multiLanguage';
	}

	/**
	 * @return array
	 */
	public function getFilters()
	{
		return [
			new \Twig_SimpleFilter(
				'trans', function ($string, $type = null, $locale = null) {
				if (isset($locale)) {
					$this->locale = $locale;
				}

				return (new MultiLanguage($this->locale))->get($string, $string, $type);
			}
			),
		];
	}
}