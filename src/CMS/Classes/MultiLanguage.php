<?php
namespace TkachInc\Core\CMS\Classes;

use TkachInc\Core\Config\Config;

/**
 * Class MultiLanguage
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class MultiLanguage
{
	const DEFAULT_LOCALE = 'en_US';

	protected $locale;

	/**
	 * MultiLanguageTwig constructor.
	 *
	 * @param $locale
	 * @throws \Exception
	 */
	public function __construct($locale = null)
	{
		if ($locale === null) {
			$locale = self::DEFAULT_LOCALE;
		}

		$this->locale = $locale;
	}

	/**
	 * @param $key
	 * @param bool $default
	 * @param null $type
	 * @return mixed
	 */
	public function get($key, $default = false, $type = null)
	{
		$keys = explode('.', $key);
		$translate = null;

		/** @var Config $config */
		$config = Config::getInstance();
		$config->loadFile('translate/' . $this->locale);
		$config->loadDB($this->locale);

		if ($type !== null) {
			$config->loadFile('translate/' . $this->locale . '/' . $type);
		} elseif (count($keys) > 1) {
			$type = reset($keys);
			$types = array_slice($keys, 1);
			$config->loadFile('translate/' . $this->locale . '/' . $type);

			return $config->get($types, $config->get($keys, $config->get([$key], $this->getFromDBConfig($config, $keys, $default))));
		}

		return $config->get($keys, $config->get([$key], $this->getFromDBConfig($config, $keys, $default)));
	}

	/**
	 * @param Config $config
	 * @param        $keys
	 * @param null   $default
	 *
	 * @return mixed
	 */
	public function getFromDBConfig(Config $config, $keys, $default = null) {
		return $config->get(array_merge([$this->locale],$keys,['value']), $default);
	}
}