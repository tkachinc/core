<?php
namespace TkachInc\Core\CMS\Controller;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class JSONController extends PageController
{
	const RENDERING_BASE = false;
	const TYPE_RESPONSE_BASE = self::RESPONSE_JSON;
} 