<?php
namespace TkachInc\Core\CMS\Controller;

use TkachInc\Core\BaseCoreController;
use TkachInc\Core\CMS\Classes\CMSApplication;
use TkachInc\Core\CMS\Classes\Page;
use TkachInc\Core\CMS\Classes\PageException;
use TkachInc\Core\CMS\Classes\PageVariable;
use TkachInc\Core\Config\Config;
use TkachInc\Engine\Application\BaseApplication;
use TkachInc\Engine\Application\ServerPrepare;
use TkachInc\Engine\Router\Route;
use TkachInc\Engine\ServerContainerFacade;
use TkachInc\Engine\Services\Request\Request;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PageController extends BaseCoreController
{
	/** @var  CMSApplication */
	protected $application;

	const RESPONSE_HTML = 'html';
	const RESPONSE_JSON = 'json';
	const PAGE_BASE = 'index';
	const LANG_BASE = 'en';
	const RENDERING_BASE = true;
	const TYPE_RESPONSE_BASE = self::RESPONSE_HTML;
	const EVENT_CONTROLLER_TWIG_EXTENSION = 'event.controller.twig.extension';

	protected $byRoute = false;

	/**
	 * Init params
	 *
	 * @param CMSApplication $application
	 */
	public function __construct(CMSApplication $application = null)
	{
		$container = ServerContainerFacade::getInstance();

		if (!isset($application)) {
			$application = $container[BaseApplication::class]??new CMSApplication();
		}

		parent::__construct($application);

		$container[ServerPrepare::class]->enableSession();

		$pageName = self::PAGE_BASE;
		$langName = self::LANG_BASE;
		$this->application->setDefaultParams($pageName, $langName);

		$pageName = (string)Request::getGetOrPost('page', $pageName);
		$langName = (string)Request::getGetOrPost('lang', $langName);
		$rendering = (bool)Request::getGetOrPost('rendering', self::RENDERING_BASE);
		$typeResponse = (string)Request::getGetOrPost('typeResponse', self::TYPE_RESPONSE_BASE);

		if ($rendering === false) {
			$typeResponse = self::RESPONSE_JSON;
		}

		$container['pageName'] = $pageName;
		$container['langName'] = $langName;
		$container['rendering'] = $rendering;
		$container['typeResponse'] = $typeResponse;
	}

	/**
	 * Магичесикй вызов нужной страницы
	 *
	 * @param $method
	 * @param $args
	 */
	public function __call($method, $args)
	{
		$container = ServerContainerFacade::getInstance();
		/** @var Route $route */
		$route = $container[Route::class];

		$useFullRoute = Config::getInstance()->get(['page', 'useFullRoute'], false);
		$routeStr = Config::getInstance()->get(['route']);
		if ($useFullRoute && $routeStr) {
			$routeStr = $route->getMethodName() . '_' . implode('_', $route->getUnusedParams());
			$container['pageName'] = trim($routeStr, '_');
		} else {
			$container['pageName'] = $method;
		}

		$this->index();
	}

	/**
	 * Базовая страница
	 */
	public function index()
	{
		$container = ServerContainerFacade::getInstance();
		PageVariable::setPageName($container['pageName']);
		PageVariable::setLangName($container['langName']);
		$page = Page::generatePage(
			ServerContainerFacade::getInstance(),
			$container['langName'],
			$container['pageName'],
			$container['rendering'],
			$this->application->templateDir,
			$this->byRoute
		);

		$renderFile = PageVariable::getTemplateDir($page['template'], !empty($page['file']) ? $page['file'] : $container['pageName'] . '.html.twig', false);

		if (!file_exists($renderFile) || !is_file($renderFile)) {
			$renderFile = PageVariable::getTemplateDir($page['template'], self::PAGE_BASE . '.html.twig', false);
			if (!file_exists($renderFile) || !is_file($renderFile)) {
				throw new PageException('Not found template', PageException::TEMPLATE_NOT_FOUND);
			}
		}

		$this->application->sendResponseByParams($page, $renderFile);
	}
}