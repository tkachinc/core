<?php
namespace TkachInc\Core\CMS\Controller;

use TkachInc\Core\CMS\Classes\CMSRouter;
use TkachInc\Core\CMS\Classes\PageException;
use TkachInc\Core\CMS\Model\RouterModel;
use TkachInc\Engine\Router\Route;
use TkachInc\Engine\ServerContainerFacade;

/**
 * Class PageRouterController
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PageRouterController extends PageController
{
	protected $attributes = [];

	public function __call($method, $args)
	{
		$container = ServerContainerFacade::getInstance();
		$route = $container[Route::class];

		$unusedParams = $route->getUnusedParams();
		$container['pageName'] = null;

		$container['attributes'] = function () {
			return $this->attributes;
		};

		$defaultPage = '404';

		$model = new RouterModel(['$or' => [['name' => $method, 'langs' => $container['langName']], ['name' => $method, 'langs' => ['$size' => 0]]]]);
		if ($model->isLoadedObject()) {
			$this->application->templateDir = $model->template;
			array_unshift($unusedParams, $method);
			$data = [$method => $model->data];
			CMSRouter::recursiveCMSRouter($method, $data, $unusedParams, $container['pageName'], $defaultPage, $this->attributes);
			if (!$container['pageName']) {
				$container['pageName'] = $defaultPage;
			}

			$page = (int)$container['pageName'];

			if ($page > 100) {
				throw new PageException('Page have error', $page);
			}

			$this->byRoute = true;
			$this->index();
		} else {
			parent::__call($method, $args);
		}
	}
}