<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Lastmod extends AbstractTag implements InterfaceChildTypes
{
	const PATTERN = '<lastmod>%1$s</lastmod>';

	public function __construct(\DateTime $time)
	{
		$this->add($time->format('c'), 'lastmod');
	}
}