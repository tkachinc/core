<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Priority extends AbstractTag
{
	const PATTERN = '<priority>%1$s</priority>';

	public function __construct(float $priority)
	{
		$this->add($priority, 'priority');
	}
}