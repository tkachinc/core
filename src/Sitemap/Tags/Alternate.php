<?php
namespace TkachInc\Core\Sitemap\Tags;
use TkachInc\Core\URLFacade;

/**
 * @author maxim
 */
class Alternate extends AbstractTag implements InterfaceChildTypes
{
	const PATTERN = '<xhtml:link rel="alternate" %2$s/>';

	public function __construct(string $path = null, string $url = null, $hreflang = null)
	{
		if ($path && !$url) {
			$url = URLFacade::to($path);
		} elseif (!$path && !$url) {
			throw new \Exception('All params is null');
		}

		if ($hreflang) {
			$this->addAttributes(sprintf('hreflang="%1$s" href="%2$s"', $hreflang, $url), 'alternate');
		} else {
			$this->addAttributes(sprintf('href="%1$s"', $url), 'alternate');
		}
	}
}