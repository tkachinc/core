<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Sitemapindex extends AbstractTag implements InterfaceBaseTypes
{
	const PATTERN = '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 " %2$s>%1$s</sitemapindex>';

	public function addSitemap(Sitemap $sitemap)
	{
		$this->add($sitemap);
	}

	public function getXml()
	{
		return new Xml($this);
	}
}