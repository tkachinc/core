<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Image extends AbstractTag implements InterfaceChildTypes
{
	const PATTERN = '<image:image>%s</image:image>';

	public function __construct(string $loc, string $caption)
	{
		$this->add(sprintf('<image:loc>%1$s</image:loc><image:caption>%2$s</image:caption>', $loc, $caption), 'image');
	}
}