<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Video extends AbstractTag implements InterfaceChildTypes
{
	const PATTERN = '<video:video>%s</video:video>';

	public function __construct(string $contentLoc, string $playerLoc, string $thumbnailLoc, string $title, string $description)
	{
		$this->add(sprintf('<video:content_loc>%1$s</video:content_loc><video:player_loc allow_embed="yes" autoplay="ap=1">%2$s</video:player_loc><video:thumbnail_loc>%3$s</video:thumbnail_loc><video:title>%4$s</video:title><video:description>%5$s</video:description>', $contentLoc, $playerLoc, $thumbnailLoc, $title, $description));
	}
}