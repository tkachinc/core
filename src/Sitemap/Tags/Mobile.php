<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Mobile extends AbstractTag implements InterfaceChildTypes
{
	const PATTERN = '<mobile:mobile/>';
}