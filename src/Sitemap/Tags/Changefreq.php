<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Changefreq extends AbstractTag implements InterfaceChildTypes
{
	const PATTERN = '<changefreq>%1$s</changefreq>';
	protected static $availableChangefreq = [
		'always'  => 1,
		'hourly'  => 1,
		'daily'   => 1,
		'weekly'  => 1,
		'monthly' => 1,
		'yearly'  => 1,
		'never'   => 1,
	];

	public function __construct(string $changefreq)
	{
		if (isset(static::$availableChangefreq[$changefreq])) {
			$this->add($changefreq, 'changefreq');
		}
	}
}