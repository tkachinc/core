<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Urlset extends AbstractTag implements InterfaceBaseTypes
{
	const PATTERN = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 " %2$s>%1$s</urlset>';
	protected $count = 0;
	protected $sets = [];
	protected $setsCount = 0;
	protected $isMulti = false;
	protected static $limit = 50000;

	public function addUrl(Url $url)
	{
		$this->add($url);
		$this->count++;
		if ($this->count >= static::$limit) {
			$this->addSet();
		}
	}

	protected function addSet()
	{
		$this->sets[] = clone $this;
		$this->setsCount++;

		$this->onceLock = [];
		$this->content = '';
		$this->attributes = '';

		$this->isMulti = true;
		$this->count = 0;
	}

	public function getCount()
	{
		return $this->count;
	}

	public function getSets()
	{
		return $this->sets;
	}

	public function isMulti()
	{
		return $this->isMulti;
	}

	public function autoGenerateSitemapIndex($pattern = 'sitemap%s.xml', $setTime = false)
	{
		if ($this->isMulti()) {
			if ($this->count > 0) {
				$this->addSet();
			}
			$sitemapindex = new Sitemapindex();
			for ($i = 1; $i <= $this->setsCount; $i++) {
				$sitemap = new Sitemap(new Loc(sprintf($pattern, $i)));
				if ($setTime) {
					$sitemap->addLastmod(new Lastmod(new \DateTime()));
				}

				$sitemapindex->addSitemap($sitemap);
			}

			return new Xml($sitemapindex);
		}

		return null;
	}

	public function getXml()
	{
		return new Xml($this);
	}

	public function autoGenerateSitemap($pattern = 'sitemap%s.xml')
	{
		if ($this->isMulti()) {
			if ($this->count > 0) {
				$this->addSet();
			}
			$i = 1;
			foreach ($this->sets as $set) {
				yield sprintf($pattern, $i) => new Xml($set);
				$i++;
			}
		}
	}

	public function extendVideo()
	{
		if ($this->once('extendVideo')) {
			$this->addAttributes('xmlns:video="http://www.google.com/schemas/sitemap-video/1.1" ');
		}
	}

	public function extendImage()
	{
		if ($this->once('extendImage')) {
			$this->addAttributes('xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" ');
		}
	}

	public function extendMobile()
	{
		if ($this->once('extendMobile')) {
			$this->addAttributes('xmlns:mobile="http://www.google.com/schemas/sitemap-mobile/1.0" ');
		}
	}
}