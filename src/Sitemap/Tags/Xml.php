<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Xml extends AbstractTag
{
	const PATTERN = '<?xml version="1.0" encoding="UTF-8"?>%s';

	public function __construct(InterfaceBaseTypes $xml)
	{
		$this->add($xml);
	}
}