<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Sitemap extends AbstractTag implements InterfaceMiddleTypes
{
	const PATTERN = '<sitemap>%1$s</sitemap>';

	public function __construct(Loc $loc)
	{
		$this->add($loc, 'loc');
	}

	public function addLastmod(Lastmod $lastmod)
	{
		$this->add($lastmod, 'lastmod');
	}
}