<?php
namespace TkachInc\Core\Sitemap\Tags;
use TkachInc\Core\URLFacade;

/**
 * @author maxim
 */
class Loc extends AbstractTag
{
	const PATTERN = '<loc>%1$s</loc>';

	public function __construct(string $path = null, string $url = null)
	{
		if ($path && !$url) {
			$url = URLFacade::to($path, true);
		} elseif (!$path && !$url) {
			throw new \Exception('All params is null');
		}
		$this->add($url, 'loc');
	}
}