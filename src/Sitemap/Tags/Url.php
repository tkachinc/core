<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
class Url extends AbstractTag implements InterfaceMiddleTypes
{
	const PATTERN = '<url>%1$s</url>';

	public function __construct(Loc $loc)
	{
		$this->add($loc, 'loc');
	}

	public function addChangefreq(Changefreq $changefreq)
	{
		$this->add($changefreq, 'changefreq');
	}

	public function addPriority(Priority $priority)
	{
		$this->add($priority, 'priority');
	}

	public function addLastmod(Lastmod $lastmod)
	{
		$this->add($lastmod, 'lastmod');
	}

	public function addImage(Image $image)
	{
		$this->add($image, 'image');
	}

	public function addVideo(Video $video)
	{
		$this->add($video, 'video');
	}

	public function addMobile(Mobile $mobile)
	{
		$this->add($mobile, 'mobile');
	}

	public function addAlternate(Alternate $alternate)
	{
		$this->add($alternate);
	}
}