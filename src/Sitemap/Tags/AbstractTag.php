<?php
namespace TkachInc\Core\Sitemap\Tags;

/**
 * @author maxim
 */
abstract class AbstractTag
{
	const PATTERN = '';
	protected $onceLock = [];
	protected $content = '';
	protected $attributes = '';

	public static function getKey($field)
	{
		return static::class . '::' . $field;
	}

	public function once($field)
	{
		if (!isset($this->onceLock[$field])) {
			$this->onceLock[$field] = 1;

			return true;
		} else {
			return false;
		}
	}

	public function add($custom = '', $field = null)
	{
		if ($field) {
			if (!$this->once($field)) {
				return;
			}
		}

		$this->content .= (string)$custom;
	}

	public function addAttributes($attributes = '', $field = null)
	{
		if ($field) {
			$field = static::getKey('changefreq');
			if (!$this->once($field)) {
				return;
			}
		}

		$this->attributes .= (string)$attributes;
	}

	public function getResult()
	{
		return sprintf(static::PATTERN, $this->content, $this->attributes);
	}

	public function __toString()
	{
		return $this->getResult();
	}
}