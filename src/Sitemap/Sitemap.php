<?php
namespace TkachInc\Core\Sitemap;

use TkachInc\Core\Sitemap\Tags\Alternate;
use TkachInc\Core\Sitemap\Tags\Changefreq;
use TkachInc\Core\Sitemap\Tags\Lastmod;
use TkachInc\Core\Sitemap\Tags\Priority;
use TkachInc\Engine\Services\Helpers\ArrayHelper;

/**
 * Class Sitemap
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Sitemap
{
	protected $urls;

	protected $xmlIndex = '';

	protected $xml = [];

	protected $pattern;

	public function __construct(Array $urls, $pattern = 'sitemap%s.xml')
	{
		$this->urls = $urls;
		$this->pattern = $pattern;
		$this->run();
	}

	protected function run()
	{
		$urlSet = new \TkachInc\Core\Sitemap\Tags\Urlset();

		foreach ($this->urls as $item) {
			$item = ArrayHelper::filterOnlyRequiredParams($item,
				[
					'path'       => ArrayHelper::TYPE_REQUIRED,
					'changefreq' => ArrayHelper::TYPE_OPTIONAL,
					'priority'   => ArrayHelper::TYPE_OPTIONAL,
					'lastmod'    => ArrayHelper::TYPE_OPTIONAL,
					'alternate'  => ArrayHelper::TYPE_OPTIONAL,
				]);

			$url = new \TkachInc\Core\Sitemap\Tags\Url(new \TkachInc\Core\Sitemap\Tags\Loc($item['path']));
			if (isset($item['changefreq'])) {
				$url->addChangefreq(new Changefreq($item['changefreq']));
			}
			if (isset($item['priority'])) {
				$url->addPriority(new Priority($item['priority']));
			}
			if (isset($item['lastmod'])) {
				$url->addLastmod(new Lastmod($item['lastmod']));
			}
			if (isset($item['alternate']) && is_array($item['alternate'])) {
				foreach ($item['alternate'] as $alt) {
					$alt = ArrayHelper::filterOnlyRequiredParams($alt,
						[
							'lang' => ArrayHelper::TYPE_OPTIONAL,
							'path' => ArrayHelper::TYPE_REQUIRED,
						]);
//					dd($alt['lang']);
					$url->addAlternate(new Alternate($alt['path'], null, $alt['lang']??null));
				}
			}

			$urlSet->addUrl($url);
		}

		if ($urlSet->isMulti()) {
			$xmlIndex = $urlSet->autoGenerateSitemapIndex($this->pattern);
			$xmlGenerator = $urlSet->autoGenerateSitemap($this->pattern);
			$data = [];
			foreach ($xmlGenerator as $name => $xml) {
				$data[$name] = (string)$xml;
			}
			$this->xmlIndex = (string)$xmlIndex;
			$this->xml = $data;
		} else {
			$xml = (string)$urlSet->getXml();
			$this->xml = [sprintf($this->pattern, '') => $xml];
		}
	}

	public function getXml()
	{
		return $this->xml;
	}

	public function getXmlIndex()
	{
		return $this->xmlIndex;
	}
}