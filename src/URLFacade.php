<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 1/31/17
 * Time: 16:39
 */

namespace TkachInc\Core;

use TkachInc\Core\Config\Config;
use TkachInc\Engine\Services\Facade;

class URLFacade extends Facade
{
	public static function getObject()
	{
		return new URLHelper(Config::getInstance());
	}
}