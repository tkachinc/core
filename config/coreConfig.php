<?php
//$config['baseUrl'] = '';

$config['additionalComponents'] = ['base' => 1];
$config['cms'] = [
	'dir'           => [
		'path'     => APP_PATH . '/templates',
		'template' => 'sparrow',
	],
	'multiLanguage' => true,
];

$config['locale'] = [
	'en' => 'en_US',
	'ru' => 'ru_RU',
	'uk' => 'uk_UA',
];

$config['isTestHost'] = false;
$config['useCase'] = false;
$config['isDebug'] = false;